import re

def get_entity(modified_design_file_name):

    with open(modified_design_file_name, "r") as design_file:
        for line in design_file:
            if "ENTITY" in line and "IS" in line:
                entity_name = re.search('ENTITY [A-Za-z_0-9]* IS', line.upper()).group(0).replace("ENTITY", "").replace("IS", "").strip().lower().replace("_mod", "")
                break
    return entity_name


def write_entity(target_file,
                 modified_design_file_name,
                 entity_name,
                 locking_inputs,
                 masking_inputs):
    target_file.write("LIBRARY ieee;\n")
    target_file.write("USE ieee.std_logic_1164.ALL;\n")
    target_file.write("\n")
    target_file.write("ENTITY "+entity_name+"_activable IS\n")
    target_file.write("\n")
    target_file.write("  PORT (\n")
    inputs = []
    outputs = []
    with open(modified_design_file_name, "r") as design_file:
        for line in design_file:
            if "IN STD_LOGIC" in line.upper():
                input_name = line.split(":")[0].strip()
                if input_name not in locking_inputs and input_name not in masking_inputs:
                    if "IN STD_LOGIC_VECTOR" in line.upper():
                        if "DOWNTO" in line:
                            vector_range = re.search('\([0-9]* DOWNTO [0-9]*\)', line.upper()).group(0)[1:-1].replace(" ", "").split("DOWNTO")
                            target_file.write("    "+input_name+" : IN STD_LOGIC_VECTOR("+vector_range[0]+" DOWNTO "+vector_range[1]+");\n")
                        elif "TO" in line:
                            vector_range = re.search('\([0-9]* TO [0-9]*\)', line.upper()).group(0)[1:-1].replace(" ", "").split("TO")
                            target_file.write("    "+input_name+" : IN STD_LOGIC_VECTOR("+vector_range[0]+" TO "+vector_range[1]+");\n")
                    else:
                        target_file.write("    "+line.split(":")[0].strip()+" : IN STD_LOGIC;\n")
                    inputs.append(line.split(":")[0].strip())
            elif "OUT STD_LOGIC" in line.upper():
                if "OUT STD_LOGIC_VECTOR" in line.upper():
                    if "DOWNTO" in line:
                        vector_range = re.search('\([0-9]* DOWNTO [0-9]*\)', line.upper()).group(0)[1:-1].replace(" ", "").split("DOWNTO")
                        target_file.write("    "+line.split(":")[0].strip()+" : OUT STD_LOGIC_VECTOR("+vector_range[0]+" DOWNTO "+vector_range[1]+");\n")
                    elif "TO" in line:
                        vector_range = re.search('\([0-9]* TO [0-9]*\)', line.upper()).group(0)[1:-1].replace(" ", "").split("TO")
                        target_file.write("    "+line.split(":")[0].strip()+" : OUT STD_LOGIC_VECTOR("+vector_range[0]+" TO "+vector_range[1]+");\n")
                else:
                    target_file.write("    "+line.split(":")[0].strip()+" : OUT STD_LOGIC;\n")
                outputs.append(line.split(":")[0].strip())
        target_file.write("    key : IN STD_LOGIC_VECTOR(127 DOWNTO 0)\n    );\n\n")
        target_file.write("END ENTITY "+entity_name+"_activable;\n\n")
    return inputs, outputs

def write_architecture(target_file,
                       entity_name,
                       locking,
                       masking,
                       inputs,
                       outputs):
    target_file.write("ARCHITECTURE rtl OF "+entity_name+"_activable IS\n\n")
    if locking[0] and masking[0]:
        target_file.write("  SIGNAL AW_locking : STD_LOGIC_VECTOR("+str(len(locking[1])-1)+" DOWNTO 0);\n")
        target_file.write("  SIGNAL AW_masking : STD_LOGIC_VECTOR(0 TO "+str(len(masking[1])-1)+");\n")
        target_file.write("  SIGNAL AW         : STD_LOGIC_VECTOR(0 TO "+str(len(masking[1])+len(locking[1])-1)+");\n\n")
    if locking[0] and not masking[0]:
        target_file.write("  SIGNAL AW : STD_LOGIC_VECTOR("+str(len(locking[1])-1)+" DOWNTO 0);\n\n")
    if not locking[0] and masking[0]:
        target_file.write("  SIGNAL AW : STD_LOGIC_VECTOR(0 TO "+str(len(masking[1])-1)+");\n\n")
    target_file.write("BEGIN\n\n")
    target_file.write("  AW_decoder_1: ENTITY work.AW_decoder\n")
    target_file.write("    PORT MAP (\n")
    target_file.write("      formatted_AW => key,\n")
    target_file.write("      AW           => AW);\n\n")
    if locking[0] and masking[0]:
        target_file.write("  AW_locking <= AW(0 TO "+str(len(locking[1])-1)+");\n")
        target_file.write("  AW_masking <= AW("+str(len(locking[1]))+" TO "+str(len(locking[1])+len(masking[1])-1)+");\n\n")
    target_file.write("  "+entity_name+"_mod_1: ENTITY work."+entity_name+"_mod\n")
    target_file.write("    PORT MAP (\n")
    for input_net in inputs:
        target_file.write("      "+input_net+" => "+input_net+",\n")
    if locking[0] and masking[0]:
        for index, locking_input in enumerate(locking[1]):
            target_file.write("      "+locking_input+" => AW_locking("+str(index)+"),\n")
        for index, masking_input in enumerate(masking[1]):
            target_file.write("      "+masking_input+" => AW_masking("+str(index)+"),\n")
    elif locking[0] and not masking[0]:
        for index, locking_input in enumerate(locking[1]):
            target_file.write("      "+locking_input+" => AW("+str(index)+"),\n")
    elif not locking[0] and masking[0]:
        for index, masking_input in enumerate(masking[1]):
            target_file.write("      "+masking_input+" => AW("+str(index)+"),\n")
    for output in outputs:
        if output == outputs[-1]:
            target_file.write("      "+output+" => "+output+");\n\n")
        else:
            target_file.write("      "+output+" => "+output+",\n")
    target_file.write("END ARCHITECTURE rtl;")


def generate_activable_design(modified_design_file_name,
                          AW_decoder_file_name,
                          locking,
                          masking):
    target_file_name = modified_design_file_name.replace("mod", "activable")
    
    with open(target_file_name, "w") as target_file:
        entity_name = get_entity(modified_design_file_name)
        inputs, outputs = write_entity(target_file, modified_design_file_name, entity_name, locking[1], masking[1])
        write_architecture(target_file, entity_name, locking, masking, inputs, outputs)
        
if __name__ == "__main__":
    locking_inputs = ["KG180gat", "KG285gat", "KG356gat", "KG416gat", "KG381gat"]
    masking_inputs = ["KG309gat", "KG203gat", "KG319gat", "KG213gat", "KG296gat", "KG357gat", "KG360gat", "KG199gat"]
    generate_activable_design("./../user_space/c432_mod.vhd",
                          "./../user_space/AW_decoder.vhd",
                          (True, locking_inputs),
                          (True, masking_inputs),
                          "./../user_space/c432_activable.vhd")
                          
