# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: build_tabs.py
# Date: 2016-10-24

from Tkinter import *
from ttk import *


def build_tabs(self):
    
    """Insert tabs"""

    self.note = Notebook(self.master)
    self.note.enable_traversal()
    self.tab_logic_modifier = Frame(self.note)
    self.note.add(self.tab_logic_modifier,
                  text="Logic modifier",
                  padding=[0, 2, 0, 0])
    self.tab_enrolment = Frame(self.note)
    self.tab_HECTOR_board_management = Frame(self.note)
    self.note.add(self.tab_HECTOR_board_management,
                  text="HECTOR board management",
                  padding=[0, 2, 0, 0])
    self.note.add(self.tab_enrolment, text="Enrolment",
                  padding=[0, 2, 0, 0])
    self.tab_activation = Frame(self.note)
    self.note.add(self.tab_activation, text="Activation",
                  padding=[0, 2, 0, 0])
    self.note.pack(side = "top", fill="both", expand="yes")

    def handle_tab_changed(event):
        self.update_status("")
    
    self.note.bind("<<NotebookTabChanged>>", handle_tab_changed)