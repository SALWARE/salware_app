import collections as col
import numpy as np
import networkx as nx

def heuristic_approximate_current_flow_betweenness(g_nx, key_size):
    try:
        print("Trying option 1 (fastest)")
        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                        normalized = False,
                                                                                                                        solver = "full",
                                                                                                                        dtype = np.float32,
                                                                                                                        epsilon = 1.0).items(), key=lambda t: t[1])))[-key_size:]]
    except:
        try:
            print("Trying option 1 (fastest)")
            list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                            normalized = False,
                                                                                                                            solver = "full",
                                                                                                                            dtype = np.float32,
                                                                                                                            epsilon = 5.0).items(), key=lambda t: t[1])))[-key_size:]]
        except:
            try:
                print("Trying option 1 (fastest)")
                list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                normalized = False,
                                                                                                                                solver = "full",
                                                                                                                                dtype = np.float32,
                                                                                                                                epsilon = 10.0).items(), key=lambda t: t[1])))[-key_size:]]
            except:
                try:
                    print("Trying option 2 (average)")
                    list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                    normalized = False,
                                                                                                                                    solver = "lu",
                                                                                                                                    dtype = np.float32,
                                                                                                                                    epsilon = 1.0).items(), key=lambda t: t[1])))[-key_size:]]
                except:
                    try:
                        print("Trying option 2 (average)")
                        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                        normalized = False,
                                                                                                                                        solver = "lu",
                                                                                                                                        dtype = np.float32,
                                                                                                                                        epsilon = 5.0).items(), key=lambda t: t[1])))[-key_size:]]
                    except:
                        try:
                            print("Trying option 2 (average)")
                            list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                            normalized = False,
                                                                                                                                            solver = "lu",
                                                                                                                                            dtype = np.float32,
                                                                                                                                            epsilon = 10.0).items(), key=lambda t: t[1])))[-key_size:]]
                        except:
                            try:
                                print("Trying option 3 (slowest)")
                                list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                                normalized = False,
                                                                                                                                                solver = "cg",
                                                                                                                                                dtype = np.float32,
                                                                                                                                                epsilon = 1.0).items(), key=lambda t: t[1])))[-key_size:]]
                            except:
                                try:
                                    print("Trying option 3 (slowest)")
                                    list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                                    normalized = False,
                                                                                                                                                    solver = "cg",
                                                                                                                                                    dtype = np.float32,
                                                                                                                                                    epsilon = 5.0).items(), key=lambda t: t[1])))[-key_size:]]
                                except:
                                    try:
                                        print("Trying option 3 (slowest)")
                                        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                                        normalized = False,
                                                                                                                                                        solver = "cg",
                                                                                                                                                        dtype = np.float32,
                                                                                                                                                        epsilon = 10.0).items(), key=lambda t: t[1])))[-key_size:]]
                                    except:
                                        print("Failed !")
                                        return
    return list_nodes_to_mask