# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : For test
Project    : Graph-based nodes selection for logic masking

File       : test.py
Last update: 2015-03-17
"""

# import sys
# sys.path.append("../Parsers/")

# import build_bench
# import build_blif
# import build_edif
# import build_slif
# import build_verilog_rtl
# import build_verilog_struct
# import build_vhd_rtl
# import build_vhd_struct
# import build_xilinx

import random
import networkx as nx
import igraph
import collections as col
import isolate_largest_cluster
import modify_nodes_mask

import heuristic_current_flow_closeness
import heuristic_approximate_current_flow_betweenness
import heuristic_current_flow_betweenness

import numpy as np

def masking(g, prim_in, prim_out, nodes, overhead, heuristic, list_nodes_to_lock = []):
    
    h = g.copy() #Perform deep copy
    key_size = int(len(nodes)*overhead/100)

    if heuristic == "random":
        list_nodes_to_mask = random.sample(nodes, key_size)
    elif heuristic == "current_flow_closeness":
        g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
        list_nodes_to_mask = heuristic_current_flow_closeness.heuristic_current_flow_closeness(g_nx, key_size)
    elif heuristic == "approximate_current_flow_betweenness":
        g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
        list_nodes_to_mask = heuristic_approximate_current_flow_betweenness.heuristic_approximate_current_flow_betweenness(g_nx, key_size)
    elif heuristic == "closeness":
        list_nodes_to_mask = [i[1] for i in sorted(zip(g.evcent(directed = False), nodes))[-key_size:]]
    elif heuristic == "betweenness":
        list_nodes_to_mask = [i[1] for i in sorted(zip(g.betweenness(directed = False), nodes))[-key_size:]]
    elif heuristic == "current_flow_betweenness":
        g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
        list_nodes_to_mask = heuristic_current_flow_betweenness.heuristic_current_flow_betweenness(g_nx, key_size)
    else:
        raise ValueError("Unknown heuristic")

    # Exclude nodes which were previously locked already
    list_nodes_to_mask = [i.encode('ascii','ignore') for i in list_nodes_to_mask if i not in [j[0] for j in list_nodes_to_lock]]
    masking_key = []
    for i in list_nodes_to_mask:
        masking_key.append(random.randint(0, 1))
    h, masking_inputs = modify_nodes_mask.modify_nodes_mask(h, list_nodes_to_mask, masking_key)
    masking_key = str(masking_key).replace(", ", "").replace("[", "").replace("]", "")
    
    return h, masking_key, masking_inputs

if __name__ == "__main__":
    for name in ["c3540"]:
        overhead = 0.01
        for heuristic in ["random",
                          "current_flow_closeness",
                          "approximated_current_flow_betweenness",
                          "closeness",
                          "betweenness",
                          "current_flow_betweenness"]:
            g, prim_in, prim_out, nodes = build_bench.build(name)
            print heuristic
            _, b = masking(g, prim_in, prim_out, nodes, overhead, heuristic)
            print b
