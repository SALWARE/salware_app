import collections as col
import networkx as nx
import numpy as np

def heuristic_current_flow_closeness(g_nx, key_size):
    try:
        print("Trying option 1 (fastest)")
        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.current_flow_closeness_centrality(g_nx,
                                                                                                          solver = "full",
                                                                                                          dtype = np.float32).items(), key=lambda t: t[1])))[-key_size:]]
    except:
        try:
            print("Trying option 2 (average)")
            list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.current_flow_closeness_centrality(g_nx,
                                                                                                              solver = "lu",
                                                                                                               dtype = np.float32).items(), key=lambda t: t[1])))[-key_size:]]
        except:
            try:
                print("Trying option 3 (slowest)")
                list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.current_flow_closeness_centrality(g_nx,
                                                                                                                  solver = "cg",
                                                                                                                  dtype = np.float32).items(), key=lambda t: t[1])))[-key_size:]]
            except:
                print("Failed !")
                return
    return list_nodes_to_mask
