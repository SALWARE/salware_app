import collections as col
import numpy as np
import networkx as nx

def heuristic_current_flow_betweenness(g_nx, key_size):

    try:
        print("Trying option 1 (fastest)")
        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.current_flow_betweenness_centrality(g_nx, normalized = False, solver = "full").items(), key=lambda t: t[1])))[-key_size:]]
    except:
        try:
            print("Trying option 2 (average)")
            list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.current_flow_betweenness_centrality(g_nx, normalized = False, solver = "cg").items(), key=lambda t: t[1])))[-key_size:]]
        except:
            print("Failed !")
            return
    return list_nodes_to_mask
