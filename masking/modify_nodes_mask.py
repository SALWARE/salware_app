# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Modify nodes mask
Project    : Graph-based nodes selection for logic masking

File       : modify_nodes_mask.py
Last update: 2015-04-09
"""

def modify_nodes_mask(g, list_nodes_to_mask, masking_key):
    
    if len(list_nodes_to_mask) != len(masking_key):
        raise ValueError("The key length does not match the number of nodes to mask !")
        
    mask = {0:"xnor", 1:"xor"}
    masking_inputs = []
    #Handle vertices
    for i in list_nodes_to_mask:
        attributes = g.vs.find(i).attributes()
        attributes["name"] = "K"+i
        masking_inputs.append("K"+i)
        attributes["label"] = attributes["name"]
        g.add_vertex(name = attributes["name"])
        for j in attributes:
            g.vs[len(g.vs)-1][j] = attributes[j]
        g.vs[len(g.vs)-1]["color"] = "orange"
        g.vs[len(g.vs)-1]["cat"] = "input"
        attributes["name"] = i+"_mod"
        attributes["label"] = attributes["name"]
        g.add_vertex(name = attributes["name"])
        for j in attributes:
            g.vs[len(g.vs)-1][j] = attributes[j]
        g.vs[len(g.vs)-1]["color"] = "lightblue"
        g.vs[len(g.vs)-1]["cat"] = "mod"
    #Handle edges
    for i in list_nodes_to_mask:
        #Copy outgoing edges to mod node
        for j in g.incident(g.vs.find(i), mode = "OUT"):
            g.add_edge(i+"_mod", g.vs[g.es[j].target]["label"],
                          label = g.es[j]["label"],
                          width = 5,
                          arrow_size = 2,
                          label_size = 30,
                          color = "#FF0000",
                          cat = "mask")
    edges_to_delete = []
    for i in list_nodes_to_mask:
        #Delete copied edges from the original node
        for j in g.incident(g.vs.find(i), mode = "OUT"):
            edges_to_delete.append((i, g.vs[g.es[j].target]["label"]))
    g.delete_edges(edges_to_delete)
    for i in zip(list_nodes_to_mask, masking_key):
        if i[1] in [0, 1]:
            #The node should be forced to 0
            #An AND gate will be used
            g.add_edge("K"+i[0], i[0]+"_mod",
                          label = mask[i[1]],
                          width = 5,
                          arrow_size = 2,
                          label_size = 30,
                          color = "#FF0000",
                          cat = "mask")
            g.add_edge(i[0], i[0]+"_mod",
                          label = mask[i[1]],
                          width = 5,
                          arrow_size = 2,
                          label_size = 30,
                          color = "#FF0000",
                          cat = "mask")
    
    return g, masking_inputs