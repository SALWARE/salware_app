def otp(plaintext, key):
    if len(plaintext) != len(key):
        raise ValueError("The key and the plaintext should have the same length")
    elif set(plaintext+"01") > set(['0', '1']):
        raise ValueError("The plaintext contains illegal characters")
    elif set(key+"01") > set(['0', '1']):
        raise ValueError("The key contains illegal characters")
    else:
        ciphertext = ""
        for plaintext_bit, key_bit in zip(plaintext, key):
            ciphertext+=str(int(plaintext_bit)^int(key_bit))
    return ciphertext

if __name__ == "__main__":
    plaintext = bin(int("CEDBCE851A6BFC6E19711E8B17C1B47C", 16))[2:].zfill(128)
    key       = bin(int("44444444444444444444444444444444", 16))[2:].zfill(128)
    print plaintext
    print key
    print hex(int(otp(plaintext, key), 2)).upper()[2:-1]
    print otp(plaintext, key)
        
