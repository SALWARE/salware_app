-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE work.RandomPkg.ALL;
USE work.TranscriptPkg.ALL;

-------------------------------------------------------------------------------

ENTITY top_tb IS

END ENTITY top_tb;

-------------------------------------------------------------------------------

ARCHITECTURE simulation OF top_tb IS

  -- component ports

  SIGNAL key         : STD_LOGIC_VECTOR(127 DOWNTO 0);
  SIGNAL inputs      : STD_LOGIC_VECTOR(35 DOWNTO 0);
  SIGNAL outputs     : STD_LOGIC_VECTOR(6 DOWNTO 0);
  SIGNAL outputs_mod : STD_LOGIC_VECTOR(6 DOWNTO 0);

BEGIN  -- ARCHITECTURE simulation

  -- component instantiation
  DUT : ENTITY work.top
    PORT MAP (
      G1gat   => inputs(0),
      G4gat   => inputs(1),
      G8gat   => inputs(2),
      G11gat  => inputs(3),
      G14gat  => inputs(4),
      G17gat  => inputs(5),
      G21gat  => inputs(6),
      G24gat  => inputs(7),
      G27gat  => inputs(8),
      G30gat  => inputs(9),
      G34gat  => inputs(10),
      G37gat  => inputs(11),
      G40gat  => inputs(12),
      G43gat  => inputs(13),
      G47gat  => inputs(14),
      G50gat  => inputs(15),
      G53gat  => inputs(16),
      G56gat  => inputs(17),
      G60gat  => inputs(18),
      G63gat  => inputs(19),
      G66gat  => inputs(20),
      G69gat  => inputs(21),
      G73gat  => inputs(22),
      G76gat  => inputs(23),
      G79gat  => inputs(24),
      G82gat  => inputs(25),
      G86gat  => inputs(26),
      G89gat  => inputs(27),
      G92gat  => inputs(28),
      G95gat  => inputs(29),
      G99gat  => inputs(30),
      G102gat => inputs(31),
      G105gat => inputs(32),
      G108gat => inputs(33),
      G112gat => inputs(34),
      G115gat => inputs(35),
      key     => key,
      G223gat => outputs_mod(0),
      G329gat => outputs_mod(1),
      G370gat => outputs_mod(2),
      G421gat => outputs_mod(3),
      G430gat => outputs_mod(4),
      G431gat => outputs_mod(5),
      G432gat => outputs_mod(6));

  c432_1 : ENTITY work.c432
    PORT MAP (
      G1gat   => inputs(0),
      G4gat   => inputs(1),
      G8gat   => inputs(2),
      G11gat  => inputs(3),
      G14gat  => inputs(4),
      G17gat  => inputs(5),
      G21gat  => inputs(6),
      G24gat  => inputs(7),
      G27gat  => inputs(8),
      G30gat  => inputs(9),
      G34gat  => inputs(10),
      G37gat  => inputs(11),
      G40gat  => inputs(12),
      G43gat  => inputs(13),
      G47gat  => inputs(14),
      G50gat  => inputs(15),
      G53gat  => inputs(16),
      G56gat  => inputs(17),
      G60gat  => inputs(18),
      G63gat  => inputs(19),
      G66gat  => inputs(20),
      G69gat  => inputs(21),
      G73gat  => inputs(22),
      G76gat  => inputs(23),
      G79gat  => inputs(24),
      G82gat  => inputs(25),
      G86gat  => inputs(26),
      G89gat  => inputs(27),
      G92gat  => inputs(28),
      G95gat  => inputs(29),
      G99gat  => inputs(30),
      G102gat => inputs(31),
      G105gat => inputs(32),
      G108gat => inputs(33),
      G112gat => inputs(34),
      G115gat => inputs(35),
      G223gat => outputs(0),
      G329gat => outputs(1),
      G370gat => outputs(2),
      G421gat => outputs(3),
      G430gat => outputs(4),
      G431gat => outputs(5),
      G432gat => outputs(6));

  PROCESS IS
    VARIABLE RV        : RandomPType;   -- protected type from RandomPkg
    VARIABLE inputs_rd : STD_LOGIC_VECTOR(35 DOWNTO 0);
    VARIABLE key_rd    : STD_LOGIC_VECTOR(127 DOWNTO 0);
  BEGIN  -- PROCESS
    RV.InitSeed (RV'instance_name);
    FOR I IN 0 TO 9 LOOP
      FOR J IN 0 TO 9 LOOP
        inputs_rd := STD_LOGIC_VECTOR(RV.RandUnsigned(36));
        inputs    <= inputs_rd;
        key_rd    := STD_LOGIC_VECTOR(RV.RandUnsigned(128));
        key       <= key_rd;
        key(0)    <= '1';
        key(3)    <= '1';
        WAIT FOR 10 ns;
      END LOOP;
      key <= "00110000010100011100000000111100010110101100101011100110011001101100010010110000110110110100110011100000001011011110110110100011";
      WAIT FOR 10 ns;
    END LOOP;
  END PROCESS;

END ARCHITECTURE simulation;

-------------------------------------------------------------------------------
