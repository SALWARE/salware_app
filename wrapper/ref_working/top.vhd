LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY top IS

  PORT (
    G1gat   : IN  STD_LOGIC;
    G4gat   : IN  STD_LOGIC;
    G8gat   : IN  STD_LOGIC;
    G11gat  : IN  STD_LOGIC;
    G14gat  : IN  STD_LOGIC;
    G17gat  : IN  STD_LOGIC;
    G21gat  : IN  STD_LOGIC;
    G24gat  : IN  STD_LOGIC;
    G27gat  : IN  STD_LOGIC;
    G30gat  : IN  STD_LOGIC;
    G34gat  : IN  STD_LOGIC;
    G37gat  : IN  STD_LOGIC;
    G40gat  : IN  STD_LOGIC;
    G43gat  : IN  STD_LOGIC;
    G47gat  : IN  STD_LOGIC;
    G50gat  : IN  STD_LOGIC;
    G53gat  : IN  STD_LOGIC;
    G56gat  : IN  STD_LOGIC;
    G60gat  : IN  STD_LOGIC;
    G63gat  : IN  STD_LOGIC;
    G66gat  : IN  STD_LOGIC;
    G69gat  : IN  STD_LOGIC;
    G73gat  : IN  STD_LOGIC;
    G76gat  : IN  STD_LOGIC;
    G79gat  : IN  STD_LOGIC;
    G82gat  : IN  STD_LOGIC;
    G86gat  : IN  STD_LOGIC;
    G89gat  : IN  STD_LOGIC;
    G92gat  : IN  STD_LOGIC;
    G95gat  : IN  STD_LOGIC;
    G99gat  : IN  STD_LOGIC;
    G102gat : IN  STD_LOGIC;
    G105gat : IN  STD_LOGIC;
    G108gat : IN  STD_LOGIC;
    G112gat : IN  STD_LOGIC;
    G115gat : IN  STD_LOGIC;
    key     : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
    G223gat : OUT STD_LOGIC;
    G329gat : OUT STD_LOGIC;
    G370gat : OUT STD_LOGIC;
    G421gat : OUT STD_LOGIC;
    G430gat : OUT STD_LOGIC;
    G431gat : OUT STD_LOGIC;
    G432gat : OUT STD_LOGIC
    );

END ENTITY top;

ARCHITECTURE rtl OF top IS

  SIGNAL AW_locking : STD_LOGIC_VECTOR(4 DOWNTO 0);
  SIGNAL AW_masking : STD_LOGIC_VECTOR(0 TO 7);
  SIGNAL AW : STD_LOGIC_VECTOR(0 TO 12);
  
BEGIN  -- ARCHITECTURE rtl

  AW_decoder_1: ENTITY work.AW_decoder
    PORT MAP (
      formatted_AW => key,
      AW           => AW);

  AW_locking <= AW(0 TO 4);
  AW_masking <= AW(5 TO 12);

  c432_mod_1: ENTITY work.c432_mod
    PORT MAP (
      G1gat    => G1gat,
      G4gat    => G4gat,
      G8gat    => G8gat,
      G11gat   => G11gat,
      G14gat   => G14gat,
      G17gat   => G17gat,
      G21gat   => G21gat,
      G24gat   => G24gat,
      G27gat   => G27gat,
      G30gat   => G30gat,
      G34gat   => G34gat,
      G37gat   => G37gat,
      G40gat   => G40gat,
      G43gat   => G43gat,
      G47gat   => G47gat,
      G50gat   => G50gat,
      G53gat   => G53gat,
      G56gat   => G56gat,
      G60gat   => G60gat,
      G63gat   => G63gat,
      G66gat   => G66gat,
      G69gat   => G69gat,
      G73gat   => G73gat,
      G76gat   => G76gat,
      G79gat   => G79gat,
      G82gat   => G82gat,
      G86gat   => G86gat,
      G89gat   => G89gat,
      G92gat   => G92gat,
      G95gat   => G95gat,
      G99gat   => G99gat,
      G102gat  => G102gat,
      G105gat  => G105gat,
      G108gat  => G108gat,
      G112gat  => G112gat,
      G115gat  => G115gat,
      KG180gat => AW_locking(0),
      KG285gat => AW_locking(1),
      KG356gat => AW_locking(2),
      KG416gat => AW_locking(3),
      KG381gat => AW_locking(4),
      KG309gat => AW_masking(0),
      KG203gat => AW_masking(1),
      KG319gat => AW_masking(2),
      KG213gat => AW_masking(3),
      KG296gat => AW_masking(4),
      KG357gat => AW_masking(5),
      KG360gat => AW_masking(6),
      KG199gat => AW_masking(7),
      G223gat  => G223gat,
      G329gat  => G329gat,
      G370gat  => G370gat,
      G421gat  => G421gat,
      G430gat  => G430gat,
      G431gat  => G431gat,
      G432gat  => G432gat);

END ARCHITECTURE rtl;
