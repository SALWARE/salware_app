vlib work
vmap work work

vcom -2008 -work work OSVVM-master/NamePkg.vhd
vcom -2008 -work work OSVVM-master/OsvvmGlobalPkg.vhd
vcom -2008 -work work OSVVM-master/TranscriptPkg.vhd
vcom -2008 -work work OSVVM-master/TextUtilPkg.vhd
vcom -2008 -work work OSVVM-master/AlertLogPkg.vhd
vcom -2008 -work work OSVVM-master/RandomBasePkg.vhd
vcom -2008 -work work OSVVM-master/SortListPkg_int.vhd
vcom -2008 -work work OSVVM-master/RandomPkg.vhd

vcom -2008 -novopt -work work ./c432_mod.vhd
vcom -2008 -novopt -work work ./c432.vhd
vcom -2008 -novopt -work work ./AW_decoder.vhd
vcom -2008 -novopt -work work ./top.vhd
vcom -2008 -novopt -work work ./top_tb.vhd

vsim -novopt work.top_tb

add wave sim:/top_tb/inputs
add wave sim:/top_tb/key
add wave sim:/top_tb/DUT/AW_decoder_1/AW
add wave sim:/top_tb/DUT/AW_locking
add wave sim:/top_tb/DUT/AW_masking
add wave sim:/top_tb/outputs
add wave sim:/top_tb/outputs_mod

virtual signal {top_tb/outputs == top_tb/outputs_mod} out_identical
add wave -position end  sim:/top_tb/out_identical

virtual signal {top_tb/key == "00110000010100011100000000111100010110101100101011100110011001101100010010110000110110110100110011100000001011011110110110100011"} key_valid
add wave -position end  sim:/top_tb/key_valid

virtual signal {(top_tb/key_valid && out_identical) == key_valid} OK
add wave -position end  sim:/top_tb/OK

run 100000 ns

wave zoom full