LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY AW_decoder IS

  PORT (
    formatted_AW : IN  STD_LOGIC_VECTOR(0 TO 127);
    AW           : OUT STD_LOGIC_VECTOR(0 TO 12));

END ENTITY AW_decoder;

ARCHITECTURE rtl OF AW_decoder IS

  SIGNAL locking_0 : STD_LOGIC;
  SIGNAL locking_1 : STD_LOGIC;

BEGIN

  -- Formatted AW: 00110000010100011100000000111100010110101100101011100110011001101100010010110000110110110100110011100000001011011110110110100011
  --           AW: 1011111011001

  -- Mask ORs
  AW(10) <= formatted_AW(104) OR formatted_AW(100) OR formatted_AW(94) OR formatted_AW(68) OR formatted_AW(110) OR formatted_AW(121) OR formatted_AW(78) OR formatted_AW(125) OR formatted_AW(102) OR formatted_AW(70) OR formatted_AW(124);
  AW(11) <= formatted_AW(66) OR formatted_AW(99) OR formatted_AW(101) OR formatted_AW(71) OR formatted_AW(105) OR formatted_AW(95) OR formatted_AW(107) OR formatted_AW(67) OR formatted_AW(103) OR formatted_AW(82);
  AW(7) <= formatted_AW(91) OR formatted_AW(88) OR formatted_AW(90) OR formatted_AW(85) OR formatted_AW(76) OR formatted_AW(123) OR formatted_AW(73) OR formatted_AW(118) OR formatted_AW(115) OR formatted_AW(77) OR formatted_AW(79);

  -- Mask ANDs
  AW(8) <= formatted_AW(114) AND formatted_AW(126) AND formatted_AW(86) AND formatted_AW(116) AND formatted_AW(75) AND formatted_AW(87);
  AW(9) <= formatted_AW(111) AND formatted_AW(84) AND formatted_AW(108) AND formatted_AW(122) AND formatted_AW(127) AND formatted_AW(81);
  AW(12) <= formatted_AW(92) AND formatted_AW(113) AND formatted_AW(72) AND formatted_AW(97) AND formatted_AW(69) AND formatted_AW(89);
  AW(5) <= formatted_AW(112) AND formatted_AW(93) AND formatted_AW(109) AND formatted_AW(80) AND formatted_AW(106) AND formatted_AW(98) AND formatted_AW(119);
  AW(6) <= formatted_AW(64) AND formatted_AW(83) AND formatted_AW(120) AND formatted_AW(117) AND formatted_AW(96) AND formatted_AW(65) AND formatted_AW(74);

  -- Lock AND
  locking_1 <= formatted_AW(2) AND formatted_AW(3) AND formatted_AW(9) AND formatted_AW(11) AND formatted_AW(15) AND formatted_AW(16) AND formatted_AW(17) AND formatted_AW(26) AND formatted_AW(27) AND formatted_AW(28) AND formatted_AW(29) AND formatted_AW(33) AND formatted_AW(35) AND formatted_AW(36) AND formatted_AW(38) AND formatted_AW(40) AND formatted_AW(41) AND formatted_AW(44) AND formatted_AW(46) AND formatted_AW(48) AND formatted_AW(49) AND formatted_AW(50) AND formatted_AW(53) AND formatted_AW(54) AND formatted_AW(57) AND formatted_AW(58) AND formatted_AW(61) AND formatted_AW(62);
  AW(0) <= locking_1;
  AW(2) <= locking_1;
  AW(3) <= locking_1;
  AW(4) <= locking_1;

  -- Lock OR
  locking_0 <= formatted_AW(0) OR formatted_AW(1) OR formatted_AW(4) OR formatted_AW(5) OR formatted_AW(6) OR formatted_AW(7) OR formatted_AW(8) OR formatted_AW(10) OR formatted_AW(12) OR formatted_AW(13) OR formatted_AW(14) OR formatted_AW(18) OR formatted_AW(19) OR formatted_AW(20) OR formatted_AW(21) OR formatted_AW(22) OR formatted_AW(23) OR formatted_AW(24) OR formatted_AW(25) OR formatted_AW(30) OR formatted_AW(31) OR formatted_AW(32) OR formatted_AW(34) OR formatted_AW(37) OR formatted_AW(39) OR formatted_AW(42) OR formatted_AW(43) OR formatted_AW(45) OR formatted_AW(47) OR formatted_AW(51) OR formatted_AW(52) OR formatted_AW(55) OR formatted_AW(56) OR formatted_AW(59) OR formatted_AW(60) OR formatted_AW(63);
  AW(1) <= locking_0;

END ARCHITECTURE rtl;
