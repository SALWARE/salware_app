import re
import hashlib
import os

def identify_interface_signals(design_to_wrap):
    
    """Identify inputs and outputs of a VHDL design"""

    inputs = []
    outputs = []

    with open(design_to_wrap, "r") as design_to_wrap_file:
        no_entity_found = True
        for line in design_to_wrap_file:
            if "ARCHITECTURE" in line:
                break #ENTITY is finished
            if no_entity_found:
                if "ENTITY" in line.upper():
                    entity_name = line.split("ENTITY")[1].split("IS")[0].strip()
                    no_entity_found = False
            if "STD_LOGIC" in line.upper():
                if "VECTOR" in line.upper():
                    vector_range = re.search('\([0-9]* DOWNTO [0-9]*\)', line.upper()).group(0)[1:-1].replace(" ", "").split("DOWNTO")
                    if "IN " in line.upper():
                        inputs.append([line.split(":")[0], vector_range])
                    elif "OUT " in line.upper():
                        outputs.append([line.split(":")[0], vector_range])
                else:
                    if "IN " in line.upper():
                        if "," in line.split(":")[0]:
                            inputs.extend([[i, 0] for i in line.split(":")[0].split(",")])
                        else:
                            inputs.append([line.split(":")[0], 0])
                    elif "OUT " in line.upper():
                        if "," in line.split(":")[0]:
                            outputs.extend([[i, 0] for i in line.split(":")[0].split(",")])
                        else:
                            outputs.append([line.split(":")[0], 0])
    inputs = [[i[0].strip(), i[1]] for i in inputs]
    outputs = [[i[0].strip(), i[1]] for i in outputs]
    return entity_name, inputs, outputs

def generate_wrapper(design_to_wrap, ref_design_file_name, wrapped_design_name, target_file_name):

    """Wrapps a vhd design in a SALWARE wrapper"""
    
    entity_name, inputs, outputs = identify_interface_signals(design_to_wrap)
    
    available_inputs = 1024
    available_outputs = 1024
    current_input_index = 0
    current_output_index = 0
    
    output_width = sum([1 for i in outputs if i[1] == 0])
    all_ranges = [i[1] for i in outputs if type(i[1]) is list]
    output_width += sum([abs(int(a) - int(b))+1 for [a, b] in all_ranges])
    if output_width > 128:
        raise ValueError("Cannot output more than 128 bits")
    

    with open(ref_design_file_name, "r") as ref_design_file, open(target_file_name, "w") as target_file:
        for line in ref_design_file:
            if "$$COMB_DATA_IN$$" in line:
                for i in inputs:
                    if i[1] == 0: #1-bit input
                        target_file.write("      "+i[0]+" => Data_to_DB("+str(current_input_index)+"),\n")
                        current_input_index += 1
                        if current_input_index > available_inputs:
                            raise ValueError("I cannot handle so many inputs! There are more than "+str(available_inputs)+" of them!")
                    elif type(i[1]) is list: #n-bit input
                        if i[0] == "key":
                            target_file.write("      key => key,\n")
                        else:
                            range = abs(int(i[1][1]) - int(i[1][0]))
                            target_file.write("      "+i[0]+" => Data_to_DB("+
                            str(current_input_index + range)+
                            " DOWNTO "+
                            str(current_input_index)+"),\n")
                            current_input_index += range+1
                            if current_input_index > available_inputs:
                                raise ValueError("I cannot handle so many inputs! There are more than "+str(available_inputs)+" of them!")
            elif "$$COMB_DATA_OUT$$" in line:
                for i in outputs:
                    if i[1] == 0: #1-bit output
                        if i == outputs[-1]: #last output
                            target_file.write("      "+i[0]+" => comb_out("+str(current_output_index)+"));\n")
                        else:
                            target_file.write("      "+i[0]+" => comb_out("+str(current_output_index)+"),\n")
                            current_output_index += 1
                            if current_output_index > available_outputs:
                                raise ValueError("I cannot handle so many outputs! There are more than "+str(available_outputs)+" of them!")
                    elif type(i[1]) is list: #n-bit output
                        range = abs(int(i[1][1]) - int(i[1][0]))
                        if i == outputs[-1]: #last output
                            target_file.write("      "+i[0]+" => comb_out("+
                            str(current_output_index + range)+
                            " DOWNTO "+
                            str(current_output_index)+"));\n")
                        else:
                            target_file.write("      "+i[0]+" => comb_out("+
                            str(current_output_index + range)+
                            " DOWNTO "+
                            str(current_output_index)+"),\n")
                        current_output_index += range+1
                        if current_output_index > available_outputs:
                            raise ValueError("I cannot handle so many outputs! There are more than "+str(available_outputs)+" of them!")
            elif "$$COMB_OUT_WIDTH$$" in line:
                target_file.write(line.replace("$$COMB_OUT_WIDTH$$", str(output_width-1)))
            elif "$$PADDED_COMB_OUT$$" in line:
                target_file.write(line.replace("$$PADDED_COMB_OUT$$", "\""+(128-output_width)*"0"+"\" & comb_out"))
            elif "$$ENTITY_NAME$$" in line:
                target_file.write(line.replace("$$ENTITY_NAME$$", entity_name))
            elif "$$WRAPPED_DESIGN_NAME$$" in line:
                target_file.write(line.replace("$$WRAPPED_DESIGN_NAME$$", wrapped_design_name))
            else:
                target_file.write(line)
            
    
if __name__ == "__main__":
    # generate_wrapper("test.vhd", "ref_design.vhd", "top", "top.vhd")
    # generate_wrapper("c432_activable.vhd", "ref_design.vhd", "top", "top.vhd")
    # generate_wrapper("multiplier.vhd", "ref_design.vhd", "top", "top.vhd")
    generate_wrapper("multiplier_activable.vhd", "ref_design.vhd", "top", "top.vhd")
