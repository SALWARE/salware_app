# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: response_converter.py
# Date: 2016-11-04

import unittest
from split import split

def response_converter_to_hex(response):
    
    """Convert a response from binary list to hexadecimal string
    
    >>> response_converter_to_hex([0, 1, 0, 1])
    '5'
    >>> response_converter_to_hex([1, 1, 1, 1, 0, 1, 1, 0, 1, 0])
    '3DA'
    """

    if type(response) != list:
        raise TypeError("Response must be a list")
    elif set(response) != {0, 1}:
        raise ValueError("The response list contains non-binary values")
    temp_response = split(response, 4, start="tail")
    temp_response = ["".join(map(str, x)) for x in temp_response]
    return "".join(map(str, [hex(int(x, 2))[2:].upper() for x in temp_response]))
    
def response_converter_to_bin_list(response):

    """Convert a response from hexadecimal string to binary list
    
    >>> response_converter_to_bin_list("A")
    [1, 0, 1, 0]
    >>> response_converter_to_bin_list("45")
    [0, 1, 0, 0, 0, 1, 0, 1]
    """
    
    if type(response) != str:
        print type(response)
        raise TypeError("Response must be a string")
    if not set(response) <= {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"}:
        raise TypeError("Response contains non-hexadecimal characters")
    temporary_response = ""
    for i in response:
        binary = bin(int(i, 16))[2:]
        temporary_response+=((4-len(binary))*"0"+binary)
    return [int(i) for i in temporary_response]

