# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: split.py
# Date : 2016-10-12

from copy import deepcopy

def split(m, size, start="head"):
    
    """Split a list into blocks of equal size
    starting from the head or the tail

    >>> split([0, 1, 0, 1], 2)
    [[0, 1], [0, 1]]
    >>> split([0, 1, 0, 1, 0, 1, 0, 1], 4)
    [[0, 1, 0, 1], [0, 1, 0, 1]]
    
    """

    if start=="head":
        m_ret = m
        for i in range(int(len(m)/size)):
            m_ret.append([m_ret.pop(0) for w in range(int(size))])
        temp = []
        while type(m_ret[0]) != list:
            temp.append(m_ret.pop(0))
        if temp:
            m_ret+=[temp]
        return m_ret
    elif start=="tail":
        m_temp = deepcopy(m)
        m_ret = []
        while len(m_temp) > size:
            temp = []
            for _ in range(size):
                temp.append(m_temp.pop())
            m_ret.append(list(reversed(temp)))
        return [m_temp]+list(reversed(m_ret))

if __name__ == "__main__":
    print split([1, 2, 3, 4, 5, 6, 7, 8], 3, start="tail")
    print split([1, 2, 3, 4, 5, 6, 7, 8], 3, start="head")
    print split([1, 2, 3, 4, 5, 6, 7, 8], 4, start="tail")
    print split([1, 2, 3, 4, 5, 6, 7, 8], 4, start="head")
    print split([1, 2, 3, 4, 5, 6, 7, 8], 2, start="tail")
    print split([1, 2, 3, 4, 5, 6, 7, 8], 2, start="head")
