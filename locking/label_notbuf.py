# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: label_notbuf.py
# Date: 2016-10-13


def label_notbuf(self):

    """Labelling the graph according to buffers and inverters"""

    for i in self.es:
        i_source = i.source
        i_target = i.target
        v_target_locks = self.vs.find(i_target)["locks"]
        v_source_forced = self.vs.find(i_source)["forced"]
        e_label = i["name"]

        if e_label == "buf":
            self.vs.find(i_source)["locks"] = list(set(v_target_locks))
            self.vs.find(i_target)["forced"] = v_source_forced

        elif e_label == "not":
            if v_target_locks in [[0], [1]]:
                self.vs.find(i_source)["locks"].append(v_target_locks[0] ^ 1)
                self.vs.find(i_source)["locks"] = list(set(self.vs.find(i_source)["locks"]))
            elif v_target_locks == [0, 1]:
                self.vs.find(i_source)["locks"].append(0)
                self.vs.find(i_source)["locks"].append(1)
                self.vs.find(i_source)["locks"] = list(set(self.vs.find(i_source)["locks"]))
            if v_source_forced:
                self.vs.find(i_target)["forced"] = [v_source_forced[0] ^ 1]

    return self
