# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: label_nonlin.py
# Date: 2016-10-13


def label_nonlin(self):

    """Labelling the graph according to non-linear (AND/NAND/OR/NOR) gates"""

    lock = {"and": [0, 0],
            "or": [1, 1],
            "nand": [0, 1],
            "nor": [1, 0]}

    for i in self.es:
        i_source = i.source
        i_target = i.target
        v_source_locks = self.vs.find(i_source)["locks"]
        func = i["name"]

        if func in ["and", "or", "nand", "nor"]:
            if v_source_locks == []:
                self.vs.find(i_source)["locks"] = [lock[func][0]]
            else:
                v_source_locks.append(lock[func][0])
                self.vs.find(i_source)["locks"] = list(set(v_source_locks))
            self.vs.find(i_target)["forced"] = [lock[func][1]]

    return self
