# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: label_undefined_nodes.py
# Date: 2016-10-13

import random
import label_notbuf


def label_undefined_nodes(self, list_nodes_to_lock):

    """Label undefined graph nodes"""

    problematic_nodes = []
    problematic_clusters = []

    for i, j in enumerate(list(zip(*list_nodes_to_lock)[1])):
        if j == []:
            problematic_nodes.append(list(zip(*list_nodes_to_lock)[0])[i])

    for i in self.clusters(mode="WEAK"):
        problematic_cluster = False
        for j in i:
            if self.vs[j]["name"] in problematic_nodes:
                problematic_cluster = True
        if problematic_cluster:
            problematic_clusters.append(i)

    for i in problematic_clusters:
        for j in i:
            for k in self.incident(j, mode="IN"):
                if self.es[k]["label"] in ["and", "nand"]:
                    for m in self.vs[j].predecessors():
                        m["locks"] = [0]
                elif self.es[k]["label"] in ["or", "nor"]:
                    for m in self.vs[j].predecessors():
                        m["locks"] = [1]

    label_notbuf.label_notbuf(self)

    for i in problematic_clusters:
        for j in i:
            if self.vs[j]["cat"] == "lock" and self.vs[j]["locks"] == []:
                rand_bit = [random.randrange(0, 2)]
                self.vs[j]["locks"] = rand_bit

    for i in problematic_nodes:
        list_nodes_to_lock[[x[0] for x in list_nodes_to_lock].index(i)][1] = self.vs.find(i)["locks"]

    for i in list_nodes_to_lock:
        i[1] = [i[1][0]]

    return self, list_nodes_to_lock
