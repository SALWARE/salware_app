# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: locking.py
# Date: 2016-10-13

import clean
import label_all
import find_optimal_nodes
import label_undefined_nodes
import modify_nodes_lock
import math

# import sys
# sys.path.append("../parsers/")

# import build_bench
# import build_blif
# import build_edif
# import build_slif
# import build_verilog_rtl
# import build_verilog_struct
# import build_vhd_rtl
# import build_vhd_struct
# import build_xilinx


def locking(g, prim_in, prim_out, nodes, overhead):

    """Implementation of logic locking"""

    h = g.copy()
    g = clean.clean(g)
    g = label_all.label_all(g, prim_out)
    list_nodes_to_lock = find_optimal_nodes.find_optimal_nodes(g, prim_out)
    list_nodes_to_lock = [list(elem) for elem in list_nodes_to_lock]

    g, list_nodes_to_lock = label_undefined_nodes.label_undefined_nodes(g, list_nodes_to_lock)
    
    message = ""
    
    if len(list_nodes_to_lock) > (float(overhead)/100) * len(nodes):
        message = "the overhead is too low to allow total locking, some outputs will remain unaltered."
        list_nodes_to_lock = list_nodes_to_lock[:int(math.floor(float(overhead)*len(nodes)/100))]
    locking_key = str(list(reversed(sum(list(zip(*list_nodes_to_lock)[1]),[]))))[1:-1].replace(", ","")

    # print "nodes:", len(nodes)
    # print "nodes to lock:", len(list_nodes_to_lock)
    h, locking_inputs = modify_nodes_lock.modify_nodes_lock(h, list_nodes_to_lock)

    # if len(locking_key) != len(list_nodes_to_lock):
        # raise Exception("Undefined V_locks, cannot generate the modified netlist.")
    return h, locking_key, locking_inputs, list_nodes_to_lock, message

if __name__ == "__main__":
    for name in ["c432"]:
        g, prim_in, prim_out, nodes = build_bench.build(name)
        _, b = locking(g, name, prim_in, prim_out, nodes, 0.05)
        print b
