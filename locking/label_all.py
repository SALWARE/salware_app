# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: label_all.py
# Date: 2016-10-13

import label
import find_sequences
import erase_labels
import random


def label_all(self, prim_out):

    """Label all graph nodes"""

    self = label.label(self)
    self = find_sequences.find_sequences(self, prim_out)
    size = 0
    while size != len(self.vs) + len(self.es):
        self = erase_labels.erase_labels(self, prim_out)
        self = label.label(self)
        self = find_sequences.find_sequences(self, prim_out)
        size = len(self.vs) + len(self.es)

    for i in self.vs:
        i["locks"] = []
        i["forced"] = []
    self = label.label(self)

    for i in self.clusters(mode="WEAK"):
        if len(i) == 2:
            for j in i:
                self.vs[j]["locks"] = [random.randrange(0, 2)]

    return self
