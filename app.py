# -*- coding: utf-8 -*-

# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: app.py
# Date: 2016-10-13

import igraph.vendor.texttable

import random

import tkFileDialog
from Tkinter import *
from ttk import *

from add_frame_elements           import add_frame_elements
from build_menu                   import build_menu
from build_tabs                   import build_tabs
from declare_initialize_variables import declare_initialize_variables
from declare_pack_frames          import declare_pack_frames
from status_bar                   import status_bar

from key_derivation.blake2 import BLAKE2s

from locking.locking import locking

from masking.masking import masking

from parsers.build_bench          import build as build_bench
from parsers.build_blif           import build as build_blif
from parsers.build_edif           import build as build_edif
from parsers.build_slif           import build as build_slif
from parsers.build_verilog_df     import build as build_verilog_df
from parsers.build_verilog_struct import build as build_verilog_struct
from parsers.build_vhd_df         import build as build_vhd_df
from parsers.build_vhd_struct     import build as build_vhd_struct
from parsers.build_xilinx         import build as build_xilinx

from generate_modified_netlist.convert_back_bench import convert_back as convert_back_bench
from generate_modified_netlist.convert_back_vhd   import convert_back as convert_back_vhd

from boards_management import board_commands

from CASCADE.cascade import cascade

from response_converter.response_converter import response_converter_to_hex
from response_converter.response_converter import response_converter_to_bin_list

from AW_decoder.gen_AW_decoder import gen_AW_decoder

from activable_design.generate_activable_design import generate_activable_design

from OTP.otp import otp

from wrapper.generate_wrapper import generate_wrapper

class App:

    """Main class for the demonstrator GUI app"""

    def __init__(self, master):

        self.master = master

        self.master.maxsize(width=1184, height=540)
        self.master.minsize(width=1184, height=540)
        self.master.resizable(width=False, height=False)
        self.master.title("SALWARE IP protection tool")
        self.master.iconbitmap(default='./contents/icon.ico')

        self.tcl_obj = Tcl()
        self.board_manager = board_commands.Board_manager()

        # Tabs
        build_tabs(self)

        # Menu bar
        build_menu(self)

        # Variables
        declare_initialize_variables(self)

        # Frames
        declare_pack_frames(self)

        # Frame elements
        add_frame_elements(self)

        # Status bar
        status_bar(self)

        # Init frame elements
        # Logic modifier frame
        self.setState(self.modify_design_frame, state = "disabled")
        self.setState(self.generate_save_modified_design_frame, state = "disabled")
        self.setState(self.wrap_modified_design_frame, state = "disabled")
        # Activation frame
        # self.setState(self.CASCADE_frame, state = "enabled")
        # self.setState(self.key_derivation_frame, state = "enabled")
        # self.setState(self.activation_word_encryption_frame, state = "enabled")
        # self.setState(self.activation_process_frame, state = "enabled")
        # Enrolment frame
        # self.setState(self.get_PUF_response_frame, state = "enabled")

    def select_file(self):
        self.update_status("")

        ftypes = [("All netlist files", ".txt; .blif; .slif; .edf; .vhd; .v"),
                  ("BENCH files", ".txt"),
                  ("BLIF files", ".blif"),
                  ("SLIF files", ".slif"),
                  ("EDIF files", ".edf"),
                  ("Xilinx EDIF files", ".edf"),
                  ("Dataflow VHDL files", ".vhd"),
                  ("Structural VHDL files", ".vhd"),
                  ("Dataflow Verilog files", ".v"),
                  ("Structural Verilog files", ".v")]
        self.filename.set(tkFileDialog.askopenfilename(initialdir = "./user_space/",
                                                       filetypes=ftypes))
        if self.filename.get() == "":
            self.filename.set("No design loaded")
        else:
            if self.filename.get().split(".")[-1] == "txt":
                self.design_format.set("BENCH")
            elif self.filename.get().split(".")[-1] == "blif":
                self.design_format.set("BLIF")
            elif self.filename.get().split(".")[-1] == "slif":
                self.design_format.set("SLIF")
            elif self.filename.get().split(".")[-1] == "edf":
                self.design_format.set("EDIF")
            elif self.filename.get().split(".")[-1] == "vhd":
                self.design_format.set("VHDL Dataflow")
            elif self.filename.get().split(".")[-1] == "v":
                self.design_format.set("Verilog Dataflow")
            self.build_graph_button.configure(state = "normal")

    # def select_modified_design_to_wrap(self):
        # self.update_status("")

        # ftypes = [("VHDL files", ".vhd")]
        # self.modified_design_to_wrap.set(tkFileDialog.askopenfilename(initialdir = "./user_space/",
                                                       # filetypes=ftypes))

    def build_graph(self):
        self.update_status("")
        self.graph_info.set("")
        self.update_status("Building the graph from file "+self.filename.get().split("/")[-1]+"...")
        self.master.config(cursor="wait")
        self.graph_info_label.configure(foreground="black")
        self.master.update()
        try:
            if self.design_format.get() == "BENCH":
                self.g, self.prim_in, self.prim_out, self.nodes = build_bench(self.filename.get())
            elif self.design_format.get() == "BLIF":
                self.g, self.prim_in, self.prim_out, self.nodes = build_blif(self.filename.get())
            elif self.design_format.get() == "SLIF":
                self.g, self.prim_in, self.prim_out, self.nodes = build_slif(self.filename.get())
            elif self.design_format.get() == "EDIF":
                self.g, self.prim_in, self.prim_out, self.nodes = build_edif(self.filename.get())
            elif self.design_format.get() == "Xilinx EDIF":
                self.g, self.prim_in, self.prim_out, self.nodes = build_xilinx(self.filename.get())
            elif self.design_format.get() == "VHDL Dataflow":
                self.g, self.prim_in, self.prim_out, self.nodes = build_vhd_df(self.filename.get())
            elif self.design_format.get() == "VHDL Structural":
                self.g, self.prim_in, self.prim_out, self.nodes = build_vhd_struct(self.filename.get())
            elif self.design_format.get() == "Verilog Dataflow":
                self.g, self.prim_in, self.prim_out, self.nodes = build_verilog_df(self.filename.get())
            elif self.design_format.get() == "Verilog Structural":
                self.g, self.prim_in, self.prim_out, self.nodes = build_verilog_struct(self.filename.get())
            self.graph_info.set(str(str(len(self.nodes))+" nodes, "+
                                    str(len(self.prim_in))+" inputs, "+
                                    str(len(self.prim_out))+" outputs."))
            self.update_status("Graph built")
            self.setState(self.modify_design_frame, state = "normal")
        except:
            self.update_status("Error in building the graph")
        # self.setState(self.generate_save_modified_design_frame, state = "enabled")
        # self.setState(self.wrap_modified_design_frame, state = "enabled")
        self.master.config(cursor="")
        self.master.update()

    def open_server_reference_response(self):
        self.update_status("")
        rrtypes = [("Reference response file", ".txt")]
        self.server_reference_response_file.set(tkFileDialog.askopenfilename(initialdir = "./user_space/",
                                                                             filetypes=rrtypes))
        with open(self.server_reference_response_file.get(), "r") as rrfile:
            line = rrfile.readline()
            if line[:20] != "Reference response: ":
                self.update_status("The file could not be parsed as a Reference Response file")
            else:
                self.server_reference_response = str(line[20:])
                self.server_reference_response_displayed.set(self.server_reference_response)
                self.server_reference_response = response_converter_to_bin_list(self.server_reference_response)#[::-1]
                self.update_status("")
                if self.connected.get() == True:
                    self.setState(self.CASCADE_frame, state = "normal")
        
    def open_activation_word(self):
        self.update_status("")
        awtypes = [("Activation word", ".txt")]
        self.activation_word_file = tkFileDialog.askopenfilename(initialdir = "./../User_space/",
                                                                 filetypes=awtypes)
        with open(self.activation_word_file, "r") as awfile:
            line = awfile.readline()
            line = awfile.readline()  # Get the second line
            if not set(line) <= {"0", "1"}:
                self.update_status("The file could not be parsed as an Activation Word file")
            else:
                self.activation_word.set(line)
                self.encrypt_activation_word_button.configure(state = "enabled")

    # def open_associated_activation_word(self):
        # self.update_status("")
        # awtypes = [("Activation word file", ".txt")]
        # self.associated_activation_word_file = tkFileDialog.askopenfilename(initialdir = "./../User_space/",
                                                                            # filetypes=awtypes)
        # with open(self.associated_activation_word_file, "r") as awfile:
            # line = awfile.readline()  # First line of the file
            # line = awfile.readline()  # Get the second line
            # if not set(line) <= {"0", "1", " ", "\n"}:
                # self.update_status("The file could not be parsed as an Activation Word file")
            # else:
                # self.associated_activation_word.set(line)
                # if self.modified_design_to_wrap.get() != "No design loaded":
                    # self.generate_wrapped_design_button.configure(state = "normal")

    def round_masking(self, _):  # Dummy second argument
        self.masking_overhead.set(int(round(self.masking_overhead.get())))

    def round_locking(self, _):  # Dummy second argument
        self.locking_overhead.set(int(round(self.locking_overhead.get())))

    def get_PUF_response(self):
        self.update_status("")
        # self.PUF_response_displayed.set("")
        self.tcl_obj.eval(self.board_manager.reset_boards(port=2))
        status, temp_hex_response = self.tcl_obj.eval(self.board_manager.generate_response()).split(" ")
        if status != "CAFEBABE":
            self.server_reference_response_displayed.set("")
            self.update_status("Response could not be obtained")
            self.PUF_response_displayed.set("")
        else:
            self.PUF_response = list(reversed(response_converter_to_bin_list(temp_hex_response)))
            self.PUF_response_displayed.set(response_converter_to_hex(self.PUF_response))
            self.PUF_response_label.configure(state = "enabled")
            self.save_as_reference_response_button.configure(state = "enabled")

    def save_as_reference_response(self):
        self.update_status("")
        with open("./user_space/rr.txt", "w") as rrfile:
            rrfile.write("Reference response: "+self.PUF_response_displayed.get())
            # rrfile.write(self.PUF_response_displayed.get()+"\n")
            self.update_status("Reference response saved under ./user_space/rr.txt")

    def pop_up_about(self):
        self.update_status("")
        self.top = Toplevel(background="White")
        self.top.title("About")

        self.msg = Label(self.top, text="This software was realised in the frame of\n\
the SALWARE project number ANR-13-JS03-0003\n\
supported by the French \"Agence Nationale de\n\
la Recherche\" and by the French \"Fondation de\n\
Recherche pour l'Aéronautique et l'Espace\",\n\
funding for this project was also provided by\n\
a grant from \"La Région Rhône-Alpes\".", background="white")
        self.msg.pack()
        self.salware_logo = PhotoImage(file="./contents/Logo_SALWARE.gif")

        self.label_salware_logo = Label(self.top,
                                        image=self.salware_logo,
                                        background="white")
        self.label_salware_logo.image = self.salware_logo # keep a reference!
        self.label_salware_logo.pack()
        self.logo_RA = PhotoImage(file="./contents/Logo_RA.gif")
        self.label_logo_RA = Label(self.top,
                                   image=self.logo_RA,
                                   background="white")
        self.label_logo_RA.image = self.logo_RA # keep a reference!
        self.label_logo_RA.pack()
        
    def pop_up_license(self):
        self.update_status("")
        self.top = Toplevel()
        self.top.title("License")
        try:
            self.License_file = open("LICENSE", 'r')
            self.License_text = self.License_file.read()
            self.License_file.close()
        except:
            self.update_status("Cannot find LICENSE.txt")
        self.msg = Label(self.top,
                         text=self.License_text,
                         background="white")
        self.msg.pack()

    def connect(self):
        self.update_status("")
        self.tcl_obj.eval(self.board_manager.source_tcl_package())
        tcl_return = self.tcl_obj.eval(self.board_manager.connect(self.com_port.get()))
        if tcl_return == "-1": # Error handling
            self.update_status("Could not connect to the board")
            self.connected.set(False)
        else: # Connection successful
            self.com_port_button_connect.configure(state="disabled")
            self.com_port_button_reset.configure(state="normal")
            self.com_port_button_disconnect.configure(state="normal")
            self.connected.set(True)
            self.board_status.set("Board Connected")
            self.board_status_label.configure(foreground="darkgreen")
            self.update_status("")
            self.get_PUF_response_button.configure(state="enabled")
            self.load_server_reference_response_button.configure(state="enabled")
            self.setState(self.load_server_reference_response_frame, state = "enabled")

    def disconnect(self):
        self.update_status("")
        try:
            self.tcl_obj.eval(self.board_manager.disconnect())
            self.com_port_button_disconnect.configure(state="disabled")
            self.com_port_button_reset.configure(state="disabled")
            self.com_port_button_connect.configure(state="normal")
            self.connected.set(False)
            self.board_status.set("Board not connected")
            self.board_status_label.configure(foreground="red")
            self.setState(self.get_PUF_response_frame, state = "disabled")
            self.setState(self.tab_activation, state = "disabled")
            self.server_reference_response_displayed.set("")
            self.activation_word.set("")
        except:
            pass

    def modify_design(self):
        self.update_status("")
        self.associated_activation_word = ""
        self.update_status("Modifying the design...")
        self.master.config(cursor="wait")
        self.graph_info_label.configure(foreground="black")
        self.master.update()
        self.locking_inputs = []
        self.masking_inputs = []
        
        message = "" # Locking message, in case the overhead is too low to allow for total locking
        if self.modify_design_mode.get() == "locking":
            self.graph_modified, self.associated_activation_word, self.locking_inputs, list_nodes_to_lock, message = locking(self.g,
                                                                                                                             self.prim_in,
                                                                                                                             self.prim_out,
                                                                                                                             self.nodes,
                                                                                                                             self.locking_overhead.get())
        elif self.modify_design_mode.get() == "masking":
            self.graph_modified, self.associated_activation_word, self.masking_inputs = masking(self.g,
                                                                                                self.prim_in,
                                                                                                self.prim_out,
                                                                                                self.nodes,
                                                                                                self.masking_overhead.get(),
                                                                                                self.masking_heuristic.get())
        self.setState(self.generate_save_modified_design_frame, state = "normal")
        self.save_associated_activation_word_simple_button.configure(state = "enabled")
        self.setState(self.modify_design_frame, state = "enabled")
        if message:
            self.update_status("Modifications done but "+message)
        else:
            self.update_status("Modifications done")
        self.master.config(cursor="")
        self.master.update()

        # print self.locking_inputs
        # print self.masking_inputs

    def derive_key_from_response(self):
        self.update_status("")
        self.salt = bytes(''.join(random.SystemRandom().choice(["0", "1"]) for _ in range(32)))
        PRK_f = BLAKE2s(digest_size=32, key=self.salt)
        PRK_f.update(self.PUF_response_displayed)
        self.key = ''.join('{0:08b}'.format(ord(x), 'b') for x in PRK_f.final()).replace("0b", "")
        key_file_name = "./user_space/key_"+""+".txt"
        with open(key_file_name, "w") as key_file:
            key_file.write("Salt = "+self.salt+"\n")
            key_file.write("Key = "+self.key)
        self.update_status("Key saved under "+key_file_name)

    def generate_save_modified_design(self):
        self.update_status("")
        filename = self.filename.get().split("/")[-1]
        filename = filename.split(".")
        filename[0]+="_mod"
        if self.generated_design_format.get() == "vhd":
            filename[1] = "vhd"
            filename = ".".join(filename)
            filename = "/".join(self.filename.get().split("/")[:-1])+"/"+filename
            convert_back_vhd(self.graph_modified, filename)
        elif self.generated_design_format.get() == "bench":
            filename[1] = "txt"
            filename = ".".join(filename)
            filename = "/".join(self.filename.get().split("/")[:-1])+"/"+filename
            convert_back_bench(self.graph_modified, filename)
        self.update_status(str("Modified design saved under ./user_space/"+filename.split("/")[-1]))
        self.save_associated_activation_word_simple_button.configure(state = "normal")
        self.generate_modified_design_button.configure(state = "enabled")
        self.path_to_modified_design = self.filename.get()

    def save_associated_AW(self):
        self.update_status("")
        filename = self.filename.get().split("/")[-1]
        filename = filename.split(".")
        filename[0]+="_mod_associated_AW"
        filename[1] = "txt"
        filename = ".".join(filename)
        filename = "/".join(self.filename.get().split("/")[:-1])+"/"+filename
        with open(filename, "w") as aw_file:
            aw_file.write("Activation word\n"+self.associated_activation_word)
        self.update_status(str("Associated activation word saved under ./user_space/"+filename.split("/")[-1]))
        self.setState(self.wrap_modified_design_frame, state = "normal")
        self.select_crypto_PRESENT.configure(state = "disabled")
        self.select_key_derivation_blake2.configure(state = "disabled")
        self.setState(self.generate_save_modified_design_frame, state = "enabled")
        print self.filename.get()
        

    def save_formatted_AW(self):
        self.update_status("")
        filename = self.filename.get().split("/")[-1]
        print "Filename:", filename
        filename = filename.split(".")
        print "Filename:", filename
        filename[0]+="_formatted_AW"
        print "Filename:", filename
        filename[1] = "txt"
        print "Filename:", filename
        filename = ".".join(filename)
        print "Filename:", filename
        filename = "/".join(self.filename.get().split("/")[:-1])+"/"+filename
        print "Filename:", filename
        # print filename
        with open(filename, "w") as aw_file:
            aw_file.write("Formatted Activation Word\n"+self.formatted_AW)
        self.update_status(str("Formatted activation word saved under "+filename))

    def generate_save_wrapped_design(self):
        # Inputs:
        #   -> modified_design_to_wrap
        #   -> associated_activation_word
        

        self.update_status("")
        self.master.config(cursor="wait")
        self.graph_info_label.configure(foreground="black")
        self.master.update()
        # print self.associated_activation_word.get()[-1]
        if self.modify_design_mode.get() == "locking":
            locking = True
            masking = False
        elif self.modify_design_mode.get() == "masking":
            locking = False
            masking = True
        try: #Generate AW
            # print self.associated_activation_word.get()
            self.formatted_AW = gen_AW_decoder(self.associated_activation_word,
                                               128, #128-bit responses
                                               locking = locking,
                                               masking = masking,
                                               path = "./user_space/")
        except:
            print "An error occured while generating the AW decoder"
            self.update_status("An error occured while generating the AW decoder")
        try: #Generate activable design
            generate_activable_design("./user_space/"+self.filename.get().split("/")[-1].split(".")[0]+"_mod.vhd",
                                      "./user_space/AW_decoder.vhd",
                                      (locking, self.locking_inputs),
                                      (masking, self.masking_inputs))
        except Exception as e:
            print e
            self.update_status("An error occured while generating the activable design")
        try: #Generate wrapped design
            generate_wrapper("./user_space/"+self.filename.get().split("/")[-1].split(".")[0]+"_activable.vhd",
                             "./wrapper/ref_design.vhd",
                             "db_top_inst",
                             "./user_space/db_top_inst.vhd")
            self.update_status("Wrapped design generated successfully and saved under user_space/db_top_inst.vhd")
        except Exception as e:
            print e
            self.update_status("An error occured while generating the wrapped design")
        print self.formatted_AW
        self.master.config(cursor="")
        self.master.update()

    def perform_reconciliation(self):
        self.update_status("")
        self.tcl_obj.eval(self.board_manager.reset_boards(port=2))
        print self.tcl_obj.eval(self.board_manager.generate_response())
        server_before = self.server_reference_response
        self.server_reference_response, leakage = cascade(self.server_reference_response,
                                                          0.02,
                                                          int(self.reconciliation_parameter_number_of_passes_spinbox.get()),
                                                          self.tcl_obj,
                                                          self.board_manager,
                                                          int(self.reconciliation_parameter_initial_block_size_spinbox.get()))
        if self.server_reference_response:
            
            server_after = self.server_reference_response
            print "Errors corrected :", [a for (a, (b, c)) in enumerate(zip(server_before, server_after)) if b != c]
            self.update_status("Reconciliation done, "+str(leakage)+" bits leaked", color="darkgreen")
            self.setState(self.activation_word_encryption_frame, state = "enabled")
            self.encrypt_activation_word_button.configure(state = "disabled")
        else:
            self.update_status("Reconciliation failed, too much leakage. Try with other parameters.", color="red")

    def encrypt_activation_word(self):
        ref_rep = "".join([str(i) for i in self.server_reference_response])[::-1]
        self.encrypted_AW = otp(self.activation_word.get(), ref_rep) #binary
        self.encrypted_AW = hex(int(self.encrypted_AW, 2))[2:-1].upper()
        encrypted_AW_file_name = self.activation_word_file.replace("formatted", "encrypted")
        with open(encrypted_AW_file_name, "w") as encrypted_AW_file:
            encrypted_AW_file.write(self.encrypted_AW)
        self.update_status("Encrypted AW saved under ./user_space/"+encrypted_AW_file_name.split("/")[-1])
        self.setState(self.activation_process_frame, state = "enabled")
        
    def start_activation_process(self):
        print self.encrypted_AW
        print self.tcl_obj.eval(self.board_manager.activate_design(self.encrypted_AW))
        self.update_status("Activation done", color="darkgreen")

    def update_status(self, text, color = "black"):
        self.status.set(text)
        self.status_label.configure(foreground=color)

    def reset_board(self, port=2):
        self.update_status("")
        self.tcl_obj.eval(self.board_manager.reset_boards(port=2))

    def reset(self):
        declare_initialize_variables(self)
        self.update_status("")
        self.setState(self.modify_design_frame, state = "enabled")
        self.setState(self.generate_save_modified_design_frame, state = "enabled")
        self.setState(self.wrap_modified_design_frame, state = "enabled")
        # Activation frame
        self.setState(self.CASCADE_frame, state = "enabled")
        self.setState(self.key_derivation_frame, state = "enabled")
        self.setState(self.activation_word_encryption_frame, state = "enabled")
        self.setState(self.activation_process_frame, state = "enabled")
        # Enrolment frame
        self.setState(self.get_PUF_response_frame, state = "enabled")

    def setState(self, widget, state='enabled'):
        try:
            widget.configure(state=state)
        except TclError:
            pass
        for child in widget.winfo_children():
            self.setState(child, state=state)
            


if __name__ == "__main__":
        
    fenetre = Tk()
    app = App(fenetre)

    fenetre.mainloop()
