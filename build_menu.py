# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: build_menu.py
# Date: 2016-10-13

from Tkinter import *
from ttk import *


def build_menu(self):
    
    """Build the menu for the main GUI"""

    self.menubar = Menu(self.master)
    
    self.filemenu = Menu(self.menubar, tearoff=0)
    # self.filemenu.add_command(label="Reset", command=self.reset)
    self.filemenu.add_command(label="Exit", command=self.master.quit)
    self.menubar.add_cascade(label="File", menu=self.filemenu)

    self.helpmenu = Menu(self.menubar, tearoff=0)
    self.helpmenu.add_command(label="License", command=self.pop_up_license)
    self.helpmenu.add_command(label="About", command=self.pop_up_about)
    self.menubar.add_cascade(label="Help", menu=self.helpmenu)
    
    self.master.config(menu=self.menubar)
