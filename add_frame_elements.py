# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: add_frame_elements.py
# Date: 2016-10-13

from Tkinter import *
from ttk import *


def add_frame_elements(self):

    """Add elements to frames of the GUI main window"""

    self.com_port_text = Label(self.motherboard_frame,
                               text="COM port (ex: COM3)")
    self.com_port_text.pack(side=LEFT)
    self.com_port_entry = Entry(self.motherboard_frame,
                                width=10,
                                textvariable=self.com_port)
    self.com_port_entry.pack(side=LEFT)
    self.com_port_button_connect = Button(self.motherboard_frame,
                                          text="Connect",
                                          command=self.connect)
    self.com_port_button_connect.pack(side=LEFT)
    self.com_port_button_reset = Button(self.motherboard_frame,
                                             text="Reset",
                                             state="disabled",
                                             command=self.reset_board)
    self.com_port_button_reset.pack(side=LEFT)
    self.com_port_button_disconnect = Button(self.motherboard_frame,
                                             text="Disconnect",
                                             state="disabled",
                                             command=self.disconnect)
    self.com_port_button_disconnect.pack(side=LEFT)
    # ------------------------------------------------------
    self.reconciliation_parameters_label = Label(self.CASCADE_frame,
                                                 state = "disabled",
                                                 text="Parameters:")
    self.reconciliation_parameters_label.grid(row=0, column=0)
    self.reconciliation_parameter_initial_block_size_label = Label(self.CASCADE_frame,
                                                                   state = "disabled",
                                                                   text="Initial block size (bits):")
    self.reconciliation_parameter_initial_block_size_label.grid(row=0,
                                                                column=1)
    self.reconciliation_parameter_initial_block_size_spinbox = Spinbox(self.CASCADE_frame,
                                                                       values=(4, 8, 16, 32, 64),
                                                                       width=3,
                                                                       state="disabled")
    self.reconciliation_parameter_initial_block_size_spinbox.grid(row=0,
                                                                  column=2)
    self.reconciliation_parameter_number_of_passes_label=Label(self.CASCADE_frame,
                                                               state = "disabled",
                                                               text="Number of passes:")
    self.reconciliation_parameter_number_of_passes_label.grid(row=1,
                                                              column=1,
                                                              sticky=E)
    self.reconciliation_parameter_number_of_passes_spinbox=Spinbox(self.CASCADE_frame,
                                                                   from_=1,
                                                                   to=25,
                                                                   width=3,
                                                                   state="disabled")
    self.reconciliation_parameter_number_of_passes_spinbox.grid(row=1,
                                                                column=2)
    self.perform_reconciliation_button=Button(self.CASCADE_frame,
                                              text="Perform reconciliation",
                                              state="disabled",
                                              command=self.perform_reconciliation)
    self.perform_reconciliation_button.grid(row=0,
                                            column=3,
                                            rowspan=2,
                                            padx=10)
    self.load_server_reference_response_label = Label(self.load_server_reference_response_frame,
                                                      state = "disabled",
                                                      text="Reference response on server:")
    self.load_server_reference_response_label.pack(side=LEFT)
    self.reference_response_label=Label(self.load_server_reference_response_frame,
                                        textvariable=self.server_reference_response_displayed,
                                        state = "disabled",
                                        font=("Courier", 8),
                                        background="white")
    self.reference_response_label.pack(side=LEFT, padx=10, fill="x", expand="yes")
    self.load_server_reference_response_button=Button(self.load_server_reference_response_frame,
                                                      state = "disabled",
                                                      text="Search...",
                                                      command=self.open_server_reference_response)
    self.load_server_reference_response_button.pack(side=LEFT)
    self.get_PUF_response_button=Button(self.get_PUF_response_frame,
                                        text="Get PUF response",
                                        command=self.get_PUF_response,
                                        state="disabled")
    self.get_PUF_response_button.grid(row=0, column=0, sticky = "WE")
    self.PUF_response_label=Label(self.get_PUF_response_frame,
                                  textvariable=self.PUF_response_displayed,
                                  font=("Courier", 8),
                                  background="white")
    self.PUF_response_label.grid(row=0, column=1, sticky="WE", padx=10)
    self.save_as_reference_response_button=Button(self.get_PUF_response_frame,
                                                  text="Save as reference response",
                                                  command=self.save_as_reference_response,
                                                  state="disabled")
    self.save_as_reference_response_button.grid(row=1, column=0)
    self.save_as_reference_response_status_label=Label(self.get_PUF_response_frame,
                                                       textvariable=self.save_as_reference_response_status)
    self.save_as_reference_response_status_label.grid(row=1, column=1, sticky="W", padx=10)
    self.derive_key_button=Button(self.key_derivation_frame,
                                  text="Derive a key from the response",
                                  command=self.derive_key_from_response,
                                  state="disabled")
    self.derive_key_button.pack(side=LEFT)
    self.key_saved_label=Label(self.key_derivation_frame,
                               textvariable=self.message_key_saved)
    self.key_saved_label.pack(side=LEFT)
    # ------------------------------------------------------
    self.design_label=Label(self.file_frame,
                            text="Design:")
    self.design_label.grid(row=0, column=0, sticky=E)
    self.format_label=Label(self.file_frame,
                            text="Format:")
    self.format_label.grid(row=1, column=0, sticky=E)
    self.filename_entry=Entry(self.file_frame,
                              textvariable=self.filename,
                              width=100)
    self.filename_entry.grid(row=0, column=1, columnspan=2, sticky="EW")
    self.button_open_design=Button(self.file_frame,
                                     text="Search...",
                                     command=self.select_file)
    self.button_open_design.grid(row=0, column=3)
    self.choose_design_format_option_menu = OptionMenu(self.file_frame,
                                                       self.design_format,
                                                       "BENCH",
                                                       "BENCH", "BLIF", "SLIF", "EDIF", "Xilinx EDIF", "VHDL Dataflow", "VHDL Structural", "Verilog Dataflow", "Verilog Structural")
    self.choose_design_format_option_menu.grid(row=1, column=1, sticky=W)
    self.build_graph_button=Button(self.file_frame,
                                   text="Build graph",
                                   command=self.build_graph,
                                   state="enabled")
    self.build_graph_button.grid(row=2, column=0)
    self.graph_info_label=Label(self.file_frame,
                                textvariable=self.graph_info)
    self.graph_info_label.grid(row=2, column=1, sticky=W)
    # -------------------------------------------------------
    self.locking_button=Radiobutton(self.modify_design_frame,
                                    text="Locking with",
                                    variable=self.modify_design_mode,
                                    value="locking")
    self.locking_button.grid(row=0, column=0, sticky=W)
    self.masking_button=Radiobutton(self.modify_design_frame,
                                    text="Masking with",
                                    variable=self.modify_design_mode,
                                    value="masking")
    self.masking_button.grid(row=1, column=0, sticky=W)
    self.locking_overhead_spinbox=Spinbox(self.modify_design_frame,
                                            from_=1,
                                            to=100,
                                            width=4,
                                            textvariable=self.locking_overhead,
                                            state="normal")
    self.locking_overhead_spinbox.grid(row=0, column=1)
    self.masking_overhead_spinbox=Spinbox(self.modify_design_frame,
                                            from_=1,
                                            to=100,
                                            width=4,
                                            textvariable=self.masking_overhead,
                                            state="normal")
    self.masking_overhead_spinbox.grid(row=1, column=1)
    self.locking_overhead_label=Label(self.modify_design_frame,
                                      text="% area overhead at most")
    self.locking_overhead_label.grid(row=0, column=2, sticky=W)
    self.masking_overhead_label=Label(self.modify_design_frame,
                                      text="% area overhead using heuristic:")
    self.masking_overhead_label.grid(row=1, column=2)
    self.masking_heuristic_radio_button_random=Radiobutton(self.modify_design_frame,
                                                           text="Random",
                                                           variable=self.masking_heuristic,
                                                           value="random")
    self.masking_heuristic_radio_button_random.grid(row=1,
                                                    column=3,
                                                    sticky=W,
                                                    padx=5)
    self.masking_heuristic_radio_button_betweenness=Radiobutton(self.modify_design_frame,
                                                                text="Betweenness",
                                                                variable=self.masking_heuristic,
                                                                value="betweenness")
    self.masking_heuristic_radio_button_betweenness.grid(row=2,
                                                         column=3,
                                                         sticky=W,
                                                         padx=5)
    self.masking_heuristic_radio_button_closeness=Radiobutton(self.modify_design_frame,
                                                              text="Closeness",
                                                              variable=self.masking_heuristic,
                                                              value="closeness")
    self.masking_heuristic_radio_button_closeness.grid(row=3,
                                                       column=3,
                                                       sticky=W,
                                                       padx=5)
    self.masking_heuristic_radio_button_current_flow_betweenness=Radiobutton(self.modify_design_frame,
                                                                             text="Current-flow betweenness",
                                                                             variable=self.masking_heuristic,
                                                                             value="current_flow_betweenness")
    self.masking_heuristic_radio_button_current_flow_betweenness.grid(row=1,
                                                                      column=4,
                                                                      sticky=W,
                                                                      padx=5)
    self.masking_heuristic_radio_button_current_flow_closeness=Radiobutton(self.modify_design_frame,
                                                                           text="Current-flow closeness",
                                                                           variable=self.masking_heuristic,
                                                                           value="current_flow_closeness")
    self.masking_heuristic_radio_button_current_flow_closeness.grid(row=2,
                                                                    column=4,
                                                                    sticky=W,
                                                                    padx=5)
    self.masking_heuristic_radio_button_approximate_current_flow_betweenness=Radiobutton(self.modify_design_frame,
                                                                                           text="Approximate current-flow betweenness",
                                                                                           variable=self.masking_heuristic,
                                                                                           value="approximate_current_flow_betweenness")
    self.masking_heuristic_radio_button_approximate_current_flow_betweenness.grid(row=3,
                                                                                  column=4,
                                                                                  sticky=W,
                                                                                  padx=5)
    self.modify_design_button=Button(self.modify_design_frame,
                                     text="Modify design",
                                     state="normal",
                                     command=self.modify_design)
    self.modify_design_button.grid(row=4, column=0)
    # -------------------------------------------------------
    # self.generate_modified_design_simple_frame = LabelFrame(self.generate_save_modified_design_frame,
    #                                                         text = "Simple")
    # self.generate_modified_design_simple_frame.pack(side="left",
    #                                                 expand=1,
    #                                                 fill="both")
    self.generated_design_format_label = Label(self.generate_save_modified_design_frame,
                                               text="Output format:")
    self.generated_design_format_label.grid(row=0,
                                            column=0,
                                            sticky=E)
    self.generated_design_format_vhd = Radiobutton(self.generate_save_modified_design_frame,
                                                   text="VHDL (.vhd)",
                                                   variable=self.generated_design_format,
                                                   value="vhd")
    self.generated_design_format_vhd.grid(row=0,
                                          column=1,
                                          sticky=W)
    self.generated_design_format_bench = Radiobutton(self.generate_save_modified_design_frame,
                                                     text="BENCH (.txt)",
                                                     variable=self.generated_design_format,
                                                     value="bench")
    self.generated_design_format_bench.grid(row=1,
                                            column=1,
                                            sticky=W)
    self.generate_modified_design_button=Button(self.generate_save_modified_design_frame,
                                                text="Generate and save modified design",
                                                state="normal",
                                                command= self.generate_save_modified_design,
                                                width=32)
    self.generate_modified_design_button.grid(row=2,
                                              column=0,
                                              sticky=W)
    self.modified_design_saved_label = Label(self.generate_save_modified_design_frame,
                                             textvariable=self.message_modified_design_saved)
    self.modified_design_saved_label.grid(row=2,
                                          column=1,
                                          sticky=W)
    self.save_associated_activation_word_simple_button=Button(self.generate_save_modified_design_frame,
                                                              text="Save associated Activation word",
                                                              state="enabled",
                                                              command=self.save_associated_AW,
                                                              width=32)
    self.save_associated_activation_word_simple_button.grid(row=3,
                                                            column=0,
                                                            sticky=W)
    self.activation_word_saved_label = Label(self.generate_save_modified_design_frame,
                                             textvariable=self.message_AW_saved)
    self.activation_word_saved_label.grid(row=3,
                                          column=1,
                                          sticky=W)
    # self.modified_design_to_wrap_label=Label(self.wrap_modified_design_frame,
                                             # text="Design to wrap:")
    # self.modified_design_to_wrap_label.grid(row=0, column=0, sticky=E)
    # self.modified_design_to_wrap_entry=Entry(self.wrap_modified_design_frame,
                                             # width=150,
                                             # textvariable=self.modified_design_to_wrap)
    # self.modified_design_to_wrap_entry.grid(row=0,
                                            # column=1,
                                            # sticky="EW")
    # self.button_open_modified_design_to_wrap=Button(self.wrap_modified_design_frame,
                                                    # text="Search...",
                                                    # command=self.select_modified_design_to_wrap)
    # self.button_open_modified_design_to_wrap.grid(row=0, column=2)
    # self.associated_activation_word_label = Label(self.wrap_modified_design_frame,
                                                  # text="Associated Activation word:")
    # self.associated_activation_word_label.grid(sticky=E)
    # self.associated_activation_word_display_label = Label(self.wrap_modified_design_frame,
                                                          # textvariable=self.associated_activation_word,
                                                          # font=("Courier", 8),
                                                          # background="white",
                                                          # width=20)
    # self.associated_activation_word_display_label.grid(row=1, column=1, sticky="EW", padx=10)

    # self.open_associated_activation_word_wrapped_button=Button(self.wrap_modified_design_frame,
                                                               # text="Search...",
                                                               # state="normal",
                                                               # command=self.open_associated_activation_word)
    # self.open_associated_activation_word_wrapped_button.grid(row=1,
                                                             # column=2,
                                                             # sticky=W)
    
    self.crypto_label = Label(self.wrap_modified_design_frame,
                              text="Crypto:")
    self.crypto_label.grid(row=0,
                           column=0,
                           sticky = E)
    self.select_crypto_OTP = Radiobutton(self.wrap_modified_design_frame,
                                         text="One Time Pad",
                                         variable=self.crypto,
                                         value="OTP")
    self.select_crypto_OTP.grid(row=0,
                                column=1,
                                sticky=W)
    self.select_crypto_PRESENT = Radiobutton(self.wrap_modified_design_frame,
                                             text="PRESENT",
                                             state=DISABLED,
                                             variable=self.crypto,
                                             value="PRESENT")
    self.select_crypto_PRESENT.grid(row=1,
                                    column=1,
                                    sticky=W)
    self.key_derivation_label = Label(self.wrap_modified_design_frame,
                                      text="Key generation:")
    self.key_derivation_label.grid(row=2,
                                   column=0,
                                   sticky = E)
    self.select_key_derivation_none = Radiobutton(self.wrap_modified_design_frame,
                                                  text="None",
                                                  variable=self.key_derivation,
                                                  value="None")
    self.select_key_derivation_none.grid(row=2,
                                         column=1,
                                         sticky=W)
    self.select_key_derivation_blake2 = Radiobutton(self.wrap_modified_design_frame,
                                                    text="Blake2 hashing",
                                                    state=DISABLED,
                                                    variable=self.key_derivation,
                                                    value="Blake2")
    self.select_key_derivation_blake2.grid(row=3,
                                           column=1,
                                           sticky=W)
    self.generate_wrapped_design_button=Button(self.wrap_modified_design_frame,
                                                text="Generate and save wrapped design",
                                                state="normal",
                                                command= self.generate_save_wrapped_design,
                                                width=32)
    self.generate_wrapped_design_button.grid(row=4,
                                             column=0,
                                             sticky=W)
    self.save_associated_activation_word_wrapped_button=Button(self.wrap_modified_design_frame,
                                                               text="Save formatted Activation word",
                                                               state="normal",
                                                               command=self.save_formatted_AW,
                                                               width=32)
    self.save_associated_activation_word_wrapped_button.grid(row=5,
                                                             column=0,
                                                             sticky=W)

    # -------------------------------------------------------
    self.activation_word_label = Label(self.activation_word_encryption_frame,
                                       text="Activation word:",
                                       state = "disabled")
    self.activation_word_label.grid(sticky=E)
    self.activation_word_display_label = Label(self.activation_word_encryption_frame,
                                               textvariable=self.activation_word,
                                               font=("Courier", 8),
                                               background="white")
    self.activation_word_display_label.grid(row=0, column=1, sticky="EW", padx=10)
    self.search_activation_word_button = Button(self.activation_word_encryption_frame,
                                                text="Search...",
                                                state = "disabled",
                                                command=self.open_activation_word)
    self.search_activation_word_button.grid(row=0, column=2)
    self.encrypt_activation_word_button = Button(self.activation_word_encryption_frame,
                                                 text="Encrypt Activation word",
                                                 state = "disabled",
                                                 command = self.encrypt_activation_word)
    self.encrypt_activation_word_button.grid(row=1)
    self.start_activation_process_button = Button(self.activation_process_frame,
                                                  text="Start activation process",
                                                  state = "disabled",
                                                  command=self.start_activation_process)
    self.start_activation_process_button.pack(side=LEFT)
