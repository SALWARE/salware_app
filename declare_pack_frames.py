# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: declare_pack_frames.py
# Date: 2016-10-13

from Tkinter import *
from ttk import *


def declare_pack_frames(self):
    
    """Declare the main GUI frames and pack them"""

    # Tab logic modifier

    self.file_frame = LabelFrame(self.tab_logic_modifier, text="Current design")
    self.file_frame.pack(side=TOP, fill=X)
    self.file_frame.grid_columnconfigure(1, weight=1)
        
    self.modify_design_frame = LabelFrame(self.tab_logic_modifier, text="Modify design")
    self.modify_design_frame.pack(fill="x")
    
    self.generate_save_modified_design_frame = LabelFrame(self.tab_logic_modifier,
                                                     text="Generate modified design")
    self.generate_save_modified_design_frame.pack(fill="x")
    self.wrap_modified_design_frame = LabelFrame(self.tab_logic_modifier,
                                                 text = "Wrap modified design")
    self.wrap_modified_design_frame.pack(fill="x")

    # Tab HECTOR board management
    
    self.motherboard_frame = LabelFrame(self.tab_HECTOR_board_management, text="HECTOR motherboard")
    self.motherboard_frame.pack(side=TOP, fill=X)
    
    # Tab enrolment

    self.get_PUF_response_frame = LabelFrame(self.tab_enrolment,
                                             text="PUF response")
    self.get_PUF_response_frame.pack(fill="x", side="top")

    # Tab activation
    self.load_server_reference_response_frame = LabelFrame(self.tab_activation,
                                                     text="Load server reference response")
    self.load_server_reference_response_frame.pack(fill="x")
    self.CASCADE_frame = LabelFrame(self.tab_activation,
                                    text="CASCADE protocol")
    self.CASCADE_frame.pack(fill="x")
    self.key_derivation_frame = LabelFrame(self.tab_activation,
                                           text="Key derivation")
    self.key_derivation_frame.pack(fill="x", side="top")
    self.activation_word_encryption_frame = LabelFrame(self.tab_activation,
                                                       text="Activation word encryption")
    self.activation_word_encryption_frame.pack(fill="x")
    self.activation_word_encryption_frame.grid_columnconfigure(1, weight=1)
    self.activation_process_frame = LabelFrame(self.tab_activation,
                                         text="Activation process")
    self.activation_process_frame.pack(fill="x")
