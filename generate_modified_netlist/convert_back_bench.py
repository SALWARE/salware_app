# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: convert_back_bench.py
# Date: 2016-10-19


def convert_back(self, dest_file):
    
    """Convert a graph into a bench netlist"""

    dest = open(dest_file, "w")

    # Preprocessing
    for i in self.vs:
        if i["cat"] == "node" and i.predecessors() == []:
            print "Error in convert_back", i
            i["cat"] = "input"

    for i in self.vs:
        if i["cat"] == "input":
            dest.write("INPUT(" + i["name"] + ")\n")

    for i in self.vs:
        if i["cat"] == "output":
            dest.write("OUTPUT(" + i["name"] + ")\n")

    dest.write("\n")
    for i in self.vs:
        if i["cat"] in ["node", "output", "mod"]:
            first = True

            for j in i.predecessors():
                function = self.es[self.get_eid(self.vs.find(j["name"]), self.vs.find(i["name"]))]["label"].lower()
                if function in ["not", "buf"]:
                    dest.write(i["name"] + " = " + function + "(" + j["name"])
                elif function in ["and", "or", "xnor", "xor", "nand", "nor"]:
                    if first:
                        dest.write(i["name"])
                        dest.write(" = ")
                        dest.write(function + "(")
                        dest.write(j["name"])
                        first = False
                    else:
                        dest.write("," + j["name"])
            dest.write(")\n")
    dest.close()
