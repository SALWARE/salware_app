# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: convert_back.py
# Date: 2016-10-13


def convert_back(self, dest_file):

    """Convert a graph into a vhd netlist"""

    dest = open(dest_file, "w")
    # LIBRARY
    dest.write("LIBRARY IEEE;\nUSE IEEE.STD_LOGIC_1164.ALL;\n\n")
    # ENTITY
    name = dest_file.split("/")[-1].split(".")[0]
    dest.write("ENTITY " + name + " IS\n\n")
    # PORTS
    dest.write("  PORT (\n")

    # Preprocessing
    for i in self.vs:
        if i["cat"] == "node" and i.predecessors() == []:
            print "Error in convert_back", i
            i["cat"] = "input"

    for i in self.vs:
        if i["cat"] == "input":
            dest.write("    " + i["name"] + " : IN STD_LOGIC;\n")

    for i in self.vs:
        if i["cat"] == "output":
            dest.write("    " + i["name"] + " : OUT STD_LOGIC;\n")

    dest.seek(-3, 2)
    dest.write("\n    );\n\n")
    dest.write("END ENTITY " + name + ";\n\n")
    dest.write("ARCHITECTURE rtl OF " + name + " IS\n\n")

    for i in self.vs:
        if i["cat"] in ["node", "mod"]:
            dest.write("  SIGNAL " + i["name"] + " : STD_LOGIC;\n")

    dest.write("\nBEGIN\n\n")

    for i in self.vs:
        if i["cat"] in ["node", "output", "mod"]:
            dest.write("  ")
            first = True

            for j in i.predecessors():
                function = self.es[self.get_eid(self.vs.find(j["name"]), self.vs.find(i["name"]))]["label"].lower()
                if function == "not":
                    dest.write(i["name"] + " <= " + function.upper() + "(" + j["name"] + ")")
                if function == "buf":
                    dest.write(i["name"] + " <= " + j["name"])
                elif function in ["and", "or", "xnor", "xor"]:
                    if first:
                        dest.write(i["name"])
                        dest.write(" <= ")
                        dest.write(j["name"])
                        dest.write(" " + function.upper() + " ")
                        first = False
                    else:
                        dest.write(j["name"])
                        dest.write(" " + function.upper() + " ")
                elif function in ["nand", "nor"]:
                    if first:
                        dest.write(i["name"])
                        dest.write(" <= ")
                        dest.write("NOT(")
                        dest.write(j["name"])
                        first = False
                    else:
                        dest.write(" " + function[1:].upper() + " " + j["name"])

            if function in ["and", "xor"]:
                dest.seek(-5, 2)
            elif function == "xnor":
                dest.seek(-6, 2)
            elif function == "or":
                dest.seek(-4, 2)
            elif function == "nand":
                dest.seek(0, 2)
                dest.write(")")
            elif function == "nor":
                dest.seek(0, 2)
                dest.write(")")
            dest.write(";\n")

    dest.write("\nEND ARCHITECTURE rtl;\n")
    dest.close()
