# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Building the graph from the netlist
Project    : Graph-based nodes selection for functional locking

File       : build_def.py
Last update: 2015-03-17
"""
from __future__ import print_function
import igraph as ig
import pyparsing as pp
import time
import sys
import os

def adv_add_edges(g, es, **kwds):
    """adv_add_edges(es, **kwds)
    
    Adds multiple edges to the graph with a unique set of keywords.
    
    Keyword arguments (except the source and target arguments) will be
    assigned to added edges as attributes.
    @param g : the graph where to add given edges    
    @param es: list of source - dest tuples to add
    @param instances: list of instances names to add corresponding to edges
    @param **kwds : attributes to add to all the edges
    
    @return result of igraph.add_edges()
    """
    
    if not kwds:
        return g.add_edges(es)
#   Getting next edge ID
    eid = g.ecount()
#   Adding all the edges from es to the graphe
    result = g.add_edges(es)
#   Adding Keywords and instance name to all the edges
    for i, _ in enumerate(es):
        for key, value in kwds.iteritems():
            g.es[eid + i][key] = value
    return result

def AddInputs(toks, g, prim_in, nodes):
    """AddInputs(toks)
    
    Add Inputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Functions' definition list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list to fill with inputs
    @param nodes: list to fill with signal which can be inputs of functions
    """
    print ("AddInputs")
    
    for Input in toks[0]:
        if not Input in prim_in:
            prim_in.append(Input)
            nodes.append(Input)
            g.add_vertex(Input,
                         label=Input,
                         color="#DDDDDD",
                         cat="input",
                         locks=[],
                         forced=[],
                         size=100,
                         label_size=30)

def AddOutputs(toks, g, prim_in, prim_out):
    """AddOutputs(toks)
    
    Add Outputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Functions' definition list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list conatining inputs names
    @param prim_out: list to fill with outputs
    """
    print("AddOutputs")
    
    for Output in toks[1]:
        if (not Output in prim_in) and (not Output in prim_out):
            prim_out.append(Output)
            g.add_vertex(Output,
                         label=Output,
                         color="#666666",
                         cat="output",
                         locks=[0, 1],#By convention
                         forced=[],
                         size=100,
                         label_size=30)

def AddGate(toks, g, prim_out, nodes):
    """AddGate(toks)
    
    Add edges to the graph
        
    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Functions' definition list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signal which can be inputs of functions
    """
    print("AddGates") #Debug
#   Initializing vars
    to_add = {'and':[[],[]], 'nand':[[],[]], 'or':[[],[]], 'not':[[],[]], 'buf':[[],[]]}
    add_vertices = []
    nots_list = []
    
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[2])) + '/' + nbdisp, end='')
#   Reading all functions definitions from the netlist
    for Def in toks[2]:
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
#       Getting datas
        inputs = Def[0][0]
        output = Def[0][1]
#       adding target node in add_vertices
        if not output in prim_out:
            add_vertices.append(output)
            nodes.append(output)
#   =°1=Check inputs number : more than 1 inputs
        if len(inputs) > 1:
            and_count = 0
            and_nodes = []
#           if there are multiple signal cover, each lines have to be combinated with an or
            if len(Def[1]) > 1:
                global_or = True
            else:
                global_or = False
            
#           Reading all output cover
            for cover in Def[1]:
#               if an or is needed, create intermediate nodes and redirect the output of the and gate
                if global_or: 
                    and_count += 1
                    target = "and{0}_{1}".format(output, and_count)
                else:
                    target = output
#               Initializing some vars
                edges_list = []
                used_signal = 0
                last_state_found = ''
#               reading inputs states for this cover
                for j, source in enumerate(cover[0]):
#                   Adding direct input if as '1' source
                    if source == '1':
                        used_signal += 1
                        edges_list.append((inputs[j], target))
                        last_state_found = '1'
#                   Adding complemented input if as '0' source
                    elif source == '0':
                        used_signal += 1
                        not_source = 'interNot_{0}'.format(inputs[j])
#                       Creating complemented nodes for the input if doesn't exist yet
                        if not not_source in nots_list:
                            add_vertices.append(not_source)
                            to_add['not'][0].append((inputs[j], not_source))
                            nots_list.append(not_source)
                        edges_list.append((not_source, target))
                        last_state_found = '0'
#           =°2=Checking the number of inputs used : more than 1 inputs used
                if(used_signal > 1):
#                   adding the intermediate node if global or needed
                    if global_or:
                        and_nodes.append(target)
                        add_vertices.append(target)
#                   Adding edges to the list
                    if cover[1] == '1':
                        to_add['and'][0].extend(edges_list)
                    elif cover[1] == '0':
                        to_add['nand'][0].extend(edges_list)
#           =°2=Checking the number of inputs used : 1 input used
                elif used_signal == 1:
#                   Adding the inputs directly for the global or
                    if global_or:
                        and_nodes.append(edges_list[0][0])
#                   If no global or, adding a BUF or a NOT depending on the input state (last_state_found)
                    else:
                        if last_state_found == '1':
                            to_add['buf'][0].extend(edges_list)
                        elif last_state_found == '0':
#                           if complemented version added just for this, redirecting the target node to the real output
                            if add_vertices[-1] == edges_list[0][0]:
                                nots_list.pop()
                                to_add['not'][0][-1] = (to_add['not'][0][-1][0], target)
#                           if complemented version added before, adding a buf from the complemented version to the output
                            else:
                                to_add['buf'][0].extend(edges_list)
#           adding the global or for the function if needed
            if global_or:
                edges_list = []
                for int_and in and_nodes:
                    edges_list.append((int_and, output))
                to_add['or'][0].extend(edges_list)
#   =°1=Check inputs number: 1 inputs
        elif len(inputs) == 1:
#           input state = output state => buf
            if Def[1][0][1] == Def[1][0][0][0]:
                to_add['buf'][0].append((inputs[0], output))
#           input state =/= output state => not
            else:
                to_add['not'][0].append((inputs[0], output))
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb)) 
#   Adding nodes to the graphe
    for vertex in add_vertices:
        g.add_vertex(vertex,
                     label=vertex,
                     color="#FFFFFF",
                     cat="node",
                     locks=[],
                     forced=[],
                     size=100,
                     label_size=30)
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add, key=lambda i: len(to_add[i][0]), reverse=False):
        if len(to_add[i][0]) > 0:
            print('-> Adding ' + str(len(to_add[i][0])) + ' ' + i + '...')
            adv_add_edges(g, to_add[i][0],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")
                            
def build(name, result=None, parsedbg=False, verbose=True):
    """build(name, result=None, parsedbg=False)
    Builds the graph from an EDIF netlist
    @param name: name of the netlist to build
    @keyword result(None): result set from the edif syntax reader. Avoid reading and use set given instead. Used mainly for debugging.
    @keyword parsebg(False): Boolean used to disable graph generating and return the syntax reader set of the netlist choosen. Used mainly for debugging.
    
    @return a tuple of 4 elements : the graph for the netlist, the list of inputs, the list of outputs, the list of node (inputs and internal nodes)
    """
    print("---- Build BLIF")
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    prim_in = []    #primary inputs of the netlist
    prim_out = []   #primary outputs of the netlist
    nodes = []      #nodes of the netlist
    g = ig.Graph(directed=1)
#   Setting base elements
    InputWord = pp.Suppress('.' + pp.CaselessLiteral("INPUTS"))
    OutputWord = pp.Suppress('.' + pp.CaselessLiteral("OUTPUTS"))
    Node = pp.Word(pp.alphanums + '$/_[]()<>', bodyChars = pp.alphanums + '$/_[]()<>.')# /!\Attention aux caractères spéciaux !
    SignalNames = pp.Suppress('.' + pp.CaselessLiteral("names")) + pp.Group(pp.ZeroOrMore(Node + pp.NotAny(pp.LineEnd()))) + Node + pp.Suppress(pp.LineEnd())
#   Setting function reading elements
    SignalValues = pp.ZeroOrMore(pp.Group(pp.Group(pp.OneOrMore(pp.NotAny(pp.White(' ')) + pp.Or(['1', '0','-']))) + pp.Or(['1', '0'])))
    SignalDef = pp.Group(SignalNames) + pp.Group(SignalValues)
#   Setting IOreading elements
    Inputs = pp.OneOrMore(InputWord + pp.OneOrMore(Node))
    Outputs = pp.OneOrMore(OutputWord + pp.OneOrMore(Node))
#   Setting full parser
    FullParser = pp.Suppress(pp.SkipTo(Inputs)) + pp.Group(Inputs) + pp.Group(Outputs) + pp.Group(pp.OneOrMore(pp.Group(SignalDef)))
#    SignalNames.ignore(pp.Regex("\(\d*\)"))
    FullParser.ignore('\\' + pp.LineEnd())
#   Parsing if no data given
    if result == None:
        print("Parsing...")
        result = FullParser.parseFile(name)
#   Returning data readen without generating graph if needed
    if parsedbg:
        if not verbose:
            sys.stdout = sys.__stdout__
        print('---- Done (parsedbg)\n')
        return result
#   Generating graph with data readen or given adn returning
    else:
        AddInputs(result, g, prim_in, nodes)
        AddOutputs(result, g, prim_in, prim_out)
        AddGate(result, g, prim_out, nodes)
        if not verbose:
            sys.stdout = sys.__stdout__
        print("---- Done\n")
        return g, prim_in, prim_out, nodes