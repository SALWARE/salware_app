# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Building the graph from the netlist
Project    : Graph-based nodes selection for functional locking

File       : build_def.py
Last update: 2015-03-17
"""

from __future__ import print_function
import igraph as ig
import pyparsing as pp
import time
import sys
import os

def adv_add_edges(g, es, instances, **kwds):
    """adv_add_edges(es, **kwds)
    
    Adds multiple edges to the graph with a unique set of keywords.
    
    Keyword arguments (except the source and target arguments) will be
    assigned to added edges as attributes.
    @param g : the graph where to add given edges    
    @param es: list of source - dest tuples to add
    @param instances: list of instances names to add corresponding to edges
    @param **kwds : attributes to add to all the edges
    
    @return result of igraph.add_edges()
    """
    
    if len(es) != len(instances):
        raise Exception('Length of es different than length of instances')
    if not kwds:
        return g.add_edges(es)
#   Getting next edge ID
    eid = g.ecount()
#   Adding all the edges from es to the graphe
    result = g.add_edges(es)
#   Adding Keywords and instance name to all the edges
    for i, _ in enumerate(es):
        for key, value in kwds.iteritems():
            g.es[eid + i][key] = value
        g.es[eid + i]['instance'] = instances[i]
    return result
    
def AddIO(toks, g, prim_in, prim_out, nodes):
    """AddIO(toks)
    
    Add Inputs and Outputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs/Outputs list
           toks[1] : Gates list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list to fill with inputs
    @param prim_out: list to fill with outputs
    @param nodes: list to fill with signal which can be inputs of functions
    """
    print("AddIO") #Debug
#    print toks
    
    for row in toks[0]:#toks contain the full result of the parsing
        Dir = row[0]
        InputOutput = row[1]
        if Dir == "INPUT":
            if not InputOutput in prim_in:
                g.add_vertex(InputOutput,
                                 label=InputOutput,
                                 color="#DDDDDD",
                                 cat="input",
                                 locks=[],
                                 forced=[],
                                 size=100,
                                 label_size=30)
                prim_in.append(InputOutput)
                nodes.append(InputOutput)
            
        elif Dir == "OUTPUT":
            if (not InputOutput in prim_in) and (not InputOutput in prim_out):
                prim_out.append(InputOutput)
                g.add_vertex(InputOutput,
                              label=InputOutput,
                              color="#666666",
                              cat="output",
                              locks=[0, 1],#By convention
                              forced=[],
                              size=100,
                              label_size=30)
#            else:
#                g.vs.find(name=InputOutput).delete()
#                nodes.remove(InputOutput)
#                prim_in.remove(InputOutput)

def AddGate(toks, g, prim_in, prim_out, nodes):
    """AddGate(toks)
    
    Add edges to the graph
        
    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs/Outputs list
           toks[1] : Gates list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signal which can be inputs of functions
    """
    print("AddGate") #Debug

    to_add_edges = {'and':[[],[]], 'nand':[[],[]], 'xor':[[],[]], 'or':[[],[]], 'nor':[[],[]], 'not':[[],[]], 'buf':[[],[]]}    
    instance_counter = 0;
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[1])) + '/' + nbdisp, end='')
#   Reading gates list
    for row in toks[1]:
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
#       Adding internal node if needed
        noeud = row[0]
        function = row[1]
        inputs = row[2]
        if not noeud in prim_out:
            g.add_vertex(noeud,
                         label=noeud,
                         color="#FFFFFF",
                         cat="node",
                         locks=[],
                         forced=[],
                         size=100,
                         label_size=30)
            nodes.append(noeud)
#       Adding edges with the right function to the list
        while inputs:
            dest = inputs.pop()
            to_add_edges[function][0].append((dest, noeud))
            to_add_edges[function][1].append('inst_{0}'.format(instance_counter))  
        instance_counter += 1
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb)) 
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add_edges, key=lambda i: len(to_add_edges[i][0]), reverse=False):
        if len(to_add_edges[i][0]) > 0:
            print('-> Adding ' + str(len(to_add_edges[i][0])) + ' ' + i + '...')
            adv_add_edges(g, to_add_edges[i][0], to_add_edges[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")
        
def build(name, result=None, parsedbg=False, verbose=True):
    """build(name, result=None, parsedbg=False)
    Builds the graph from an EDIF netlist
    @param name: name of the netlist to build
    @keyword result(None): result set from the edif syntax reader. Avoid reading and use set given instead. Used mainly for debugging.
    @keyword parsebg(False): Boolean used to disable graph generating and return the syntax reader set of the netlist choosen. Used mainly for debugging.
    
    @return a tuple of 4 elements : the graph for the netlist, the list of inputs, the list of outputs, the list of node (inputs and internal nodes)
    """

    print("---- Build BENCH")
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    prim_in = []    #primary inputs of the netlist
    prim_out = []   #primary outputs of the netlist
    nodes = []      #nodes of the netlist
    g = ig.Graph(directed=1)
    
#   Defining parsing elements
    Dir = pp.Or([pp.CaselessLiteral("INPUT"), pp.CaselessLiteral("OUTPUT")])
    Node = pp.Word(pp.alphanums + '_')# /!\Attention aux caractères spéciaux !
    Gate = pp.Or([pp.CaselessLiteral("nand"), pp.CaselessLiteral("and"), pp.CaselessLiteral("nor"), pp.CaselessLiteral("or"), pp.CaselessLiteral("xor"), pp.CaselessLiteral("not"), pp.CaselessLiteral("buf")])
    
#   Defining Parsing groups
    IOLike = Dir + '(' + Node + ')' #Used to detect IO lines
    IO = pp.ZeroOrMore(pp.Group(Dir + pp.Suppress('(') + Node + pp.Suppress(')')))
    GateLink = pp.ZeroOrMore(pp.Group(Node + pp.Suppress('=') + Gate + pp.Suppress('(') + pp.Group(pp.OneOrMore(Node + pp.Optional(pp.Suppress(',')))) + pp.Suppress(')')))
#   Defining full parser
    FullParser = pp.Suppress(pp.SkipTo(IOLike)) + pp.Group(IO) + pp.Group(GateLink)
#   Opening netlist file
    path = ""
    source_file = path+name
#   Parsing if no data given
    if result == None:
        print("Parsing...")
        result = FullParser.parseFile(source_file)
#   Returning data readen without generating graph if needed
    if parsedbg:
        if not verbose:
            sys.stdout = sys.__stdout__
        print('---- Done (parsedbg)\n')
        return result
#   Generating graph with data readen or given and returning
    else:
        AddIO(result, g, prim_in, prim_out, nodes)
        AddGate(result, g, prim_in, prim_out, nodes)
        if not verbose:
            sys.stdout = sys.__stdout__
        print("---- Done\n")
        return g, prim_in, prim_out, nodes
