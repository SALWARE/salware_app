# -*- coding: utf-8 -*-
"""
Created on Mon May 02 14:18:57 2016

@author: théo
"""
import build_edif

def get_chain_start(graph, not_edge_id):
    prev_edge = not_edge_id
    source = graph.es[not_edge_id].source
    source_edges_ID = graph.incident(source, mode='IN')
    if len(source_edges_ID) > 0:
        source_edge = graph.es[source_edges_ID[0]]
        while source_edge['name'] in ['not', 'buf']:
            prev_edge = source_edge.index
            source = source_edge.source
            source_edges_ID = graph.incident(source, mode='IN')
            if len(source_edges_ID) > 0: 
                source_edge = graph.es[source_edges_ID[0]]
            else:
                break
    
    return prev_edge

def clean_const(graph):
    """    gère l'utilisation des constantes logiques dans le graphe.
    Les constantes logiques doivent être intégrés en représentant
    les noeuds Logic0 et Logic1 et en les reliants aux endroits voulus"""
    func_conv = [{'and' : '0',     'nand' : '1',     'or' : 'buf', 'nor' : 'not', 'xor' : 'buf', 'xnor' : 'not', 'not' : '1', 'buf' : '0', '1' : '1', '0' : '0'}, 
                 {'and' : 'buf',   'nand' : 'not',   'or' : '1',   'nor' : '0',   'xor' : 'not', 'xnor' : 'buf', 'not' : '0', 'buf' : '1', '1' : '1', '0' : '0'}]

    const_node_pile = [(graph.vs.find(name='Logic0').index, '0'), (graph.vs.find(name='Logic1').index, '1')]
    instance_dict = {}
    
    print('Building Instances dictionnary')
    for i in graph.es:
        if instance_dict.has_key(i['instance']):
            instance_dict[i['instance']].append(i.index)
        else:
            instance_dict[i['instance']] = [i.index]
            
    print('Cleaning constant')
    while len(const_node_pile) > 0:
        const_node_ID, const_node_state = const_node_pile.pop()
        for i in graph.incident(const_node_ID):
            if not graph.es[i]['instance'] == 'useless':
                if func_conv[int(const_node_state)][graph.es[i]['name']] in ['0', '1']:
                    const_node_pile.append((graph.es[i].target, func_conv[int(const_node_state)][graph.es[i]['name']])) 
                    for j in instance_dict.pop(graph.es[i]['instance']):
                        graph.es[j]["instance"] = 'useless'
                        graph.es[j]["name"] = 'const'
                        graph.es[j]["label"] = 'const'
                else:
                    if len(instance_dict[graph.es[i]['instance']]) == 2:
                        for j in instance_dict[graph.es[i]['instance']]:
                            if not i == j:
                                graph.es[j]["name"] = func_conv[int(const_node_state)][graph.es[j]["name"]]
                                graph.es[j]["label"] = func_conv[int(const_node_state)][graph.es[j]["label"]]
                    instance_dict[graph.es[i]['instance']].remove(i)
                    graph.es[i]['instance'] = 'useless'
    print('Deleting ' + str(len(graph.es.select(instance='useless'))) + ' useless edges')
    graph.delete_edges(graph.es.select(instance='useless'))
    print('---- Done\n')
    return graph
    
def clean_not_buf(graph):
    to_add = {'and':[[],[]], 'nand':[[],[]], 'or':[[],[]], 'nor':[[],[]], 'xor':[[],[]], 'xnor':[[],[]], 'not':[[],[]], 'buf':[[],[]]}
    to_delete = []
    not_buf_edges = []

    print 'Building nots index list'
    for i in graph.es(name='not'):
        not_buf_edges.append(i.index)
    for i in graph.es(name='buf'):
        not_buf_edges.append(i.index)
#    print not_buf_edges
    print 'Searching for not chains and fixing'
    while len(not_buf_edges) > 0:
#        print('\n')
#        print(not_buf_edges[0])
#        print('----')
        chain_start = get_chain_start(graph, not_buf_edges[0])
        if graph.es[chain_start]['name'] == 'not':
            branch_pile = [(chain_start, True)]  
        elif graph.es[chain_start]['name'] == 'buf':
            branch_pile = [(chain_start, False)]
#            to_delete.append(chain_start)
        else:
            print graph.es[chain_start]
            raise Exception('Chain start is not a \'buf\' nor a \'not\'')
            
        not_chain = False
        buf_source = graph.es[branch_pile[0][0]].source
#        raw_input()
        moved_edge = 0
        while len(branch_pile) > 0:
            this_edge_ID, complemented = branch_pile.pop()
            if (graph.es[this_edge_ID]['name'] == 'not') and (not_chain == False):
                not_source = graph.es[this_edge_ID].target
                not_chain = True
#            print this_edge_ID                                                       ###
            not_buf_edges.remove(this_edge_ID)
            
            target_node_ID = graph.es[this_edge_ID].target
            node_edges = graph.incident(target_node_ID)
            to_move = True
            for j in node_edges:
                if (graph.es[j]['name'] == 'not'):
                    branch_pile.append((j, not complemented))
                    if not graph.vs[graph.es[j].target]['cat'] == 'output':
                        if not_chain:
                            to_delete.append(j)
                            to_move = False
#                    else:
##                        print j                                                   ###
#                        not_buf_edges.remove(j)
                        
                elif (graph.es[j]['name'] == 'buf'):
                    branch_pile.append((j, complemented))
                    if not graph.vs[graph.es[j].target]['cat'] == 'output':
                        to_delete.append(j)
                        to_move = False
#                    else:
##                        print j                                                   ###
#                        not_buf_edges.remove(j)
                    
                if to_move:
                    moved_edge += 1
                    if complemented:
                        to_add[graph.es[j]['name']][0].append((not_source, graph.es[j].target))
                    else:
                        to_add[graph.es[j]['name']][0].append((buf_source, graph.es[j].target))
                    to_add[graph.es[j]['name']][1].append(graph.es[j]['instance'])
                    to_delete.append(j)
                to_move = True
        
        if (moved_edge == 0) and (not graph.vs[target_node_ID]['cat'] == 'output'):
            to_delete.append(chain_start)

    print 'Updating graph'
    print '-> Deleting ' + str(len(to_delete)) + ' edges'
    graph.delete_edges(to_delete)
    print '-> Adding edges'
#    print to_add
    for i in to_add:
        if len(to_add[i][0]) > 0:
            print(' - Adding ' + str(len(to_add[i][0])) + ' ' + i + '...')
            build_edif.adv_add_edges(graph, to_add[i][0], to_add[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")
    print('---- Done\n')
    return graph