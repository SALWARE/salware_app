# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Building the graph from the netlist
Project    : Graph-based nodes selection for functional locking

File       : build_def.py
Last update: 2015-03-17
"""
from __future__ import print_function
import igraph as ig
import pyparsing as pp
import time
import clean_const
import sys
import os

def adv_add_edges(g, es, instances, **kwds):
    """adv_add_edges(es, **kwds)
    
    Adds multiple edges to the graph with a unique set of keywords.
    
    Keyword arguments (except the source and target arguments) will be
    assigned to added edges as attributes.
    @param g : the graph where to add given edges    
    @param es: list of source - dest tuples to add
    @param instances: list of instances names to add corresponding to edges
    @param **kwds : attributes to add to all the edges
    
    @return result of igraph.add_edges()
    """
    
    if len(es) != len(instances):
        raise Exception('Length of es different than length of instances')
    if not kwds:
        return g.add_edges(es)
#   Getting next edge ID
    eid = g.ecount()
#   Adding all the edges from es to the graphe
    result = g.add_edges(es)
#   Adding Keywords and instance name to all the edges
    for i, _ in enumerate(es):
        for key, value in kwds.iteritems():
            g.es[eid + i][key] = value
        g.es[eid + i]['instance'] = instances[i]
    return result

def priorityFind(inputs_str):
    """priorityFind(inputs_str)
    
    Convert an inputs combination string with priority between parenthesis
    
    @param inputs_str: inputs combination string
    
    @return an array of string corresponding to priority blocks sorted by level (0 is the lowest level)
    """
#   Initializing some vars
    prio_level = 0     #Save the current priority level
    temp_storage = ['']#Save the blocs in progress of lower priority levels
    bloc_count = [0]   #Counters of blocs of each level to generate reference between levels
    result = ['']      #The result which will be returned
#   reading all characters one by one from the logical equation
    for idl, letter in enumerate(inputs_str):
#           Beginning of a new bloc of higher level
        if letter == '(':
#               Adding a new temporary bloc
            if temp_storage[prio_level][-1] == '~':
                temp_storage.append('#')
                temp_storage[prio_level] = temp_storage[prio_level][:-3]
            else:
                temp_storage.append('')
#               Adding a counter to bloc_count if it doesn't exist yet
            if len(bloc_count) == prio_level + 1:
                bloc_count.append(0)
                result.append([])
#               Increasing the value else
            else:
                bloc_count[prio_level + 1] += 1
#               Adding the reference in the bloc in progress and increasing priority level
            temp_storage[prio_level] += '{' + str(bloc_count[prio_level + 1]) + '}'
            prio_level += 1
#           Ending of a priority bloc
        elif letter == ')':
#               Transfering prcessed bloc to the result and decreasing priority level
            result[prio_level].append(temp_storage.pop())
            prio_level -= 1
#           In any other case, copying letter to the processing bloc
        else:
            temp_storage[prio_level] += letter

#   Copying the lower level bloc in the results
    result[0] = [temp_storage[0]]
#   Splitting blocs strings to make processing easier after
    inputs_array = []
    for i, level in enumerate(result):
        inputs_array.append([])
        for j, block in enumerate(level):
            if block[0] == '#':
                block = block[1:]
                complemented = True
            else:
                complemented = False
            inputs_array[i].append([])
            for k, or_branch in enumerate(block.split(' | ')):
                if ' & ' in or_branch:
                    operator = ' & '
                    inputs_array[i][j].append(['and'])
                elif ' ^ ' in or_branch:
                    operator = ' ^ '
                    inputs_array[i][j].append(['xor'])
                elif ' ~^ ' in or_branch:
                    operator = ' ~^ '
                    inputs_array[i][j].append(['xnor'])
                elif ' ^~ ' in or_branch:
                    operator = ' ^~ '
                    inputs_array[i][j].append(['xnor'])
                else:
                    operator = 'none'
                    inputs_array[i][j].append(['none'])
                inputs_array[i][j][k].extend(filter(None, or_branch.split(operator)))
                if complemented:
                    inputs_array[i][j][k].append('#')
                    
    return inputs_array

def AddIO(toks, g, prim_in, prim_out, nodes):
    """AddIO(toks)
    
    Add Inputs and Outputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signal which can be inputs of functions
    @param top_cell: ID of the top cell in the results list
    """
    print("AddIO") #Debug
#   Reading all the IO bus of the global cell
    for IODecl in toks[0]:
        InputOutput = IODecl[1]
        Dir = IODecl[0]
#       Adding ports
        if Dir == "input":
            if not InputOutput in prim_in:
                g.add_vertex(InputOutput,
                             label=InputOutput,
                             color="#DDDDDD",
                             cat="input",
                             locks=[],
                             forced=[],
                             size=100,
                             label_size=30)
                prim_in.append(InputOutput)
                nodes.append(InputOutput)
        
        elif Dir == "output":
            if (not InputOutput in prim_in) and (not InputOutput in prim_out):
                prim_out.append(InputOutput)
                g.add_vertex(InputOutput,
                             label=InputOutput,
                             color="#666666",
                             cat="output",
                             locks=[0, 1],#By convention
                             forced=[],
                             size=100,
                             label_size=30)

def AddGate(toks, g, prim_in, prim_out, nodes, name_conv):
    """AddGate(toks)
    
    Add edges to the graph
        
    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Nodes' link list
           toks[3] : logical functions list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to fill with signal which can be inputs of functions
    @param name_conv: dictionnary containing nodes name to be replace with an other name
    """
    print("AddGates") #Debug
#   Initializing some vars
    to_add_edges = {'and':[[],[]], 'nand':[[],[]], 'or':[[],[]], 'nor':[[],[]], 'not':[[],[]], 'buf':[[],[]], 'xor':[[],[]], 'xnor':[[],[]]}
    complement = {'and':'nand', 'nand':'and', 'or':'nor', 'nor':'or', 'xor':'xnor', 'xnor':'xor'}
    add_vertices = []   #list of vertices to add
    instance_counter = 0#counter to generate instances names
    
    add_vertices.extend(toks[1])
    
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[2])) + '/' + nbdisp, end='')
#   Reading all the logical functions
    for Def in toks[2]: 
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')

        output = Def[0]
#       Getting inputs array to process
        inputs_array = priorityFind(Def[1].replace('\n', ''))
#       Reading all the priority level from the array (from the higher to the lower)
        for i, level in reversed(list(enumerate(inputs_array))):
            if (i == 0) and (len(level) == 1) and (len(level[0]) == 1) and (len(level[0][0]) == 2) and (level[0][0][1][0] == '{'):
                continue
#           Reading all the blocks from the priority level
            for j, block in enumerate(level):
#               Checking if a global or is needed
                if len(block) > 1:
                    global_or = True
                else:
                    global_or = False
#               Generating an internal name for blocs except for the last one to built
                if (i > 1) or ((i == 1) and (len(inputs_array[0]) > 1)):
                    block_out = "Or{0}{1}_{2}".format(i, j, output)
                    add_vertices.append(block_out)
                else:
                    block_out = output
#               initializing an array which will contain and output nodes to link
                block_ands = []
#               setting the complementation state
                complemented = False
#               Reading and gates in the block
                for k, and_ins in enumerate(block):
                    if and_ins == '#':
                        if complemented:
                            complemented = False
                        else:
                            complemented = True
                        continue
#                       If a global or is needed, generating the internal name for the and output
                    if global_or:
                        inner_name = "And{0}{1}{2}_{3}".format(i, j, k, output)
                        add_vertices.append(inner_name)
                        block_ands.append(inner_name)
#                       Else, using block output as and output
                    else:
                        inner_name = block_out
#                       Initializing an array which will contain edges (source, target)
                    edges_list = []
#                       Reading all inputs for the and function
                    for signal in and_ins:
#                           If the input is '#', changing the complementation state of the block
                        if signal == '#':
                            if complemented:
                                complemented = False
                            else:
                                complemented = True
                        elif signal in ['and', 'nand', 'nor', 'xor', 'xnor', 'none']:
                            function = signal
                        elif signal[0] == '{':
                            edges_list.append(("Or{0}{1}_{2}".format(i+1, signal[1:-1], output),inner_name))
#                           If the signal end with ('), using a complemented version of the input
                        else:
                            edges_list.append((signal, inner_name))
#                   If more than 1 inputs
                    if len(edges_list) > 1:
#                       Using a NAND only if ther is no global or and the bloc is complemented
                        if (not global_or) and complemented:
                            to_add_edges[complement[function]][0].extend(edges_list)
                            to_add_edges[complement[function]][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                            instance_counter += 1
#                       Else, using an AND
                        else:
                            to_add_edges[function][0].extend(edges_list)
                            to_add_edges[function][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                            instance_counter += 1
#                   If only 1 input, no need to use an AND or a NAND
                    else:
                        
                        if not global_or and complemented:
                            to_add_edges['not'][0].append(edges_list[0])
                            to_add_edges['not'][1].append('U' + str(instance_counter))
                            instance_counter += 1
                        elif not global_or and not complemented:
                            to_add_edges['buf'][0].append(edges_list[0])
                            to_add_edges['buf'][1].append('U' + str(instance_counter))
                            instance_counter += 1
                        else:
                            block_ands.append(edges_list[0][0])
#               If a global or is needed
                if global_or:
#                   building all the edge beteen block_ands and block_out
                    edges_list = []
                    for and_signal in block_ands:
                        edges_list.append((and_signal, block_out))
#                   Adding edges to to_add_edges with the right function depending on complmented
                    if complemented:
                        to_add_edges['nor'][0].extend(edges_list)
                        to_add_edges['nor'][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                        instance_counter += 1
                    else:
                        to_add_edges['or'][0].extend(edges_list)
                        to_add_edges['or'][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                        instance_counter += 1
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb)) 
#   Adding all vertices to the graphn
    for vertex in add_vertices:
        if (not vertex in prim_in) and (not vertex in prim_out):
            g.add_vertex(vertex,
                         label=vertex,
                         color="#FFFFFF",
                         cat="node",
                         locks=[],
                         forced=[],
                         size=100,
                         label_size=30)
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add_edges, key=lambda i: len(to_add_edges[i][0]), reverse=False):
        if len(to_add_edges[i][0]) > 0:
            print('-> Adding ' + str(len(to_add_edges[i][0])) + ' ' + i + '...')
            adv_add_edges(g, to_add_edges[i][0], to_add_edges[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")

def build(netlist, result=None, parsedbg=False, verbose=True):
    """build(name, result=None, parsedbg=False)
    Builds the graph from an EDIF netlist
    @param netlist: name of the netlist to build
    @keyword result(None): result set from the edif syntax reader. Avoid reading and use set given instead. Used mainly for debugging.
    @keyword parsebg(False): Boolean used to disable graph generating and return the syntax reader set of the netlist choosen. Used mainly for debugging.
    
    @return a tuple of 4 elements : the graph for the netlist, the list of inputs, the list of outputs, the list of node (inputs and internal nodes)
    """
    print("---- Build Verilog RTL")
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    g = ig.Graph(directed=1)
    prim_in = []    #primary inputs of the netlist
    prim_out = []   #primary outputs of the netlist
    nodes = []      #nodes of the netlist
    name_conv = {}
#   Setting basic elements
    name = pp.Word(pp.alphanums + '_\[')
    port = pp.Group(pp.Or(['input', 'output']) + name + pp.Suppress(pp.Literal(';')))
    
    signals = pp.Group(pp.OneOrMore(pp.Suppress(pp.CaselessLiteral('wire')) + name + pp.Suppress(pp.Literal(';'))))
    
    content = pp.Group(pp.OneOrMore(pp.Group(pp.Suppress(pp.Literal('assign')) + name + pp.Suppress(pp.Literal('=')) + pp.SkipTo(pp.Literal(';')) + pp.Suppress(pp.Literal(';')))))
    module = pp.Suppress(pp.CaselessLiteral('module') + name + pp.CaselessLiteral('(') + pp.OneOrMore(name + pp.Literal(',')) + name + pp.Literal(');')) + pp.Group(pp.OneOrMore(port))
    architecture = signals + content + pp.Suppress(pp.CaselessLiteral('endmodule'))
    
    FullParser = pp.Suppress(pp.SkipTo(module)) + module + architecture
#   Opening netlist file
    source_file = netlist
#   Reading the file if no results given
    if result == None:
        print("Parsing...")
        result = FullParser.parseFile(source_file)
        print("---- Done\n")
#   Returning only the result from the file if asked by user
    if parsedbg:
        if not verbose:
            sys.stdout = sys.__stdout__
        print('---- Done (parsedbg)\n')
        return result
#   Building and returning the graphe
    else:
        AddIO(result, g, prim_in, prim_out, nodes)
        AddGate(result, g, prim_in, prim_out, nodes, name_conv)
        print("---- Done\n")
        g = clean_const.clean_not_buf(g)
        if not verbose:
            sys.stdout = sys.__stdout__
            print('---- Done\n')

        return g, prim_in, prim_out, nodes

if __name__ == "__main__":
    build("./addsub_df.v")
