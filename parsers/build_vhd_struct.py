# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Building the graph from the netlist
Project    : Graph-based nodes selection for functional locking

File       : build_def.py
Last update: 2015-03-17
"""

from __future__ import print_function
import igraph as ig
import pyparsing as pp
import clean_const
from cells_dict import vhd_cells
import time
import os
import sys

def TopCellSelect(toks, verbose):
    if not verbose:
        sys.stdout = sys.__stdout__
    if len(toks) > 1:
        print('Cells in the file :')
        for i, cell in enumerate(toks):
            print(str(i) + ': ' + cell[0])
        good_choice = False
        while not good_choice:
            choice = raw_input('Choose top cell :')
            try:
                if int(choice) < len(toks):
                    top_cell = int(choice)
                    good_choice = True
                else:
                    print("this cell doesn't exist")
            except:
                print("Type a number")
    else:
        top_cell = 0
        print('Using top cell ' + toks[0][0])
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    return top_cell

def adv_add_edges(g, es, instances, **kwds):
    """adv_add_edges(es, **kwds)
    
    Adds multiple edges to the graph with a unique set of keywords.
    
    Keyword arguments (except the source and target arguments) will be
    assigned to added edges as attributes.
    @param g : the graph where to add given edges    
    @param es: list of source - dest tuples to add
    @param instances: list of instances names to add corresponding to edges
    @param **kwds : attributes to add to all the edges
    
    @return result of igraph.add_edges()
    """
    
    if len(es) != len(instances):
        raise Exception('Length of es different than length of instances')
    if not kwds:
        return g.add_edges(es)
#   Getting next edge ID
    eid = g.ecount()
#   Adding all the edges from es to the graphe
    result = g.add_edges(es)
#   Adding Keywords and instance name to all the edges
    for i, _ in enumerate(es):
        for key, value in kwds.iteritems():
            g.es[eid + i][key] = value
        g.es[eid + i]['instance'] = instances[i]
    return result
    
def AddIO(toks, g, prim_in, prim_out, nodes, top_cell):
    """AddIO(toks)
    
    Add Inputs and Outputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signal which can be inputs of functions
    @param top_cell: ID of the top cell in the results list
    """
    print("AddIO") #Debug
#   Reading all the IO bus of the global cell
    for IODecl in toks[top_cell][1]:
#       Looping for the size of the bus
        if len(IODecl) == 2:
            bus_len = 1
        else:
            bus_len = int(IODecl[2]) + 1
        Dir = IODecl[1]
        for i in range(bus_len):
#           Getting direction of the port and adding id in bus if multiple ports in the same bus
            if bus_len == 1:
                InputOutput = IODecl[0]
            else:
                InputOutput = IODecl[0] + '_' + str(i)
#           Adding ports
            if Dir == "IN":
                if not InputOutput in prim_in:
                    g.add_vertex(InputOutput,
                                 label=InputOutput,
                                 color="#DDDDDD",
                                 cat="input",
                                 locks=[],
                                 forced=[],
                                 size=100,
                                 label_size=30)
                    prim_in.append(InputOutput)
                    nodes.append(InputOutput)
            
            elif Dir == "OUT":
                    prim_out.append(InputOutput)
                    g.add_vertex(InputOutput,
                                 label=InputOutput,
                                 color="#666666",
                                 cat="output",
                                 locks=[0, 1],#By convention
                                 forced=[],
                                 size=100,
                                 label_size=30)

def GateFinder(Component_name, internal_cells):
    """GateFinder(Component_name)
    
    Dictionnary which allow to find which model associate with which cell name    
    If the cell isn't described, raise an Exception with the name of the gate which caused the error.
    
    @param Component_name: name of the edif cell to represent
    @param internal_cells: dictionnary of internal cells models for internal type
    
    @return the gate type corresponding to the name given in Component_name.
            Can be:
            - The name of a standard cell (not, buf, and, nand, or, nor, xor, xnor)
            - A tuple containing first the library (internal or software) where the cell is described, and second the name of the cell in this library.
    """
#   removing useless informations before checking cell type
    comp_array = Component_name.split('_')
    number_del = ''
    while comp_array[0][-1] in '0123456789':
#       memorizing removed number for some uses
        number_del = comp_array[0][-1] + number_del
        comp_array[0] = comp_array[0][:-1]
#   returning datas depending on the cell name
    if Component_name[0:5].upper() == 'LOGIC':
        result = ('const', Component_name[-1])
        sequential = False
    elif comp_array[0].upper() == 'DFF':
        result = 'latch'
        sequential = True
    elif comp_array[0].upper() in ['OAI', 'AOI', 'AO', 'OA', 'AND','NAND', 'OR', 'NOR', 'XOR', 'XNOR', 'BUF', 'INV']:
        result = ('software', Component_name)
        sequential = False
    elif Component_name.upper() == 'MUX21':
        result = ('software', Component_name.upper())
        sequential = False
    elif comp_array[0].upper() == 'FLIP':
        result = ('software', comp_array[0].upper())
        sequential = True
    elif Component_name in internal_cells.keys():
        result = ('internal', Component_name)
        sequential = False
#   Raising an exception if the cell is not known
    else:
        raise Exception('unknown gate ' + Component_name + '. Maybe it is not yet implemented...')
#   retunring datas 
    return sequential, result
    
def BuildCell(toks, cell_name, internal_cells, output_name, ExtSeq=False):
    """BuildCell(toks)
    Build a model for each cells discribed in the internal library
    
    Models are stored in global variable internal_cells for easy access for other function in this script
    
    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param output_name: generic output name
    @param internal_cells: dictionnary to fill with builded model
    @param cell_name: name of the cell to build
    @keyword ExtSeq(False): indicate if the model should externalise DFF by formating connected nodes to indicate if they are inputs or outputs.
    """
    
#   Reading all internal cells
    for i in toks:
        if i[0] == cell_name:
            cell = i
            break
            
    int_in = [] #cell's input list
    int_out = [] #cell's output list
    
    print('-> Building cell ' + cell[0])
    CellName = cell[0]
    internal_cells[CellName] = [[], []]
    
    for ext_cell in cell[2]:
        if not internal_cells.has_key(ext_cell):
            BuildCell(toks, ext_cell, internal_cells, output_name, ExtSeq)
#       generating ports name from bus of the cell
    for IOBus in cell[1]:
        if len(IOBus) == 2:
            bus_len = 1
        else:
            bus_len = int(IOBus[2]) + 1
#           putting inputs in int_in
        if IOBus[1] == 'IN':
            for i in range(bus_len):
                if bus_len == 1:
                    int_in.append(IOBus[0])
                else:
                    int_in.append(IOBus[0] + '_' + str(i))
#           putting outputs in int_out
        else:
            for i in range(bus_len):
                if bus_len == 1:
                    int_out.append(IOBus[0])
                else:
                    int_out.append(IOBus[0] + '_' + str(i))

    internal_cells[CellName][0].extend(cell[3])
    
    for i, buf in enumerate(cell[4]):
        if not isinstance(buf[0], str):
            target = buf[0][0] + '_' + buf[0][1]
        else:
            target = buf[0]
            
        if not isinstance(buf[1], str):
            source = buf[1][0] + '_' + buf[1][1]
        else:
            source = buf[1]
        
        if (target in int_in) or (target in int_out):
            target = '{' + target + '}'
        if (source in int_in) or (source in int_out):
            source = '{' + source + '}'
            
        internal_cells[CellName][1].append([(source, target), 'buf', 'buf' + str(i)])
    
    for Instance in cell[5]:
        Inst_name = Instance[0]
        cell_ports = {}
        for IO in Instance[2]:
            if not isinstance(IO[0], str):
                port = IO[0][0] + '_' + IO[0][1]
            else:
                port = IO[0]
            
            if not isinstance(IO[1], str):
                node = IO[1][0] + '_' + IO[1][1]
            else:
                node = IO[1]
            if (node in int_in) or (node in int_out):
                node = '{' + node + '}'
                
            cell_ports[port] = node
#           Getting datas from GateFinder
        sequential, function = GateFinder(Instance[1], internal_cells)
#           if instanciated cell is a DFF and flag ExtSeq as true, use special syntax in the model : [<dir_char><signal_name>]
        if sequential and ExtSeq :
            for port in cell_ports:
                if not cell_ports[port] == 'OPEN':
                    if not port in output_name:
                        if not '[O' + cell_ports[port] + ']' in internal_cells[CellName][0]:
                            if not cell_ports[port][0] == '{':
                                internal_cells[CellName][0].remove(cell_ports[port])
                            internal_cells[CellName][0].append('[O' + cell_ports[port] + ']')
                    else:
                        if not '[I' + cell_ports[port] + ']' in internal_cells[CellName][0]:
                            if not cell_ports[port][0] == '{':
                                internal_cells[CellName][0].remove(cell_ports[port])
                            internal_cells[CellName][0].append('[I' + cell_ports[port] + ']')
        else:
#               type 'latch' 
            if function == 'latch':
                    output_linked = False
#                   adding edge for QB output if needed
                    if not cell_ports['QB'] == 'OPEN':
                        output_linked = True
                        internal_cells[CellName][1].append([(cell_ports['D'], cell_ports['QB']), 'not', 'dff_not_' + Inst_name])
#                   adding edge for Q output if needed
                    if not cell_ports['Q'] == 'OPEN':
                        output_linked = True
                        internal_cells[CellName][1].append([(cell_ports['D'], cell_ports['Q']),  'buf', 'dff_buf_' + Inst_name])
#                   raise an exception if no output linked
                    if not output_linked:
                        raise Exception('Q and QB of a non-reset latch not linked to a node')
#               type 'const'
            elif function[0] == 'const':
#                   adding const edge from the right const source
                internal_cells[CellName][1].append([('Logic' + function[1], cell_ports['O']), function[1], Inst_name])
#               type ao, aoi, oa , or oai
            elif function[0] == 'software':
#                   getting model from software library
                cell_vert = vhd_cells[function[1]][0]
                cell_edge = vhd_cells[function[1]][1]
#                   adding model nodes with instance name at the end
                for node in cell_vert:
                    internal_cells[CellName][0].append(node + '_' + Inst_name)
#                   adding model's edges 
                for edge in cell_edge:
#                       replacing source name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][0][0] == '{':
                        edge_source = cell_ports[edge[0][0][1:-1]]
                        if edge_source == 'OPEN':
                            edge_source = 'Logic0'
                    else:
                        edge_source = edge[0][0] + '_' + Inst_name
#                       replacing target name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][1][0] == '{':
                        edge_target = cell_ports[edge[0][1][1:-1]]
                    else:
                        edge_target = edge[0][1] + '_' + Inst_name
#                       adding edge to the modelin construction
                    internal_cells[CellName][1].append([(edge_source, edge_target), edge[1], edge[2] + '_' + Inst_name])
    
def AddGate(toks, g, prim_in, prim_out, nodes, top_cell, internal_cells, output_name, ExtSeq=False):
    """AddGate(toks)
    
    Add edges to the graph
        
    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete
    @param nodes: list to complete with signal which can be inputs of functions
    @param top_cell: Top cell ID in the results to build
    @param internal_cells: dictionnary of models to use for internal cell type
    @param output_name: generic output name
    @keyword ExtSeq(False): indicate if the graph should externalise sequential standards cells by putting connected nodes as input or output.
    """
    print("AddGates")
#   adding value forcing nodes
    g.add_vertex('Logic0',
                 label='Logic0',
                 color="#FF0000",
                 cat="const",
                 locks=[],
                 forced=[],
                 size=100,
                 label_size=30)
    g.add_vertex('Logic1',
                 label='Logic1',
                 color="#FF0000",
                 cat="const",
                 locks=[],
                 forced=[],
                 size=100,
                 label_size=30)
#   initializing var
    to_add_edges = {'and':[[],[]], 'nand':[[],[]], 'or':[[],[]], 'nor':[[],[]], 'xor':[[],[]], 'xnor':[[],[]], 'not':[[],[]], 'buf':[[],[]], '0':[[],[]], '1':[[],[]]}

    nodes.extend(toks[top_cell][3])
    
    for ext_cell in toks[top_cell][2]:
        if not internal_cells.has_key(ext_cell):
            BuildCell(toks, ext_cell, internal_cells, output_name, ExtSeq)
    
    for i in toks[top_cell][3]:
        g.add_vertex(i,
                         label=i,
                         color="#FFFFFF",
                         cat="node",
                         locks=[],
                         forced=[],
                         size=100,
                         label_size=30)
                         
    for i, buf in enumerate(toks[top_cell][4]):
        if not isinstance(buf[0], str):
            target = buf[0][0] + '_' + buf[0][1]
        else:
            target = buf[0]
            
        if not isinstance(buf[1], str):
            source = buf[1][0] + '_' + buf[1][1]
        else:
            source = buf[1]
            
        to_add_edges['buf'][0].append((source, target))
        to_add_edges['buf'][1].append('buf' + str(i))

#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[top_cell][5])) + '/' + nbdisp, end='')
#   reading instances of global cell
    for Instance in toks[top_cell][5]:
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
            
        Inst_name = Instance[0]
        cell_ports = {}
#       if cell doesn't have a model, separating inputs and output
        for IO in Instance[2]:
            if not isinstance(IO[0], str) :
                port = IO[0][0] + '_' + IO[0][1]
            else:
                port = IO[0]
            
            if not isinstance(IO[1], str):
                node = IO[1][0] + '_' + IO[1][1]
            else:
                node = IO[1]
                
            cell_ports[port] = node
#       gathering data from GateFinder
        sequential, function = GateFinder(Instance[1], internal_cells)
#       if instanciated cell is a DFF and flag ExtSeq as true, putting inputs and outputs nodes as ports of the global ports
        if sequential and ExtSeq :
            for IO in cell_ports:
#               setting node dir depending on instantiated cell's port dir
                if not IO in output_name:
                    direction = 'output'
                else:
                    direction = 'input'
#                   changing categeorie of linked nodes if internal node
                if not cell_ports[IO] == 'OPEN':
                    if g.vs.find(name=cell_ports[IO])['cat'] == 'node':
                        g.vs.find(name=cell_ports[IO])['cat'] = direction
                        if direction == 'input':
                            prim_in.append(cell_ports[IO])
                        else:
                            prim_out.append(cell_ports[IO])
                        
        else:
#               type 'latch' 
            if function == 'latch':
                output_linked = False
#                   adding edge for QB output if needed
                if not cell_ports['QB'] == 'OPEN':
                    output_linked = True
                    to_add_edges['not'][0].append((cell_ports['D'], cell_ports['QB']))
                    to_add_edges['not'][1].append('dff_not_' + Inst_name)
#                   adding edge for Q output if needed
                if not cell_ports['Q'] == 'OPEN':
                    output_linked = True
                    to_add_edges['buf'][0].append((cell_ports['D'], cell_ports['Q']))
                    to_add_edges['buf'][1].append('dff_buf_' + Inst_name)
#                   raise an exception if no output linked
                if not output_linked:
                    raise Exception('Q and Q\' of a non-reset latch not linked to a node')
#               type 'const'
            elif function[0] == 'const':
#               adding const edge from the right const source
                to_add_edges[function[1]][0].append(('Logic' + function[1], cell_ports['O']))
                to_add_edges[function[1]][1].append(Inst_name)
#           type ao, aoi, oa , or oai
            elif function[0] in ['internal', 'software']:
#               getting model from the right library
                if function[0] == 'internal':
                    cell_vert = internal_cells[function[1]][0]
                    cell_edge = internal_cells[function[1]][1]
                elif function[0] == 'software':
                    cell_vert = vhd_cells[function[1]][0]
                    cell_edge = vhd_cells[function[1]][1]
#               Adding model's nodes
                for node in cell_vert:
#                   checking if the node should be placed as in put or output and defining attributes
                    if node[0] == '[':
                        node_name = node[2:-1]
                        if node[1] == 'I':
                            node_color = '#DDDDDD'
                            categorie = 'input'
                        elif node[1] == 'O':
                            node_color = '#666666'
                            categorie = 'output'
                        else:
                            raise Exception('wrong internal IO latch def : ' + node)
                    else:
                        node_name = node
                        categorie = 'node'
                        node_color = '#FFFFFF'
#                   adding node with defined attributes
                    if node_name[0] == '{':
                        if g.vs.find(name=cell_ports[node_name[1:-1]])['cat'] == 'node':
                            g.vs.find(name=cell_ports[node_name[1:-1]])['cat'] = categorie
                            if categorie == 'input':
                                prim_in.append(cell_ports[node_name[1:-1]])
                            else:
                                prim_out.append(cell_ports[node_name[1:-1]])
                    else:
                        g.add_vertex(node_name + '_' + Inst_name,
                                     label=node + '_' + Inst_name,
                                     color=node_color,
                                     cat=categorie,
                                     locks=[],
                                     forced=[],
                                     size=100,
                                     label_size=30)
#               adding model's edges 
                for edge in cell_edge:
#                   replacing source name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][0][0] == '{':
                        edge_source = cell_ports[edge[0][0][1:-1]]
                        if edge_source == 'OPEN':
                            edge_source = 'Logic0'
                    else:
                        edge_source = edge[0][0] + '_' + Inst_name
#                   replacing target name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][1][0] == '{':
                        edge_target = cell_ports[edge[0][1][1:-1]]
                    else:
                        edge_target = edge[0][1] + '_' + Inst_name
#                   adding edge to the model in construction
                    to_add_edges[edge[1]][0].append((edge_source, edge_target))
                    to_add_edges[edge[1]][1].append(edge[2] + '_' + Inst_name)
#           other types
            else:
#               adding edge from each input to output using type as function
                print(function)
                edges_list = [[],[]]
                for inp in cell_ports:
                    if not inp in output_name:
                        edges_list[0].append((inp, cell_ports[output_name[0]]))
                        edges_list[1].append(Inst_name)
                to_add_edges[function][0].extend(edges_list[0])
                to_add_edges[function][1].extend(edges_list[1])
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb))
    
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add_edges, key=lambda i: len(to_add_edges[i][0]), reverse=False):
        if len(to_add_edges[i][0]) > 0:
            print('-> Adding ' + str(len(to_add_edges[i][0])) + ' ' + i + '...')
            adv_add_edges(g, to_add_edges[i][0], to_add_edges[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")
                
#def build(name, ExtSeq=False, result=None, parsedbg=False):
def build(netlist, result=None, parsedbg=False, TopCell=None, ExtSeq=False, verbose=True):
    """build(name, result=None, parsedbg=False)
    Builds the graph from an EDIF netlist
    @param netlist: name of the netlist to build
    @keyword ExtSeq(False): allow user to externalise DFF by putting connected nodes as input or output. Routed to graph building functions
    @keyword result(None): result set from the edif syntax reader. Avoid reading and use set given instead. Used mainly for debugging.
    @keyword parsedbg(False): Boolean used to disable graph generating and return the syntax reader set of the netlist choosen. Used mainly for debugging.
    
    @return a tuple of 4 elements : the graph for the netlist, the list of inputs, the list of outputs, the list of node (inputs and internal nodes)
    """
        
    print("---- Build Structural VHDL")
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
        
    g = ig.Graph(directed=1)
    prim_in = []    #primary inputs of the netlist
    prim_out = []   #primary outputs of the netlist
    nodes = []      #nodes of the netlist
    internal_cells = {}
    output_name = ['Y', 'Q', 'QB']
#   Setting basics elements
    libraryIEEE = pp.Suppress(pp.Literal('library IEEE;') + pp.Literal('use IEEE.STD_LOGIC_1164.all;'))
    name = pp.Word(pp.alphanums + '_\[')
    port = pp.Group(name + pp.Suppress(pp.Literal(':')) + pp.Or(['IN', 'OUT']) + pp.Or([pp.Suppress(pp.Literal('std_logic_vector (')) + pp.Word(pp.nums) + pp.Suppress(pp.Literal('DOWNTO')) + pp.Word(pp.nums) + pp.Suppress(')'), pp.Suppress(pp.Literal('std_logic'))]) + pp.Suppress(pp.Optional(';')))
    sigRef = pp.Or([pp.Group(name + pp.Suppress(pp.Literal('(')) + pp.Word(pp.nums) + pp.Suppress(pp.Literal(')'))), name])
    
    signals = pp.Group(pp.Optional(pp.Suppress(pp.Literal('signal')) + pp.ZeroOrMore(name + pp.Suppress(pp.Literal(','))) + name + pp.Suppress(pp.Literal(':') + pp.Literal('std_logic') + pp.Literal(';'))))
    buf = pp.Group(sigRef + pp.Suppress(pp.Literal('<=')) + sigRef + pp.Suppress(pp.Literal(';')))
    instance = pp.Group(name + pp.Suppress(pp.Literal(':')) + name + pp.Suppress(pp.Literal('port map (')) + pp.Group(pp.OneOrMore(pp.Group(sigRef + pp.Suppress(pp.Literal('=>')) + sigRef + pp.Suppress(pp.Optional(pp.Literal(',')))))) + pp.Suppress(pp.Literal(')') + pp.Literal(';')))
    library = pp.Group(pp.Optional(pp.OneOrMore(pp.Suppress(pp.Literal('component')) + name + pp.Suppress(pp.Literal('port (') + pp.OneOrMore(port) + pp.Literal(') ;') + pp.Literal('end component') + pp.Literal(';')))))
    content = pp.Suppress(pp.Literal('begin')) + pp.Group(pp.ZeroOrMore(buf)) + pp.Group(pp.OneOrMore(instance))
    
    entity = pp.Suppress(pp.Literal('entity')) + name + pp.Suppress(pp.Literal('is') + pp.Literal('port (')) + pp.Group(pp.OneOrMore(port)) + pp.Suppress(pp.Literal(') ;') + pp.Literal('end') + name + pp.Literal(';'))
    architecture = pp.Suppress(pp.Literal('architecture') + name + pp.Literal('of') + name + pp.Literal('is')) + library + signals + content + pp.Suppress(pp.Literal('end') + name + pp.Literal(';'))
    
    FullParser = pp.Suppress(pp.SkipTo(libraryIEEE)) + pp.OneOrMore(pp.Group(libraryIEEE + entity + architecture))
#   Opening netlist file
    source_file = netlist
#   Reading the file if no results given
    if result == None:
        print("Parsing...")
        result = FullParser.parseFile(source_file)
#   Returning only the result from the file if asked by user
    if parsedbg:
        if not verbose:
            sys.stdout = sys.__stdout__
        print('---- Done (parsedbg)\n')
        return result
#   Building and returning the graphe
    else:
        if TopCell == None:
            TopCell = TopCellSelect(result, verbose)
        elif TopCell >= len(result):
            TopCell = 0
        AddIO(result, g, prim_in, prim_out, nodes, TopCell)
        AddGate(result, g, prim_in, prim_out, nodes, TopCell, internal_cells, output_name, ExtSeq)
        print('---- Done\n')
        
        g = clean_const.clean_const(g)
        g = clean_const.clean_not_buf(g)
            
        if not verbose:
            sys.stdout = sys.__stdout__
            print('---- Done\n')
            
        return g, prim_in, prim_out, nodes

if __name__ == "__main__":
    build("./or_struct.vhd")
