# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Building the graph from the netlist
Project    : Graph-based nodes selection for functional locking

File       : build_def.py
Last update: 2015-03-17
"""

from __future__ import print_function
import igraph as ig
import pyparsing as pp
import clean_const
from cells_dict import edif_cells
import time
import sys
import os

#pp.ParserElement.enablePackrat()

def adv_add_edges(g, es, instances, **kwds):
    """adv_add_edges(es, **kwds)
    
    Adds multiple edges to the graph with a unique set of keywords.
    
    Keyword arguments (except the source and target arguments) will be
    assigned to added edges as attributes.
    @param g : the graph where to add given edges    
    @param es: list of source - dest tuples to add
    @param instances: list of instances names to add corresponding to edges
    @param **kwds : attributes to add to all the edges
    
    @return result of igraph.add_edges()
    """
    
    if len(es) != len(instances):
        raise Exception('Length of es different than length of instances')
    if not kwds:
        return g.add_edges(es)
#   Getting next edge ID
    eid = g.ecount()
#   Adding all the edges from es to the graphe
    result = g.add_edges(es)
#   Adding Keywords and instance name to all the edges
    for i, _ in enumerate(es):
        for key, value in kwds.iteritems():
            g.es[eid + i][key] = value
        g.es[eid + i]['instance'] = instances[i]
    return result
    
def AddIO(toks, g, prim_in, prim_out, nodes):
    """AddIO(toks)
    
    Add Inputs and Outputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list to fill with inputs
    @param prim_out: list to fill with outputs
    @param nodes: list to fill with signal which can be inputs of functions
    """
    print("AddIO") #Debug
#   Searching for global cell id in the internal lib
    for ID, cell in enumerate(toks[1]):
        if cell[0] == toks[2]:
            global_cell_ID = ID
            break
#   Reading all the IO bus of the global cell
    for IODecl in toks[1][global_cell_ID][1]:
#       Looping for the size of the bus
        for i in range(int(IODecl[1])):
#           Getting direction of the port and adding id in bus if multiple ports in the same bus
            Dir = IODecl[2]
            if int(IODecl[1]) == 1:
                InputOutput = IODecl[0]
            else:
                InputOutput = IODecl[0] + '_' + str(i)
#           Adding ports
            if Dir == "INPUT":
                if not InputOutput in prim_in:
                    g.add_vertex(InputOutput,
                                 label=InputOutput,
                                 color="#DDDDDD",
                                 cat="input",
                                 locks=[],
                                 forced=[],
                                 size=100,
                                 label_size=30)
                    prim_in.append(InputOutput)
                    nodes.append(InputOutput)
            
            elif Dir == "OUTPUT":
                if (not InputOutput in prim_in) and (not InputOutput in prim_out):
                    prim_out.append(InputOutput)
                    g.add_vertex(InputOutput,
                                 label=InputOutput,
                                 color="#666666",
                                 cat="output",
                                 locks=[0, 1],#By convention
                                 forced=[],
                                 size=100,
                                 label_size=30)

def GateFinder(Component_name, internal_cells):
    """GateFinder(Component_name)
    
    Dictionnary which allow to find which model associate with which cell name    
    If the cell isn't described, raise an Exception with the name of the gate which caused the error.
    
    @param Component_name: name of the edif cell to represent
    @param internal_cells: dictionnary containing models of internal cells to determine internal cell's type
    
    @return the gate type corresponding to the name given in Component_name.
            Can be:
            - The name of a standard cell (not, buf, and, nand, or, nor, xor, xnor)
            - A tuple containing first the library (internal or software) where the cell is described, and second the name of the cell in this library.
    """
#   removing useless informations before checking cell type
    comp_array = Component_name.split('_')
    number_del = ''
    while comp_array[0][-1] in '0123456789':
#       memorizing removed number for some uses
        number_del = comp_array[0][-1] + number_del
        comp_array[0] = comp_array[0][:-1]
#   returning datas depending on the cell name
    if comp_array[0].upper() == 'INV':
        result = 'not'
        sequential = False
    elif Component_name[0:5].upper() == 'LOGIC':
        result = ('const', Component_name[-1])
        sequential = False
    elif comp_array[0].upper() == 'DFF':
        result = 'latch'
        sequential = True
    elif comp_array[0].upper() in ['AND','NAND', 'OR', 'NOR', 'XOR', 'XNOR', 'BUF']:
        result = comp_array[0].lower()
        sequential = False
    elif comp_array[0].upper() in ['OAI', 'AOI', 'AO', 'OA']:
        result = (comp_array[0].lower(), number_del)
        sequential = False
    elif Component_name.upper() == 'MUX21':
        result = ('software', Component_name.upper())
        sequential = False
    elif comp_array[0].upper() == 'FLIP':
        result = ('software', comp_array[0].upper())
        sequential = True
    elif Component_name in internal_cells.keys():
        result = ('internal', Component_name)
        sequential = False
#   Raising an exception if the cell is not known
    else:
        raise Exception('unknown gate ' + Component_name + '. Maybe it is not yet implemented...')
#   retunring datas 
    return sequential, result
    
def BuildCell(toks, gate_dict, internal_cells, ExtSeq=False):
    """BuildCell(toks)
    Build a model for each cells discribed in the internal library
    
    Models are stored in global variable internal_cells for easy access for other function in this script
    
    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param gate_dict: dictionnary containing ports of cells used in the file
    @param internal_cells: dictionnary to fill with models builded
    @keyword ExtSeq(False): indicate if the model should externalise DFF by formating connected nodes to indicate if they are inputs or outputs.
    """
    alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
#   building a dictionary containig all ports of cells
    for lib in toks[0]:
        for Gate in lib[1]:
            IOList = []
            for IOBus in Gate[1]:
                for bit in range(int(IOBus[1])):
                    IOList.append((IOBus[0] + '_' + str(bit), IOBus[2]))
            gate_dict[Gate[0]] = IOList
    
    print('Building Cells')
#   Reading all internal cells
    for cell in toks[1]:
        
        int_in = [] #cell's input list
        int_out = [] #cell's output list
        int_conv_name = {} #cell's dict of name conv
        
#       Checking if current cell is global cell
        if cell[0] == toks[2]:
#           Skip the final cell
            continue 
        
        print('-> Building cell ' + cell[0])
        CellName = cell[0]
        internal_cells[CellName] = [[], []]
#       generating ports name from bus of the cell
        for IOBus in cell[1]:
#           putting inputs in int_in
            if IOBus[2] == 'INPUT':
                for i in range(int(IOBus[1])):
                    int_in.append(IOBus[0] + '_' + str(i))
#           putting outputs in int_out
            else:
                for i in range(int(IOBus[1])):
                    int_out.append(IOBus[0] + '_'  + str(i))
#       Reading all links to build links' dictionary
        for Net in cell[3]:
            node_name = Net[0]
#           adding the signal to the model if not an input or an output
            if (not node_name in int_in) and (not node_name in int_out):
                internal_cells[CellName][0].append(node_name)
#           adding each port linked to the signal in the links' dictionary
            for signal in Net[1:]:
                signal_name = signal[0] + '_' + signal[1]
#               if instance reference of the port is 'none', the port is part of IOs of the cell
                if signal[2] != 'none':
                    int_conv_name[signal_name + '@' + signal[2]] = node_name
                else:
#                   adding an edge between the port and the signal if the name is different
                    if signal_name != node_name:
                        if signal_name in int_in:
                            internal_cells[CellName][1].append([('{' + signal_name + '}', node_name), 'buf', '{' + signal_name + '}' + 'TO' + node_name])
                        elif signal_name in int_out:
                            internal_cells[CellName][1].append([(node_name, '{' + signal_name + '}'), 'buf', node_name + 'TO' + '{' + signal_name + '}'])
#                       If instance ref is 'none' and port is not an input nor an output, it's incoherent. Raise an exception
                        else:
                            raise Exception("alone signal " + signal_name + ' in net ' + node_name + " not an input nor an output")
#       Reading all instance of the cell
        for Instance in cell[2]:
            Inst_name = Instance[0]
            inputs = []
            output = ''
#           if the cell isn't modelised, spliting inputs from output
            if (not Instance[1] in internal_cells.keys()) and not(Instance[1] == 'dff'): 
                for IO in gate_dict[Instance[1]]:
                    global_IO_name = IO[0] + '@' + Inst_name
                    if IO[1] == 'INPUT':
                        inputs.append(int_conv_name[global_IO_name])
                    else:
                        if output == '':
                            output = int_conv_name[global_IO_name]
                        else:
                            print('Warning ! : more than one Output for Gate ' + Instance[1])
#           Getting datas from GateFinder
            sequential, function = GateFinder(Instance[1], internal_cells)
#           if instanciated cell is a DFF and flag ExtSeq as true, use special syntax in the model : [<dir_char><signal_name>]
            if sequential and ExtSeq :
                for IO in gate_dict[Instance[1]]:
#                   replacing names in nodes list
                    if int_conv_name.has_key(IO[0] + '@' + Inst_name):
                        if IO[1] == 'INPUT':
                            if not '[O' + int_conv_name[IO[0] + '@' + Inst_name] + ']' in internal_cells[0]:
                                internal_cells[0].remove(int_conv_name[IO[0] + '@' + Inst_name])
                                internal_cells[0].append('[O' + int_conv_name[IO[0] + '@' + Inst_name] + ']')
                        if IO[1] == 'OUTPUT':
                            if not '[I' + int_conv_name[IO[0] + '@' + Inst_name] + ']' in internal_cells[0]:
                                internal_cells[0].remove(int_conv_name[IO[0] + '@' + Inst_name])
                                internal_cells[0].append('[I' + int_conv_name[IO[0] + '@' + Inst_name] + ']')
            else:
#               type 'latch' 
                if function == 'latch':
                    output_linked = False
#                   adding edge for QB output if needed
                    if int_conv_name.has_key('QB_0@' + Inst_name):
                        output_linked = True
                        internal_cells[CellName][1].append([(int_conv_name['D_0@' + Inst_name], int_conv_name['QB_0@' + Inst_name]), 'not', 'not_' + Inst_name])
#                   adding edge for Q output if needed
                    if int_conv_name.has_key('Q_0@' + Inst_name):
                        output_linked = True
                        internal_cells[CellName][1].append([(int_conv_name['D_0@' + Inst_name], int_conv_name['Q_0@' + Inst_name]),  'buf', 'buf_' + Inst_name])
#                   raise an exception if no output linked
                    if not output_linked:
                        raise Exception('Q and Q\' of a non-reset latch not linked to a node')
#               type 'const'
                elif function[0] == 'const':
#                   adding const edge from the right const source
                    internal_cells[CellName][1].append([('Logic' + function[1], int_conv_name['O_0@' + Inst_name]), function[1], Inst_name])
#               type ao, aoi, oa , or oai
                elif function[0] in ['oai', 'aoi', 'ao', 'oa']:
#                   looking for functions to use
                    if function[0][0] == 'a':
                        lvl1 = 'and'
                        if function[0][-1] == 'i':
                            lvl2 = 'nor'
                        else:
                            lvl2 = 'or'
                    else:
                        lvl1 = 'or'
                        if function[0][-1] == 'i':
                            lvl2 = 'nand'
                        else:
                            lvl2 = 'and'
                            
                    to_group = []
                    inp_ID = 0
#                   reading numbers form the end of cell name
                    for i, group in enumerate(function[1]):
#                       grouping inputs with level 1 functions and adding functions outputs to 'to_group'
                        if not group == '1':
                            internal_cells[CellName][0].append(alpha[i] + '_' + Inst_name)
                            to_group.append(alpha[i] + '_' + Inst_name)
                            for _ in range(int(group)):
                                internal_cells[CellName][1].append([(inputs[inp_ID], alpha[i] + '_' + Inst_name), lvl1, lvl1 + str(i) + '_' + Inst_name])
                                inp_ID += 1
#                       if only one input, adding it to 'to_group'
                        else:
                            to_group.append(inputs[inp_ID])
                            inp_ID += 1
#                   grouping nodes in 'to_group' with level 2 function to the output
                    for node in to_group:
                        internal_cells[CellName][1].append([(node, output), lvl2, lvl2 + '_' + Inst_name])
#               type 'software'
                elif function[0] == 'software':
#                   getting model from software library
                    cell_vert = edif_cells[function[1]][0]
                    cell_edge = edif_cells[function[1]][1]
#                   adding model nodes with instance name at the end
                    for node in cell_vert:
                        internal_cells[CellName][0].append(node + '_' + Inst_name)
#                   adding model's edges 
                    for edge in cell_edge:
#                       replacing source name between brackets '{}' by corresponding name from links' dictionary
                        if edge[0][0][0] == '{':
                            edge_source = int_conv_name[edge[0][0][1:-1] + '@' + Inst_name]
                        else:
                            edge_source = edge[0][0] + '_' + Inst_name
#                       replacing target name between brackets '{}' by corresponding name from links' dictionary
                        if edge[0][1][0] == '{':
                            edge_target = int_conv_name[edge[0][1][1:-1] + '@' + Inst_name]
                        else:
                            edge_target = edge[0][1] + '_' + Inst_name
#                       adding edge to the modelin construction
                        internal_cells[CellName][1].append([(edge_source, edge_target), edge[1], edge[2] + '_' + Inst_name])
#               other types
                else:
#                   adding edge from each input to output using type as function
                    for inp in inputs:
                        internal_cells[CellName][1].append([(inp, output), function, Inst_name])
    
def AddGate(toks, g, gate_dict, name_conv, internal_cells, ExtSeq=False):
    """AddGate(toks)
    
    Add edges to the graph
        
    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete
    @param gate_dict: dictionnary of ports for gates used in the file
    @param name_conv: dictionnary of node name linked to ports (using global ports names)
    @param internal_cells: dictionnary of models to use for internal cell type
    @keyword ExtSeq(False): indicate if the graph should externalise sequential standards cells by putting connected nodes as input or output.
    """
    print("AddGates")
#   adding value forcing nodes
    g.add_vertex('Logic0',
                 label='Logic0',
                 color="#FF0000",
                 cat="const",
                 locks=[],
                 forced=[],
                 size=100,
                 label_size=30)
    g.add_vertex('Logic1',
                 label='Logic1',
                 color="#FF0000",
                 cat="const",
                 locks=[],
                 forced=[],
                 size=100,
                 label_size=30)
#   initializing vars
    alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    to_add_edges = {'and':[[],[]], 'nand':[[],[]], 'or':[[],[]], 'nor':[[],[]], 'xor':[[],[]], 'xnor':[[],[]], 'not':[[],[]], 'buf':[[],[]], '0':[[],[]], '1':[[],[]]}
#   searching for global cell id in the internal lib
    for ID, cell in enumerate(toks[1]):
        if cell[0] == toks[2]:
            global_cell_ID = ID
            break
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[1][global_cell_ID][2])) + '/' + nbdisp, end='')
#   reading instances of global cell
    for Instance in toks[1][global_cell_ID][2]:
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
            
        Inst_name = Instance[0]
        inputs = []
        output = ''
#       if cell doesn't have a model, separating inputs and output
        if not(Instance[1].upper() in edif_cells.keys()) and not(Instance[1] in internal_cells.keys()) and not(Instance[1] == 'dff'): 
            for IO in gate_dict[Instance[1]]:
                global_IO_name = IO[0] + '@' + Inst_name
                if IO[1] == 'INPUT':
                    inputs.append(name_conv[global_IO_name])
                else:
                    if output == '':
                        output = name_conv[global_IO_name]
                    else:
                        print('Warning ! : more than one Output for Gate ' + Instance[1])
#       gathering data from GateFinder
        sequential, function = GateFinder(Instance[1], internal_cells)
#       if instanciated cell is a DFF and flag ExtSeq as true, putting inputs and outputs nodes as ports of the global ports
        if sequential and ExtSeq :
            for IO in gate_dict[Instance[1]]:
#               setting node dir depending on instantiated cell's port dir
                if name_conv.has_key(IO[0] + '@' + Inst_name):
                    if IO[1] == 'INPUT':
                        direction = 'output'
                    if IO[1] == 'OUTPUT':
                        direction = 'input'
#                   changing categeorie of linked nodes if internal node
                    if g.vs.find(name=name_conv[IO[0] + '@' + Inst_name])['cat'] == 'node':
                        g.vs.find(name=name_conv[IO[0] + '@' + Inst_name])['cat'] = direction
                        
        else:
#           type 'latch'
            if function == 'latch':
                output_linked = False
#               adding edge for QB output if needed
                if name_conv.has_key('QB_0@' + Inst_name):
                    output_linked = True
                    to_add_edges['not'][0].append((name_conv['D_0@' + Inst_name], name_conv['QB_0@' + Inst_name]))
                    to_add_edges['not'][1].append('not_' + Inst_name)
#               adding edge for Q output if needed
                if name_conv.has_key('Q_0@' + Inst_name):
                    output_linked = True
                    to_add_edges['buf'][0].append((name_conv['D_0@' + Inst_name], name_conv['Q_0@' + Inst_name]))
                    to_add_edges['buf'][1].append('buf_' + Inst_name)
#               raise an exception if no output linked
                if not output_linked:
                    raise Exception('Q and Q\' of a non-reset latch not linked to a node')
#           type 'const'
            elif function[0] == 'const':
#               adding const edge from the right const source
                to_add_edges[function[1]][0].append(('Logic' + function[1], name_conv['O_0@' + Inst_name]))
                to_add_edges[function[1]][1].append(Inst_name)
#           type ao, aoi, oa , or oai
            elif function[0] in ['oai', 'aoi', 'ao', 'oa']:
#               looking for functions to use
                if function[0][0] == 'a':
                    lvl1 = 'and'
                    if function[0][-1] == 'i':
                        lvl2 = 'nor'
                    else:
                        lvl2 = 'or'
                else:
                    lvl1 = 'or'
                    if function[0][-1] == 'i':
                        lvl2 = 'nand'
                    else:
                        lvl2 = 'and'
                        
                to_group = []
                inp_ID = 0
#               reading numbers form the end of cell name
                for i, group in enumerate(function[1]):
#                   grouping inputs with level 1 functions and adding functions outputs to 'to_group'
                    if not group == '1':
                        g.add_vertex(alpha[i] + '_' + Inst_name,
                                     label=alpha[i] + '_' + Inst_name,
                                     color="#FFFFFF",
                                     cat="node",
                                     locks=[],
                                     forced=[],
                                     size=100,
                                     label_size=30)
                        to_group.append(alpha[i] + '_' + Inst_name)
                        for _ in range(int(group)):
                            to_add_edges[lvl1][0].append((inputs[inp_ID], alpha[i] + '_' + Inst_name))
                            to_add_edges[lvl1][1].append(lvl1 + str(i) + '_' + Inst_name)
                            inp_ID += 1
#                   if only one input, adding it to 'to_group'
                    else:
                        to_group.append(inputs[inp_ID])
                        inp_ID += 1
#               grouping nodes in 'to_group' with level 2 function to the output
                for node in to_group:
                    to_add_edges[lvl2][0].append((node, output))
                    to_add_edges[lvl2][1].append(lvl2 + '_' + Inst_name)
#           type 'software' and 'internal'
            elif function[0] in ['internal', 'software']:
#               getting model from the right library
                if function[0] == 'internal':
                    cell_vert = internal_cells[function[1]][0]
                    cell_edge = internal_cells[function[1]][1]
                elif function[0] == 'software':
                    cell_vert = edif_cells[function[1]][0]
                    cell_edge = edif_cells[function[1]][1]
#               Adding model's nodes
                for node in cell_vert:
#                   checking if the node should be placed as in put or output and defining attributes
                    if node[0] == '[':
                        node_name = node[2:-1]
                        if node[1] == 'I':
                            node_color = '#DDDDDD'
                            categorie = 'input'
                        if node[0] == 'O':
                            node_color = '#666666'
                            categorie = 'output'
                        else:
                            raise Exception('wrong internal IO latch def : ' + node)
                    else:
                        node_name = node
                        categorie = 'node'
                        node_color = '#FFFFFF'
#                   adding node with defined attributes
                    g.add_vertex(node_name + '_' + Inst_name,
                                 label=node + '_' + Inst_name,
                                 color=node_color,
                                 cat=categorie,
                                 locks=[],
                                 forced=[],
                                 size=100,
                                 label_size=30)
#               adding model's edges 
                for edge in cell_edge:
#                   replacing source name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][0][0] == '{':
                        edge_source = name_conv[edge[0][0][1:-1] + '@' + Inst_name]
                    else:
                        edge_source = edge[0][0] + '_' + Inst_name
#                   replacing target name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][1][0] == '{':
                        edge_target = name_conv[edge[0][1][1:-1] + '@' + Inst_name]
                    else:
                        edge_target = edge[0][1] + '_' + Inst_name
#                   adding edge to the model in construction
                    to_add_edges[edge[1]][0].append((edge_source, edge_target))
                    to_add_edges[edge[1]][1].append(edge[2] + '_' + Inst_name)
#           other types
            else:
#               adding edge from each input to output using type as function
                edges_list = [[],[]]
                for inp in inputs:
                    edges_list[0].append((inp, output))
                    edges_list[1].append(Inst_name)
                to_add_edges[function][0].extend(edges_list[0])
                to_add_edges[function][1].extend(edges_list[1])
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb))    
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add_edges, key=lambda i: len(to_add_edges[i][0]), reverse=False):
        if len(to_add_edges[i][0]) > 0:
            print('-> Adding ' + str(len(to_add_edges[i][0])) + ' ' + i + '...')
            adv_add_edges(g, to_add_edges[i][0], to_add_edges[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")
        
def AddNets(toks, g, prim_in, prim_out, nodes, name_conv):
    """AddNets(toks)
    Add nodes to the graph and build name_conv dictionary used to link instances of the global cell to the right nodes. 
    Add buffers to output and from inputs if needed
    
    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signal which can be inputs of functions
    @param name_conv: dictionnary to fill wth nodes names linked with ports (using globla names)
    """
    print("AddNets")
#   searching for global cell id in the internal lib
    for ID, cell in enumerate(toks[1]):
        if cell[0] == toks[2]:
            global_cell_ID = ID
            break    
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[1][global_cell_ID][3])) + '/' + nbdisp, end='')
#   Reading ports links of the global cell
    for Def in toks[1][global_cell_ID][3]:
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
#       Editing node name to make it match 
        if Def[0][-1] == '_':
            node_name = Def[0][:-1].replace('__', '00') + 'n'
        else:
            node_name = Def[0].replace('__', '00')
#       adding the signal to the model if not an input or an output
        if (not node_name in prim_in) and (not node_name in prim_out):
            g.add_vertex(node_name,
                         label=node_name,
                         color="#FFFFFF",
                         cat="node",
                         locks=[],
                         forced=[],
                         size=100,
                         label_size=30)
            nodes.append(node_name)
#       adding each port linked to the signal in the links' dictionary
        for signal in Def[1:]:
#           if instance reference of the port is 'none', the port is part of IOs of the cell
            if signal[2] != 'none':
                name_conv[signal[0] + '_' + signal[1] + '@' + signal[2]] = node_name
            else:
#               adding id of port in the bus if signal isn't in input or in output as it is
                if (not signal[0] in prim_in) and (not signal[0] in prim_out):
                    signal_name = signal[0] + '_' + signal[1]
                else:
                    signal_name = signal[0]
#               adding an edge between the port and the signal if the name is different
                if signal_name != node_name:
                    if signal_name in prim_in:
                        adv_add_edges(g, [(signal_name, node_name)], [signal_name + 'TO' + node_name],
                                      name='buf',
                                      label='buf',
                                      width=5,
                                      arrow_size=2,
                                      label_size=40,
                                      color="#AAAAAA")
                    elif signal_name in prim_out:
                        adv_add_edges(g, [(node_name, signal_name)], [node_name + 'TO' + signal_name],
                                      name='buf',
                                      label='buf',
                                      width=5,
                                      arrow_size=2,
                                      label_size=40,
                                      color="#AAAAAA")
#                   If instance ref is 'none' and port is not an input nor an output, it's incoherent. Raise an exception
                    else:
                        raise Exception("alone signal " + signal_name + ' in net ' + node_name + " not an input nor an output")
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb))
                
def build(name, ExtSeq=False, result=None, parsedbg=False, verbose=True):
    """build(name, result=None, parsedbg=False)
    Builds the graph from an EDIF netlist
    @param name: name of the netlist to build
    @keyword ExtSeq(False): allow user to externalise DFF by putting connected nodes as input or output. Routed to graph building functions
    @keyword result(None): result set from the edif syntax reader. Avoid reading and use set given instead. Used mainly for debugging.
    @keyword parsedbg(False): Boolean used to disable graph generating and return the syntax reader set of the netlist choosen. Used mainly for debugging.
    
    @return a tuple of 4 elements : the graph for the netlist, the list of inputs, the list of outputs, the list of node (inputs and internal nodes)
    """
    print("---- Build EDIF")
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    g = ig.Graph(directed=1)
    prim_in = []    #primary inputs of the netlist
    prim_out = []   #primary outputs of the netlist
    nodes = []      #nodes of the netlist
    name_conv = {}
    internal_cells = {}
    gate_dict = {}
#   Setting basics elements
    GateDefStart = pp.Suppress(pp.Literal("(cell"))
    ExtLibStart = pp.Suppress(pp.Literal("(external"))
    Node = pp.Word(pp.alphanums + '_&')
    Name = pp.Word(pp.alphanums + '_')
    PortRefStart = pp.Suppress(pp.Literal("(portRef"))
    InternCellStart = pp.Suppress(pp.Literal('(cell ') + pp.NotAny(pp.CaselessLiteral('logic')))
    InstanceStart = pp.Suppress(pp.Literal("(instance"))
    NetStart = pp.Suppress(pp.Literal("(net"))
#   Setting data elements
    GatePort = pp.Group(pp.Suppress(pp.Literal("(port")) + pp.Optional(pp.Suppress(pp.Literal('(array'))) + pp.Optional(pp.Suppress(pp.Literal('(rename'))) + Node + pp.Optional(pp.Suppress(pp.SkipTo(pp.Word(pp.nums) + pp.White(' '), failOn=pp.Literal("(direction"))) + pp.Word(pp.nums), default='1') + pp.Suppress(pp.SkipTo(pp.Literal("(direction"), include=True)) + pp.Or([pp.CaselessLiteral('INPUT'), pp.CaselessLiteral('OUTPUT')]) + pp.Suppress('))'))
    PortRef = pp.Group(PortRefStart + pp.Optional(pp.Suppress(pp.Literal("(member"))) + Node + pp.Optional(pp.Word(pp.nums), default='0') + pp.Optional(pp.Suppress(')')) + pp.Optional(pp.Suppress(pp.Literal("(instanceRef")) + Node + pp.Suppress(')'), default='none') + pp.Optional(pp.Suppress(')')))
    GateDef = pp.Group(GateDefStart + Name + pp.Suppress(pp.SkipTo(pp.Literal("(interface"), include=True)) + pp.Group(pp.OneOrMore(GatePort)) + pp.Suppress(')') + pp.Suppress(')') + pp.Suppress(')'))
    CellDesc = InternCellStart + Name + pp.Suppress(pp.SkipTo(pp.Literal("(interface"), include=True)) + pp.Group(pp.OneOrMore(GatePort))
    Instance = pp.Group(InstanceStart + pp.Optional(pp.Suppress(pp.Literal("(rename"))) + Node + pp.Suppress(pp.SkipTo(pp.Literal("(cellRef"), include=True)) + Name + pp.Suppress(pp.Or([pp.Literal('(libraryRef') + Name + pp.Literal('))'), pp.Literal(')')])) + pp.Suppress(')') + pp.Suppress(')'))
    Net = pp.Group(NetStart + pp.Optional(pp.Suppress(pp.Literal("(rename"))) + Node + pp.Suppress(pp.SkipTo(PortRefStart)) + pp.OneOrMore(PortRef) + pp.Optional(pp.Suppress(')')) + pp.Optional(pp.Suppress(')')))
#   Grouping data elements
    InternCell = pp.Group(pp.Suppress(pp.SkipTo(InternCellStart)) + CellDesc + pp.Group(pp.Suppress(pp.SkipTo(InstanceStart)) + pp.OneOrMore(Instance)) + pp.Group(pp.Suppress(pp.SkipTo(NetStart)) + pp.Dict(pp.OneOrMore(Net))))
    ExtLib = pp.Group(ExtLibStart + Name + pp.Group(pp.Optional(pp.Suppress(pp.SkipTo(GateDefStart, failOn=pp.Literal('(external'))) + pp.OneOrMore(GateDef))))
    InternLib = pp.Group(pp.OneOrMore(InternCell))
    FinalCell = pp.Suppress(pp.SkipTo(pp.Literal("(cellRef"), include=True)) + Name
#   Setting full parser
    FullParser = pp.Group(pp.OneOrMore(pp.Suppress(pp.SkipTo(ExtLibStart)) + ExtLib)) + InternLib + FinalCell
#   Reading the file if no results given
    if result == None:
        print("Parsing...")
        result = FullParser.parseFile(name)
#   Returning only the result from the file if asked by user
    if parsedbg:
        if not verbose:
            sys.stdout = sys.__stdout__
        print('---- Done (parsedbg)\n')
        return result
#   Building and returning the graphe
    else:
        BuildCell(result, gate_dict, internal_cells, ExtSeq)
        AddIO(result, g, prim_in, prim_out, nodes)
        AddNets(result, g, prim_in, prim_out, nodes, name_conv)
        AddGate(result, g, gate_dict, name_conv, internal_cells, ExtSeq)
        print('---- Done\n')
        
        g = clean_const.clean_const(g)
        g = clean_const.clean_not_buf(g)
        if not verbose:
            sys.stdout = sys.__stdout__
            print('---- Done\n')
    
        return g, prim_in, prim_out, nodes