# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Building the graph from the netlist
Project    : Graph-based nodes selection for functional locking

File       : build_def.py
Last update: 2015-03-17
"""

from __future__ import print_function
from qm import QuineMcCluskey
import igraph as ig
import pyparsing as pp
import clean_const
from cells_dict import xilinx_cells
import time
import sys
import os

def adv_add_edges(g, es, instances, **kwds):
    """adv_add_edges(es, **kwds)
    
    Adds multiple edges to the graph with a unique set of keywords.
    
    Keyword arguments (except the source and target arguments) will be
    assigned to added edges as attributes.
    @param g : the graph where to add given edges    
    @param es: list of source - dest tuples to add
    @param instances: list of instances names to add corresponding to edges
    @param **kwds : attributes to add to all the edges
    
    @return result of igraph.add_edges()
    """
    
    if len(es) != len(instances):
        raise Exception('Length of es different than length of instances')
    if not kwds:
        return g.add_edges(es)
#   Getting next edge ID
    eid = g.ecount()
#   Adding all the edges from es to the graphe
    result = g.add_edges(es)
#   Adding Keywords and instance name to all the edges
    for i, _ in enumerate(es):
        for key, value in kwds.iteritems():
            g.es[eid + i][key] = value
        g.es[eid + i]['instance'] = instances[i]
    return result
    
def AddIO(toks, g, prim_in, prim_out, nodes):
    """AddIO(toks)
    
    Add Inputs and Outputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signal which can be inputs of functions
    """
    print("AddIO") #Debug
#   Searching for global cell id in the internal lib
    for ID, cell in enumerate(toks[1]):
        if cell[0] == toks[2]:
            global_cell_ID = ID
            break
#   Reading all the IO bus of the global cell
    for IODecl in toks[1][global_cell_ID][1]:
#       Looping for the size of the bus
        for i in range(int(IODecl[1])):
#           Getting direction of the port and adding id in bus if multiple ports in the same bus
            Dir = IODecl[2]
            if int(IODecl[1]) == 1:
                InputOutput = IODecl[0]
            else:
                InputOutput = IODecl[0] + '_' + str(i)
#           Adding ports
            if Dir == "INPUT":
                if not InputOutput in prim_in:
                    g.add_vertex(InputOutput,
                                 label=InputOutput,
                                 color="#DDDDDD",
                                 cat="input",
                                 locks=[],
                                 forced=[],
                                 size=100,
                                 label_size=30)
                    prim_in.append(InputOutput)
                    nodes.append(InputOutput)
            
            elif Dir == "OUTPUT":
                if (not InputOutput in prim_in) and (not InputOutput in prim_out):
                    prim_out.append(InputOutput)
                    g.add_vertex(InputOutput,
                                 label=InputOutput,
                                 color="#666666",
                                 cat="output",
                                 locks=[0, 1],#By convention
                                 forced=[],
                                 size=100,
                                 label_size=30)

def GateFinder(Component_name):
    """GateFinder(Component_name)
    
    Dictionnary which allow to find which model associate with which cell name    
    If the cell isn't described, raise an Exception with the name of the gate which caused the error.
    
    @param Component_name: name of the edif cell to represent
    
    @return the gate type corresponding to the name given in Component_name.
            Can be:
            - The name of a standard cell (not, buf, and, nand, or, nor, xor, xnor)
            - A tuple containing first the library (internal or software) where the cell is described, and second the name of the cell in this library.
    """
#   removing useless informations before checking cell type
    comp_array = Component_name.split('_')
    number_del = ''
    while comp_array[0][-1] in '0123456789':
#       memorizing removed number for some uses
        number_del = comp_array[0][-1] + number_del
        comp_array[0] = comp_array[0][:-1]
#   returning datas depending on the cell name
    if comp_array[0].upper() == 'LUT':
        if Component_name[-1] in ['D', 'L']:
            result = ('lut', number_del, Component_name[-1])
        else:
            result = ('lut', number_del, 'G')
        sequential = False
    elif comp_array[0].upper() =='INV':
        result = 'not'
        sequential = False
    elif comp_array[0].upper() in ['IBUF', 'OBUF']:
        result = 'buf'
        sequential = False
    elif comp_array[0].upper() in ['FD', 'FDS', 'FDR', 'FDRS']:
        result = ('software', comp_array[0].upper())
        sequential = True
    elif comp_array[0].upper() in ['MUXF', 'MUXCY']:
        result = ('software', comp_array[0].upper())
        sequential = False
    elif comp_array[0].upper() == 'GND':
        result = ('const', '0')
        sequential = False
    elif comp_array[0].upper() == 'VCC':
        result = ('const', '1')
        sequential = False
#   Raising an exception if the cell is not known
    else:
        raise Exception('unknown gate ' + Component_name + '. Maybe it is not yet implemented...')
#   retunring datas 
    return sequential, result
    
def LUTBuilder(lutsize, initstring, out_type):
    """LUTBuilder(lutsize, initstring, out_type)
    
    Build the model to represent a LUT fundtion based on its size and intial value.
    @param lutsize: number of inputs for the LUT
    @param initstring: hexadecimal string representing the initial value of the LUT
    @param out_type: output type(D, L, or G). Used to know the name of the output to build the model
    
    @return a model of the LUT to build in a tuple containing first the name of internal signals and second a list of edges to add (source, target, function, instance)
    """
#   Initializing QuineMcCluskey lib
    qm = QuineMcCluskey()
#   Defining model contianers
    lut_vertices_list = []
    lut_edges_list = []
    nots_list = []
#   Searching for the ones in the initializing value and saving inputs state in 'ones'
    ones = []
    initval = int(initstring, 16)
    for row in range(2**lutsize):
        if initval & (1 << row): 
            ones.append(row)
#   building inputs lists 
    inputs = []
    for i in reversed(range(lutsize)):
        inputs.append('{I' + str(i) + '_0}')
#   defining outputs
    if out_type == 'D':        
        output = '{O_0}'
        lut_edges_list.append([('{O_0}', '{LO_0}'), 'buf', "DOut_buf"])
    elif out_type == 'L':        
        output = '{LO_0}'
    else:        
        output = '{O_0}'
#   Getting simplified truth table
    Def = qm.simplify(ones, num_bits=lutsize)
#   Building LUT model:
#   If more than one inputq
    if lutsize > 1:
        and_count = 0
        and_nodes = []
#       if there are multiple signal cover, each lines have to be combinated with an or
        if len(Def) > 1:
            global_or = True
        else:
            global_or = False
        
#       Reading all output cover
        for cover in Def:
#           if an or is needed, create intermediate nodes and redirect the output of the and gate
            if global_or: 
                and_count += 1
                target = "and{0}_{1}".format(output[1:-1], and_count)
            else:
                target = output
#           Initializing some vars
            edges_list = []
            used_signal = 0
            last_state_found = ''
#           reading inputs states for this cover
            for j, source in enumerate(cover):
#               Adding direct input if as '1' source
                if source == '1':
                    used_signal += 1
                    edges_list.append((inputs[j], target))
                    last_state_found = '1'
#               Adding complemented input if as '0' source
                elif source == '0':
                    used_signal += 1
                    if inputs[j][0] == '{':
                        not_source = 'interNot_{0}'.format(inputs[j][1:-1])
                    else:
                        not_source = 'interNot_{0}'.format(inputs[j])
#                   Creating complemented nodes for the input if doesn't exist yet
                    if not not_source in nots_list:
                        lut_vertices_list.append(not_source)
                        lut_edges_list.append([(inputs[j], not_source), 'not', not_source])
                        nots_list.append(not_source)
                    edges_list.append((not_source, target))
                    last_state_found = '0'
#       =°2=Checking the number of inputs used : more than 1 inputs used
            if(used_signal > 1):
#               adding the intermediate node if global or needed
                if global_or:
                    and_nodes.append(target)
                    lut_vertices_list.append(target)
#               Adding edges to the list
                for edge in edges_list:
                    lut_edges_list.append([edge, 'and', target])
#       =°2=Checking the number of inputs used : 1 input used
            elif used_signal == 1:
#               Adding the inputs directly for the global or
                if global_or:
                    and_nodes.append(edges_list[0][0])
#               If no global or, adding a BUF or a NOT depending on the input state (last_state_found)
                else:
                    if last_state_found == '1':
                        lut_edges_list.append([edges_list[0], 'buf', "buf{0}_{1}".format(output[1:-1], and_count)])
                    elif last_state_found == '0':
#                       if complemented version added just for this, redirecting the target node to the real output
                        if lut_vertices_list[-1] == edges_list[0][0]:
                            nots_list.pop()
                            lut_edges_list[-1][0] = (lut_edges_list[-1][0][0], target)
#                       if complemented version added before, adding a buf from the complemented version to the output
                        else:
                            lut_edges_list.append([edges_list[0], 'buf', "buf{0}_{1}".format(output[1:-1], and_count)])
#       adding the global or for the function if needed
        if global_or:
            edges_list = []
            for int_and in and_nodes:
                lut_edges_list.append([(int_and, output), 'or', "or{0}".format(output[1:-1])])
#°1=Check inputs number: 1 inputs
    elif lutsize == 1:
#       1st one (in ones list) == '1' => buf
        if ones[0] == 1 :
            lut_edges_list.append([(inputs[0], output), 'buf', "buf{0}".format(output[1:-1])])
#       Else => not
        else:
            lut_edges_list.append([(inputs[0], output), 'not', "buf{0}".format(output[1:-1])])

    return lut_vertices_list, lut_edges_list

def AddGate(toks, g, name_conv, ExtSeq=False):
    """AddGate(toks)
    
    Add edges to the graph
        
    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g : the graph where to add given edges
    @param name_conv: dictionnary of node name linked to ports (using global ports names)
    @keyword ExtSeq(False): indicate if the graph should externalise sequential standards cells by putting connected nodes as input or output.
    """
    print("AddGates")
    gate_dict = {}
#   building a dictionary containig all ports of cells
    for lib in toks[0]:
        for Gate in lib[1]:
            IOList = []
            for IOBus in Gate[1]:
                for bit in range(int(IOBus[1])):
                    IOList.append((IOBus[0] + '_' + str(bit), IOBus[2]))
            gate_dict[Gate[0]] = IOList
#   adding value forcing nodes
    g.add_vertex('Logic0',
                 label='Logic0',
                 color="#FFFFFF",
                 cat="const",
                 locks=[],
                 forced=[],
                 size=100,
                 label_size=30)
    g.add_vertex('Logic1',
                 label='Logic1',
                 color="#FF0000",
                 cat="const",
                 locks=[],
                 forced=[],
                 size=100,
                 label_size=30)
#   initializing var
    to_add_edges = {'and':[[],[]], 'nand':[[],[]], 'or':[[],[]], 'nor':[[],[]], 'xor':[[],[]], 'xnor':[[],[]], 'not':[[],[]], 'buf':[[],[]], '0':[[],[]], '1':[[],[]]}
#   searching for global cell id in the internal lib
    for ID, cell in enumerate(toks[1]):
        if cell[0] == toks[2]:
            global_cell_ID = ID
            break
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print(str(len(toks[1][global_cell_ID][2])) + '/' + nbdisp, end='')
#   reading instances of global cell
    for Instance in toks[1][global_cell_ID][2]:
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
            
        Inst_name = Instance[0]
        inputs = []
        output = ''
#       if cell doesn't have a model, separating inputs and output
        for IO in gate_dict[Instance[1]]:
            if not(Instance[1] == 'dff'): 
                global_IO_name = IO[0] + '@' + Inst_name
                if IO[1] == 'INPUT':
                    inputs.append(name_conv[global_IO_name])
                else:
                    if output == '':
                        output = name_conv[global_IO_name]
                    else:
                        print('Warning ! : more than one Output for Gate ' + Instance[1])
#       gathering data from GateFinder
        sequential, function = GateFinder(Instance[1])
#       if instanciated cell is a DFF and flag ExtSeq as true, putting inputs and outputs nodes as ports of the global ports
        if sequential and ExtSeq :
            for IO in gate_dict[Instance[1]]:
#               setting node dir depending on instantiated cell's port dir
                if name_conv.has_key(IO[0] + '@' + Inst_name):
                    if IO[1] == 'INPUT':
                        direction = 'output'
                    if IO[1] == 'OUTPUT':
                        direction = 'input'
#                   changing categeorie of linked nodes if internal node
                    if g.vs.find(name=name_conv[IO[0] + '@' + Inst_name])['cat'] == 'node':
                        g.vs.find(name=name_conv[IO[0] + '@' + Inst_name])['cat'] = direction
        else:
#           type 'const'
            if function[0] == 'const':
                const_val = function[1]
#               adding const edge from the right const source
                to_add_edges[const_val][0].append(('Logic' + const_val, output))
                to_add_edges[const_val][1].append(Inst_name)
#           type 'lut'
            elif function[0] == 'lut':
#               Getting model corresponding to the initializing value
                lut_vertices, lut_edges = LUTBuilder(int(function[1]), Instance[2], function[2])
#               Adding model's nodes
                for node in lut_vertices:
                    g.add_vertex(node + '_' + Inst_name,
                                 label=node + '_' + Inst_name,
                                 color="#FFFFFF",
                                 cat="node",
                                 locks=[],
                                 forced=[],
                                 size=100,
                                 label_size=30)
#               adding model's edges 
                for edge in lut_edges:
#                   replacing source name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][0][0] == '{':
                        edge_source = name_conv[edge[0][0][1:-1] + '@' + Inst_name]
                    else:
                        edge_source = edge[0][0] + '_' + Inst_name
#                   replacing target name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][1][0] == '{':
                        edge_target = name_conv[edge[0][1][1:-1] + '@' + Inst_name]
                    else:
                        edge_target = edge[0][1] + '_' + Inst_name
#                   adding edge to the model in construction
                    to_add_edges[edge[1]][0].append((edge_source, edge_target))
                    to_add_edges[edge[1]][1].append(edge[2] + '_' + Inst_name)
#           type 'software'
            elif function[0] == 'software':
#               getting model from the library
                cell_vertices = xilinx_cells[function[1]][0]
                cell_edges = xilinx_cells[function[1]][1]
#               Adding model's nodes
                for node in cell_vertices:
                    g.add_vertex(node + '_' + Inst_name,
                                 label=node + '_' + Inst_name,
                                 color="#FFFFFF",
                                 cat="node",
                                 locks=[],
                                 forced=[],
                                 size=100,
                                 label_size=30)
#               adding model's edges 
                for edge in cell_edges:
#                   replacing source name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][0][0] == '{':
                        edge_source = name_conv[edge[0][0][1:-1] + '@' + Inst_name]
                    else:
                        edge_source = edge[0][0] + '_' + Inst_name
#                   replacing target name between brackets '{}' by corresponding name from links' dictionary
                    if edge[0][1][0] == '{':
                        edge_target = name_conv[edge[0][1][1:-1] + '@' + Inst_name]
                    else:
                        edge_target = edge[0][1] + '_' + Inst_name
#                   adding edge to the model in construction
                    to_add_edges[edge[1]][0].append((edge_source, edge_target))
                    to_add_edges[edge[1]][1].append(edge[2] + '_' + Inst_name)
#           other types
            else:
#               adding edge from each input to output using type as function
                edges_list = [[],[]]
                for inp in inputs:
                    edges_list[0].append((inp, output))
                    edges_list[1].append(Inst_name)
                to_add_edges[function][0].extend(edges_list[0])
                to_add_edges[function][1].extend(edges_list[1])
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb))    
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add_edges, key=lambda i: len(to_add_edges[i][0]), reverse=False):
        if len(to_add_edges[i][0]) > 0:
            print('-> Adding ' + str(len(to_add_edges[i][0])) + ' ' + i + '...')
            adv_add_edges(g, to_add_edges[i][0], to_add_edges[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")
        
def AddNets(toks, g, prim_in, prim_out, nodes, name_conv):
    """AddNets(toks)
    Add nodes to the graph and build name_conv dictionary used to link instances of the global cell to the right nodes. 
    Add buffers to output and from inputs if needed
    
    @param toks: pyparsing result for the edif syntax
           toks[0] : external libraries list
           toks[1] : internal cells list
           toks[2] : described cell's name
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signal which can be inputs of functions
    @param name_conv: dictionnary to fill wth nodes names linked with ports (using globla names)
    """
    print("AddNets")
#   searching for global cell id in the internal lib
    for ID, cell in enumerate(toks[1]):
        if cell[0] == toks[2]:
            global_cell_ID = ID
            break    
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[1][global_cell_ID][3])) + '/' + nbdisp, end='')
#   Reading ports links of the global cell
    for Def in toks[1][global_cell_ID][3]:
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
#       Editing node name to make it match 
        if Def[0][-1] == '_':
            node_name = Def[0][:-1].replace('__', '00') + 'n'
        else:
            node_name = Def[0].replace('__', '00')
#       adding the signal to the model if not an input or an output
        if (not node_name in prim_in) and (not node_name in prim_out):
            g.add_vertex(node_name,
                         label=node_name,
                         color="#FFFFFF",
                         cat="node",
                         locks=[],
                         forced=[],
                         size=100,
                         label_size=30)
            nodes.append(node_name)
#       adding each port linked to the signal in the links' dictionary
        for signal in Def[1:]:
#           if instance reference of the port is 'none', the port is part of IOs of the cell
            if signal[2] != 'none':
                name_conv[signal[0] + '_' + signal[1] + '@' + signal[2]] = node_name
            else:
#               adding id of port in the bus if signal isn't in input or in output as it is
                if (not signal[0] in prim_in) and (not signal[0] in prim_out):
                    signal_name = signal[0] + '_' + signal[1]
                else:
                    signal_name = signal[0]
#               adding an edge between the port and the signal if the name is different
                if signal_name != node_name:
                    if signal_name in prim_in:
                        adv_add_edges(g, [(signal_name, node_name)], [signal_name + 'TO' + node_name],
                                      name='buf',
                                      label='buf',
                                      width=5,
                                      arrow_size=2,
                                      label_size=40,
                                      color="#AAAAAA")
                    elif signal_name in prim_out:
                        adv_add_edges(g, [(node_name, signal_name)], [node_name + 'TO' + signal_name],
                                      name='buf',
                                      label='buf',
                                      width=5,
                                      arrow_size=2,
                                      label_size=40,
                                      color="#AAAAAA")
#                   If instance ref is 'none' and port is not an input nor an output, it's incoherent. Raise an exception
                    else:
                        raise Exception("alone signal " + signal_name + ' in net ' + node_name + " not an input nor an output")
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb))
                    
                
def build(name, ExtSeq=False, result=None, parsedbg=False, verbose=True):
    """build(name, result=None, parsedbg=False)
    Builds the graph from an EDIF netlist
    @param name: name of the netlist to build
    @keyword ExtSeq: allow the user to externalise sequential standards cells by putting connected nodes as input or output. Routed to graph buildings functions.
    @keyword result: result set from the edif syntax reader. Avoid reading and use set given instead. Used mainly for debugging.
    @keyword parsebg: Boolean used to disable graph generating and return the syntax reader set of the netlist choosen. Used mainly for debugging.
    
    @return a tuple of 4 elements : the graph for the netlist, the list of inputs, the list of outputs, the list of node (inputs and internal nodes)
    """
    print("---- Build EDIF-Xilinx")
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    prim_in = []    #primary inputs of the netlist
    prim_out = []   #primary outputs of the netlist
    nodes = []      #nodes of the netlist
    name_conv = {}
    g = ig.Graph(directed=1)
#   Setting basics elements
    GateDefStart = pp.Suppress(pp.Literal("(cell"))
    ExtLibStart = pp.Suppress(pp.Literal("(external"))
    Node = pp.Word(pp.alphanums + '_&')
    Name = pp.Word(pp.alphanums + '_')
    PortRefStart = pp.Suppress(pp.Literal("(portRef"))
    InternCellStart = pp.Suppress(pp.Literal('(cell ') + pp.NotAny(pp.CaselessLiteral('logic')))
    InstanceStart = pp.Suppress(pp.Literal("(instance"))
    NetStart = pp.Suppress(pp.Literal("(net"))
#   Setting data elements
    GatePort = pp.Group(pp.Suppress(pp.Literal("(port")) + pp.Optional(pp.Suppress(pp.Literal('(array'))) + pp.Optional(pp.Suppress(pp.Literal('(rename'))) + Node + pp.Optional(pp.Suppress(pp.SkipTo(pp.Word(pp.nums) + pp.White(' '), failOn=pp.Literal("(direction"))) + pp.Word(pp.nums), default='1') + pp.Suppress(pp.SkipTo(pp.Literal("(direction"), include=True)) + pp.Or([pp.CaselessLiteral('INPUT'), pp.CaselessLiteral('OUTPUT')]) + pp.Suppress(')') + pp.Suppress(')'))
    PortRef = pp.Group(PortRefStart + pp.Optional(pp.Suppress(pp.Literal("(member"))) + Node + pp.Optional(pp.Word(pp.nums), default='0') + pp.Optional(pp.Suppress(')')) + pp.Optional(pp.Suppress(pp.Literal("(instanceRef")) + Node + pp.Suppress(')'), default='none') + pp.Optional(pp.Suppress(')')))
    GateDef = pp.Group(GateDefStart + Name + pp.Suppress(pp.SkipTo(pp.Literal("(interface"), include=True)) + pp.Group(pp.OneOrMore(GatePort)) + pp.Suppress(')') + pp.Suppress(')') + pp.Suppress(')'))
    CellDesc = InternCellStart + Name + pp.Suppress(pp.SkipTo(pp.Literal("(interface"), include=True)) + pp.Group(pp.OneOrMore(GatePort))
    Instance = pp.Group(InstanceStart + pp.Optional(pp.Suppress(pp.Literal("(rename"))) + Node + pp.Suppress(pp.SkipTo(pp.Literal("(cellRef"), include=True)) + Name + pp.Suppress(pp.Or([pp.Literal('(libraryRef') + Name + pp.Literal(')') + pp.Literal(')'), pp.Literal(')')])) + pp.Suppress(')') + pp.Optional(pp.Suppress(pp.Literal("(property XSTLIB (boolean (true)) (owner \"Xilinx\"))")))+ pp.Optional(pp.Suppress(pp.Literal("(property PK_HLUTNM (string \"") + Node + pp.Literal("\") (owner \"Xilinx\"))"))) + pp.Optional(pp.Suppress(pp.Literal("(property INIT (string \"")) + Name + pp.Suppress(pp.Literal("\") (owner \"Xilinx\"))"))) + pp.Suppress(')'))
    Net = pp.Group(NetStart + pp.Optional(pp.Suppress(pp.Literal("(rename"))) + Node + pp.Suppress(pp.SkipTo(PortRefStart)) + pp.OneOrMore(PortRef) + pp.Optional(pp.Suppress(')')) + pp.Optional(pp.Suppress(')')))
#   Grouping data elements
    InternCell = pp.Group(pp.Suppress(pp.SkipTo(InternCellStart)) + CellDesc + pp.Group(pp.Suppress(pp.SkipTo(InstanceStart)) + pp.OneOrMore(Instance)) + pp.Group(pp.Suppress(pp.SkipTo(NetStart)) + pp.Dict(pp.OneOrMore(Net))))
    ExtLib = pp.Group(ExtLibStart + Name + pp.Group(pp.Optional(pp.Suppress(pp.SkipTo(GateDefStart, failOn=pp.Literal('(external'))) + pp.OneOrMore(GateDef))))
    InternLib = pp.Group(pp.OneOrMore(InternCell))
    FinalCell = pp.Suppress(pp.SkipTo(pp.Literal("(cellRef"), include=True)) + Name
#   Setting full parser
    FullParser = pp.Group(pp.OneOrMore(pp.Suppress(pp.SkipTo(ExtLibStart)) + ExtLib)) + InternLib + FinalCell
#   Reading the file if no results given
    if result == None:
        print("Parsing...")
        result = FullParser.parseFile(name)
#   Returning only the result from the file if asked by user
    if parsedbg:
        if not verbose:
            sys.stdout = sys.__stdout__
        print('---- Done (parsedbg)\n')
        return result
#   Building and returning the graphe
    else:
        AddIO(result, g, prim_in, prim_out, nodes)
        AddNets(result, g, prim_in, prim_out, nodes, name_conv)
        AddGate(result, g, name_conv, ExtSeq)
        print('---- Done\n')
        
        g = clean_const.clean_const(g)
        g = clean_const.clean_not_buf(g)
            
        if not verbose:
            sys.stdout = sys.__stdout__
            print('---- Done\n')
    
        return g, prim_in, prim_out, nodes