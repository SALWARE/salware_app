# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Building the graph from the netlist
Project    : Graph-based nodes selection for functional locking

File       : build_def.py
Last update: 2015-03-17
"""
from __future__ import print_function
import igraph as ig
import pyparsing as pp
import time
import sys
import os

def adv_add_edges(g, es, instances, **kwds):
    """adv_add_edges(es, **kwds)
    
    Adds multiple edges to the graph with a unique set of keywords.
    
    Keyword arguments (except the source and target arguments) will be
    assigned to added edges as attributes.
    @param g : the graph where to add given edges    
    @param es: list of source - dest tuples to add
    @param instances: list of instances names to add corresponding to edges
    @param **kwds : attributes to add to all the edges
    
    @return result of igraph.add_edges()
    """
    
    if len(es) != len(instances):
        raise Exception('Length of es different than length of instances')
    if not kwds:
        return g.add_edges(es)
#   Getting next edge ID
    eid = g.ecount()
#   Adding all the edges from es to the graphe
    result = g.add_edges(es)
#   Adding Keywords and instance name to all the edges
    for i, _ in enumerate(es):
        for key, value in kwds.iteritems():
            g.es[eid + i][key] = value
        g.es[eid + i]['instance'] = instances[i]
    return result

def priorityFind(inputs_str):
    """priorityFind(inputs_str)
    
    Convert an inputs combination string with priority between parenthesis
    
    @param inputs_str: inputs combination string
    
    @return an array of string corresponding to priority blocks sorted by level (0 is the lowest level)
    """
#   Initializing some vars
    prio_level = 0     #Save the current priority level
    temp_storage = ['']#Save the blocs in progress of lower priority levels
    bloc_count = [0]   #Counters of blocs of each level to generate reference between levels
    result = ['']      #The result which will be returned
    skip = False       #Flag allowing to skip a character
#   reading all characters one by one from the logical equation
    for idl, letter in enumerate(inputs_str):
#       Allow to skip a character during the processing
        if not skip:
#           Beginning of a new bloc of higher level
            if letter == '(':
#               Adding a new temporary bloc
                temp_storage.append('')
#               Adding a counter to bloc_count if it doesn't exist yet
                if len(bloc_count) == prio_level + 1:
                    bloc_count.append(0)
                    result.append([])
#               Increasing the value else
                else:
                    bloc_count[prio_level + 1] += 1
#               Adding the reference in the bloc in progress and increasing priority level
                temp_storage[prio_level] += '{' + str(bloc_count[prio_level + 1]) + '}'
                prio_level += 1
#           Ending of a priority bloc
            elif letter == ')':
#               Checking if the bloc is complemented
                if inputs_str[idl + 1] == '\'':
#                   Adding the complementation mark and skipping the (') character
                    temp_storage[prio_level] += ' #'
                    skip = True
#               Transfering prcessed bloc to the result and decreasing priority level
                result[prio_level].append(temp_storage.pop())
                prio_level -= 1
#           In any other case, copying letter to the processing bloc
            else:
                temp_storage[prio_level] += letter
#       if the skipping was set, removing it
        else:
            skip = False
#   Copying the lower level bloc in the results
    result[0] = [temp_storage[0]]
#   Splitting blocs strings to make processing easier after
    inputs_array = []
    for i, level in enumerate(result):
        inputs_array.append([])
        for j, block in enumerate(level):
            inputs_array[i].append([])
            for or_branch in block.split('+'):
                inputs_array[i][j].append(filter(None, or_branch.split(' ')))
    
    return inputs_array

def AddInputs(toks, g, prim_in, nodes):
    """AddInputs(toks)
    
    Add Inputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Nodes' link list
           toks[3] : logical functions list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list to fill with inputs
    @param nodes: list to fill with signal which can be inputs of functions
    """
    print("AddInputs") #Debug
    
    for Input in toks[0]:#toks contain the full result of the parsing
#        print Dir.upper() + " " + InputOutput #debug
        if not Input in prim_in:
            prim_in.append(Input)
            nodes.append(Input)
            g.add_vertex(Input,
                         label=Input,
                         color="#DDDDDD",
                         cat="input",
                         locks=[],
                         forced=[],
                         size=100,
                         label_size=30)
#    print nodes

def AddOutputs(toks, g, prim_in, prim_out):
    """AddOutputs(toks)
    
    Add Outputs to the graph

    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Nodes' link list
           toks[3] : logical functions list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list to fill with outputs
    """
    print("AddOutputs")
    
    for Output in toks[1]:#toks contain the full result of the parsing
        if (not Output in prim_in) and (not Output in prim_out):
            prim_out.append(Output)
            g.add_vertex(Output,
                         label=Output,
                         color="#666666",
                         cat="output",
                         locks=[0, 1],#By convention
                         forced=[],
                         size=100,
                         label_size=30)
#    print prim_out
#            else:
#                g.vs.find(name=InputOutput).delete()
#                nodes.remove(InputOutput)
#                prim_in.remove(InputOutput)

def AddGate(toks, g, prim_in, prim_out, nodes, name_conv):
    """AddGate(toks)
    
    Add edges to the graph
        
    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Nodes' link list
           toks[3] : logical functions list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to fill with signal which can be inputs of functions
    @param name_conv: dictionnary containing nodes name to be replace with an other name
    """
    print("AddGates") #Debug
#   Initializing some vars
    nots_list = []
    to_add_edges = {'and':[[],[]], 'nand':[[],[]], 'or':[[],[]], 'nor':[[],[]], 'not':[[],[]]}    
    add_vertices = []   #list of vertices to add
    instance_counter = 0#counter to generate instances names
    
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[3])) + '/' + nbdisp, end='')
#   Reading all the logcal functions
    for Def in toks[3]: 
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')

#       Converting signal name if needed (from AddNets)
        if name_conv.has_key(Def[0]):
            signal = name_conv[Def[0]]
        else:
            signal = Def[0]
#       Chech the output of the logical function and memorize complementation. Update output name if needed
        if signal[-1] == '\'':
            all_complemented = True   
            output = signal[:-1]
        else:
            all_complemented = False
            output = signal
#       Add output to the list if not present
        if (not output in nodes) and (not output in prim_out) and (not name_conv.has_key(output)):
            add_vertices.append(output)
            nodes.append(output)
#       Getting inputs array to process
        inputs_array = priorityFind(Def[1].replace('\n', ''))
#       Reading all the priority level from the array (from the higher to the lower)
        for i, level in reversed(list(enumerate(inputs_array))):
#           Reading all the blocks from the priority level
            for j, block in enumerate(level):
#               Checking if a global or is needed
                if len(block) > 1:
                    global_or = True
                else:
                    global_or = False
#               Generating an internal name for blocs except for the last one to built
                if (i > 1) or ((i == 1) and (len(inputs_array[0]) > 1)):
                    block_out = "Or{0}{1}_{2}".format(i, j, output)
                    add_vertices.append(block_out)
                else:
                    block_out = output
#               initializing an array which will contain and output nodes to link
                block_ands = []
#               setting the complementation state depending on the block (last block depend of the output)
                if block_out == output:
                    complemented = all_complemented
                else:
                    complemented = False
#               Reading and gates in the block
                for k, and_ins in enumerate(block):
#               =°1=If more than 1 inputs
                    if len(and_ins) > 1:
#                       If a global or is needed, generating the internal name for the and output
                        if global_or:
                            inner_name = "And{0}{1}{2}_{3}".format(i, j, k, output)
                            add_vertices.append(inner_name)
                            block_ands.append(inner_name)
#                       Else, using block output as and output
                        else:
                            inner_name = block_out
#                       Initializing an array which will contain edges (source, target)
                        edges_list = []
#                       Reading all inputs for the and function
                        for Orig_signal in and_ins:
#                           Replacing input name if needed
                            if (name_conv.has_key(Orig_signal)):
                                signal = name_conv[Orig_signal]
                            else:
                                signal = Orig_signal
#                           If the input is '#', changing the complementation state of the block
                            if signal == '#':
                                if complemented:
                                    complemented = False
                                else:
                                    complemented = True
#                           If the signal start with '{', link to a higher level block
                            elif signal[0] == '{':
                                edges_list.append(("Or{0}{1}_{2}".format(i+1, signal[1:-1], output),inner_name))
#                           If the signal end with ('), using a complemented version of the input
                            elif signal[-1] == '\'':
                                not_source = 'interNot_{0}'.format(signal[:-1])
#                               If complemented verion doesn't exist, create it
                                if not not_source in nots_list:
                                    add_vertices.append(not_source)
                                    to_add_edges['not'][0].append((signal[:-1], not_source))
                                    to_add_edges['not'][1].append('U' + str(instance_counter))
                                    instance_counter += 1
                                    nots_list.append(not_source)
#                               Adding the edge to edges_list
                                edges_list.append((not_source, inner_name))
#                           in any other case, adding directly the edge to edges_list
                            else:
                                edges_list.append((signal, inner_name))
                                
#                       Using a NAND only if ther is no global or and the bloc is complemented
                        if (not global_or) and complemented:
                            to_add_edges['nand'][0].extend(edges_list)
                            to_add_edges['nand'][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                            instance_counter += 1
#                       Else, using an AND
                        else:
                            to_add_edges['and'][0].extend(edges_list)
                            to_add_edges['and'][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                            instance_counter += 1
#               =°1=If only 1 input, no need to use an AND or a NAND
                    else:
#                       Converting signal name if needed
                        if name_conv.has_key(and_ins[0]):
                            signal = name_conv[and_ins[0]]
                        else:
                            signal = and_ins[0]
#                       If signal name end with('), using complemented input
                        if signal[-1] == '\'':
                            not_source = 'interNot_{0}'.format(signal[:-1])
#                           Adding complemented input if not done yet
                            if not not_source in nots_list:
                                add_vertices.append(not_source)
                                to_add_edges['not'][0].append((signal[:-1], not_source))
                                to_add_edges['not'][1].append('U' + str(instance_counter))
                                instance_counter += 1
                                nots_list.append(not_source)
#                           Adding signal to ands nodes to link with a global or
                            block_ands.append(not_source)
#                       If signal start with '{', adding higher level block output to the list of nodes to link with an or
                        elif signal[0] == '{':
                            block_ands.append("Or{0}{1}_{2}".format(i+1, signal[1:-1], output))
#                       In other case, adding the signal to the list of nodes to link with an or
                        else:
                            block_ands.append(signal)
#               If a global or is needed
                if global_or:
#                   building all the edge beteen block_ands and block_out
                    edges_list = []
                    for and_signal in block_ands:
                        edges_list.append((and_signal, block_out))
#                   Adding edges to to_add_edges with the right function depending on complmented
                    if complemented:
                        to_add_edges['nor'][0].extend(edges_list)
                        to_add_edges['nor'][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                        instance_counter += 1
                    else:
                        to_add_edges['or'][0].extend(edges_list)
                        to_add_edges['or'][1].extend(['U' + str(instance_counter) for x in range(len(edges_list))])
                        instance_counter += 1
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb)) 
#   Adding all vertices to the graphn
    for vertex in add_vertices:
        g.add_vertex(vertex,
                     label=vertex,
                     color="#FFFFFF",
                     cat="node",
                     locks=[],
                     forced=[],
                     size=100,
                     label_size=30)
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add_edges, key=lambda i: len(to_add_edges[i][0]), reverse=False):
        print('-> Adding ' + i + '...')
        adv_add_edges(g, to_add_edges[i][0], to_add_edges[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")

def AddNets(toks, g, prim_in, prim_out, nodes, name_conv):
    """AddNets(toks)
    Add nodes to the graph and build name_conv dictionary used to link instances of the global cell to the right nodes. 
    Add buffers to output and from inputs if needed
    
    @param toks: pyparsing result for the edif syntax
           toks[0] : Inputs list
           toks[1] : Outputs list
           toks[2] : Nodes' link list
           toks[3] : logical functions list
    @param g: igraph instance to complete with inputs and outputs
    @param prim_in: list containing inputs names
    @param prim_out: list containing outputs names
    @param nodes: list to complete with signals which can be inputs of functions
    @param name_conv: dictionnary to fill with nodes name replacement
    """
    print("AddNets")
#   Initializing some vars
    to_add_edges = {'buf':[[],[]], 'not':[[],[]], '0':[[],[]], '1':[[],[]]}
    instance_counter = 0
#   Initializing progress display
    nb = 0
    disp_time = time.time()
    nbdisp = '0'
    print('-> ' + str(len(toks[2])) + '/' + nbdisp, end='')
#   Reading all node links
    for i, Def in enumerate(toks[2]):
#       Updating progress display every 0.5s
        nb += 1
        if time.time() >= disp_time + 0.5:
            disp_time += 0.5
            for _ in nbdisp:
                print('\b', end='')
            nbdisp = str(nb)
            print(nbdisp, end='')
#       Checking if there is definition 
        if not Def == 'none':
#           Generating common node name and initializing cste
            group_name = "Net{0}".format(i)
            cste = False
#           Checking if the common node is a constant node and setting cste flag if it's the case
            for signal in Def:
                if (signal == '0') or (signal =='1'):
                    cste = True
                    break
#           Adding the common node to the graphe if not a constant node
            if not cste:
                g.add_vertex(group_name,
                             label=group_name,
                             color="#FFFFFF",
                             cat="node",
                             locks=[],
                             forced=[],
                             size=100,
                             label_size=30)
                nodes.append(group_name)
#           Reading all the signals linked. Ignoring constant nodes.
            if not cste:
                for signal in Def:
#                   If the signal is complemented
                    if signal[-1] == '\'':
#                       If the signal is an output, adding a note edge from the common node to the output
                        if signal[:-1] in prim_out:
                            to_add_edges['not'][0].append((group_name, signal[:-1]))
                            to_add_edges['not'][1].append('N' + str(instance_counter))
                            instance_counter += 1
#                           Adding a conversion name for the signal
                            name_conv[signal[:-1]] = group_name + '\''
#                       All others node are linked to the common node usina an edge with the common node as a target
                        else:
                            to_add_edges['not'][0].append((signal[:-1], group_name))
                            to_add_edges['not'][1].append('N' + str(instance_counter))
                            instance_counter += 1
#                   If the signal is not complemented
                    else:
#                       If the signal is an input, adding a buf from the input to the common node
                        if signal in prim_in:
                            to_add_edges['buf'][0].append((signal, group_name))
                            to_add_edges['buf'][1].append('N' + str(instance_counter))
                            instance_counter += 1
#                       if the signal is an output, adding a buf from the common node to the output
                        elif signal in prim_out:
                            to_add_edges['buf'][0].append((group_name, signal))
                            to_add_edges['buf'][1].append('N' + str(instance_counter))
                            instance_counter += 1
#                   Adding the conversion name of the signal
                    name_conv[signal] = group_name
#   displaying the last progress number
    for _ in nbdisp:
        print('\b', end='')
    print(str(nb))
#   Adding all edges to the graph ordered by function
    for i in sorted(to_add_edges, key=lambda i: len(to_add_edges[i][0]), reverse=False):
        if len(to_add_edges[i][0]) > 0:
            print('-> Adding ' + str(len(to_add_edges[i][0])) + ' ' + i + '...')
            adv_add_edges(g, to_add_edges[i][0], to_add_edges[i][1],
                      name=i,
                      label=i,
                      width=5,
                      arrow_size=2,
                      label_size=40,
                      color="#AAAAAA")

def build(name, result=None, parsedbg=False, verbose=True):
    """build(name, result=None, parsedbg=False)
    Builds the graph from an EDIF netlist
    @param name: name of the netlist to build
    @keyword result(None): result set from the edif syntax reader. Avoid reading and use set given instead. Used mainly for debugging.
    @keyword parsebg(False): Boolean used to disable graph generating and return the syntax reader set of the netlist choosen. Used mainly for debugging.
    
    @return a tuple of 4 elements : the graph for the netlist, the list of inputs, the list of outputs, the list of node (inputs and internal nodes)
    """
    print("---- Build SLIF")
    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    g = ig.Graph(directed=1)
    prim_in = []    #primary inputs of the netlist
    prim_out = []   #primary outputs of the netlist
    nodes = []      #nodes of the netlist
    name_conv = {}
#   Setting basic elements
    InputWord = pp.Suppress('.' + pp.CaselessLiteral("INPUTS"))
    OutputWord = pp.Suppress('.' + pp.CaselessLiteral("OUTPUTS"))
    BufWord = pp.Suppress('.' + pp.CaselessLiteral("NET"))
    Node = pp.Word(pp.alphanums + '<>$:^%[]_/~-\'', bodyChars = pp.alphanums + '<>$:^%[]_./~-\'')
#   Setting data elements 
    Inputs = pp.OneOrMore(InputWord + pp.OneOrMore(Node) + pp.Suppress(';'))
    Outputs = pp.OneOrMore(OutputWord + pp.OneOrMore(Node) + pp.Suppress(';'))
    Bufs = pp.OneOrMore(pp.Group(BufWord + pp.OneOrMore(Node) + pp.Suppress(';')))
    Declare = pp.OneOrMore(pp.Group(Node + pp.Suppress('=') + pp.SkipTo(';') + pp.Suppress(';')))
#   Setting full parser
    FullParser = pp.Suppress(pp.SkipTo(InputWord)) + pp.Group(Inputs) + pp.Group(Outputs) + pp.Group(pp.Optional(Bufs, default="none")) + pp.Group(Declare)
#   Reading the file if no results given    
    if result == None:
        print("Parsing...")
        result = FullParser.parseFile(name)
#   Returning only the result from the file if asked by user
    if parsedbg:
        if not verbose:
            sys.stdout = sys.__stdout__
        print('---- Done (parsedbg)\n')
        return result
#   Building and returning the graphe
    else:
        AddInputs(result, g, prim_in, nodes)
        AddOutputs(result, g, prim_in, prim_out)
        AddNets(result, g, prim_in, prim_out, nodes, name_conv)
        AddGate(result, g, prim_in, prim_out, nodes, name_conv)
        if not verbose:
            sys.stdout = sys.__stdout__
        print("---- Done\n")

        return g, prim_in, prim_out, nodes
