# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: status_bar.py
# Date: 2016-10-24

from Tkinter import *
from ttk import *


def status_bar(self):

    """Insert a status bar"""

    self.status_bar_frame = Frame(self.master,
                                  borderwidth=1,
                                  relief="ridge")
    self.status_bar_frame.pack(side="bottom",
                               fill="x")
    self.board_status_label = Label(self.status_bar_frame,
                              textvariable=self.board_status,
                              foreground="red",
                              relief="ridge",
                              padding=[5, 2, 5, 2])
    self.board_status_label.pack(side="right")
    self.status_label = Label(self.status_bar_frame,
                              textvariable=self.status,
                              relief="ridge",
                              padding=[5, 2, 5, 2])
    self.status_label.pack(side="right",
                           expand="yes",
                           fill="x")
