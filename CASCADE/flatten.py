# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: flatten.py
# Date : 2016-10-12

import itertools as it


def flatten(liste):

    """Turns a list of lists into a list.

    >>> flatten([[1, 2], [3, 4]])
    [1, 2, 3, 4]
    """

    liste = list(it.chain.from_iterable(liste))
    return liste
