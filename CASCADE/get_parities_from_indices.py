# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: get_parities_from_indices.py
# Date : 2016-10-12

import Tkinter

import os

import sys
sys.path.append(os.path.abspath("./../"))

from boards_management import board_commands

def get_parities_from_indices(indices, tclsh, board_manager, response_length):
    
    """Get the parities of the on-board response blocks.

    Hardware-specific implementation.
    >>> get_parities_from_indices([[1, 5], [7, 2], [3, 6], [4, 0]])
    [0, 1, 1, 0]
    """

    command = board_manager.command_get_parities_from_indices(indices, response_length)
    parities = tclsh.eval(command)
    parities =  [int(i) for i in bin(int(parities, 16))[2:].zfill(len(indices))]
    return parities
    

if __name__ == "__main__":
    tcl_obj = Tkinter.Tcl()
    board_manager = board_commands.Board_manager()
    tcl_obj.eval(board_manager.source_tcl_package())
    tcl_obj.eval(board_manager.connect("COM5"))
    tcl_obj.eval(board_manager.reset_boards())
    tcl_obj.eval(board_manager.generate_response())
    indices = [range(i, i+4) for i in range(0, 64, 4)]
    # indices = [[0, 1, 2, 3], [4, 5, 6, 7]]
    print indices
    print get_parities_from_indices(indices, tcl_obj, board_manager)
