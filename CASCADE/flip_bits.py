# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: flip_bits.py
# Date : 2016-10-12


def flip_bits(message, indices_to_flip, indices):
    
    """Flip the message bits at specific indices.

    >>> flip_bits([1, 1, 1, 1], [1])
    [1, 0, 1, 1]
    >>> flip_bits([0, 1, 0, 1], [2], [2, 0, 3, 1])
    [1, 1, 0, 1]
    
    """

    if indices:
        for i in indices_to_flip:
            message[indices.index(i)] ^= 1
    else:
        for i in indices_to_flip:
            message[i] ^= 1
    
            
if __name__ == "__main__":
    message = [0, 1, 0, 1]
    print(message)
    flip_bits(message, [2])
    print(message)
