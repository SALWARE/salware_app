import random as rd
from operator import itemgetter
import math

def write_header(vhd_file, locking, width_formatted_AW, len_AW):
    vhd_file.write("LIBRARY ieee;\n")
    vhd_file.write("USE ieee.std_logic_1164.ALL;\n\n")
    # Entity
    vhd_file.write("ENTITY AW_decoder IS\n\n")
    vhd_file.write("  PORT (\n")
    vhd_file.write("    formatted_AW : IN  STD_LOGIC_VECTOR(0 TO "+str(width_formatted_AW-1)+");\n")
    vhd_file.write("    AW           : OUT STD_LOGIC_VECTOR(0 TO "+str(len_AW-1)+"));\n\n")
    vhd_file.write("END ENTITY AW_decoder;\n\n")
    # Architecture
    vhd_file.write("ARCHITECTURE rtl OF AW_decoder IS\n\n")
    if locking:
        vhd_file.write("  SIGNAL locking_0 : STD_LOGIC;\n")
        vhd_file.write("  SIGNAL locking_1 : STD_LOGIC;\n\n")
    vhd_file.write("BEGIN\n\n")


def match_mask(pos_0s_formatted_masking_word, pos_1s_formatted_masking_word, pos_0s_masking_word, pos_1s_masking_word):
    match_0s_mask = {}
    match_1s_mask = {}
    if len(pos_0s_formatted_masking_word) > len(pos_0s_masking_word):
        # There are more 0s available than needed, they must be ORed
        rd.shuffle(pos_0s_formatted_masking_word)
        ratio = math.ceil(float(len(pos_0s_formatted_masking_word))/len(pos_0s_masking_word))
        # How many times more ? 
        pos_0s_masking_word = int(ratio)*pos_0s_masking_word
        # Round up the ratio and duplicate the list accordingly
        assocs = [[a, b] for a, b in zip(pos_0s_formatted_masking_word, pos_0s_masking_word)]
        # Zip to make associations
        for i in set(pos_0s_masking_word):
            match_0s_mask[i] = [pos_for for [pos_for, pos] in assocs if pos == i]
            # Group associations to the same position in the formatted word
    elif len(pos_0s_formatted_masking_word) < len(pos_0s_masking_word):
        # There are not enough 0s available, they must be shared
        rd.shuffle(pos_0s_masking_word)
        ratio = math.ceil(float(len(pos_0s_masking_word))/len(pos_0s_formatted_masking_word))
        pos_0s_formatted_masking_word = int(ratio)*pos_0s_formatted_masking_word
        assocs = [[a, b] for a, b in zip(pos_0s_formatted_masking_word, pos_0s_masking_word)]
        for i in set(pos_0s_masking_word):
            match_0s_mask[i] = [pos_for for [pos_for, pos] in assocs if pos == i]
    elif len(pos_0s_formatted_masking_word) == len(pos_0s_masking_word):
        # Perfect match
        for a, b in zip(pos_0s_formatted_masking_word, pos_0s_masking_word):
            match_0s_mask[b] = [a]
    if len(pos_1s_formatted_masking_word) > len(pos_1s_masking_word):
        # There are more 1s available than needed, they must be ORed
        rd.shuffle(pos_1s_formatted_masking_word)
        ratio = math.ceil(float(len(pos_1s_formatted_masking_word))/len(pos_1s_masking_word))
        pos_1s_masking_word = int(ratio)*pos_1s_masking_word
        assocs = [[a, b] for a, b in zip(pos_1s_formatted_masking_word, pos_1s_masking_word)]
        for i in set(pos_1s_masking_word):
            match_1s_mask[i] = [pos_for for [pos_for, pos] in assocs if pos == i]
    elif len(pos_1s_formatted_masking_word) < len(pos_1s_masking_word):
        # There are not enough 1s available, they must be shared
        rd.shuffle(pos_1s_masking_word)
        ratio = math.ceil(float(len(pos_1s_masking_word))/len(pos_1s_formatted_masking_word))
        pos_1s_formatted_masking_word = int(ratio)*pos_1s_formatted_masking_word
        assocs = [[a, b] for a, b in zip(pos_1s_formatted_masking_word, pos_1s_masking_word)]
        for i in set(pos_1s_masking_word):
            match_1s_mask[i] = [pos_for for [pos_for, pos] in assocs if pos == i]
    elif len(pos_1s_formatted_masking_word) == len(pos_1s_masking_word):
        # Perfect match
        for a, b in zip(pos_1s_formatted_masking_word, pos_1s_masking_word):
            match_1s_mask[b] = [a]
    # Os must be ORed
    # 1s must be ANDed
    for i in match_0s_mask:
        print i
    return match_0s_mask, match_1s_mask

def write_masking_part_to_file(vhd_file,
                               match_0s_mask,
                               match_1s_mask):
    print "Masking part"
    vhd_file.write("\n  -- Mask ORs\n")
    for AW_index in match_0s_mask:
        if len(match_0s_mask[AW_index]) > 1:
            # Must be ORed
            vhd_file.write("  AW("+str(AW_index)+") <= "+" OR ".join(["formatted_AW("+str(i)+")" for i in match_0s_mask[AW_index]])+";\n")
        else:
            vhd_file.write("  AW("+str(AW_index)+") <= formatted_AW("+str(match_0s_mask[AW_index][0])+");\n")
    vhd_file.write("\n  -- Mask ANDs\n")
    for AW_index in match_1s_mask:
        if len(match_1s_mask[AW_index]) > 1:
            # Must be ORed
            vhd_file.write("  AW("+str(AW_index)+") <= "+" AND ".join(["formatted_AW("+str(i)+")" for i in match_1s_mask[AW_index]])+";\n")
        else:
            vhd_file.write("  AW("+str(AW_index)+") <= formatted_AW("+str(match_1s_mask[AW_index][0])+");\n")

def write_locking_part_to_file(vhd_file,
                               pos_0s_formatted_locking_word, pos_0s_locking_word,
                               pos_1s_formatted_locking_word, pos_1s_locking_word):
    vhd_file.write("\n  -- Lock AND\n")
    # locking_1 = 1 if all pos_1s_formatted_locking_word = 1
    vhd_file.write("  locking_1 <= "+" AND ".join(["formatted_AW("+str(i)+")" for i in pos_1s_formatted_locking_word])+";\n")
    for AW_index in pos_1s_locking_word:
        # Distribute locking_1 to all AW outputs
        vhd_file.write("  AW("+str(AW_index)+") <= locking_1;\n")
    vhd_file.write("\n  -- Lock OR\n")
    # locking_0 = 0 if all pos_0s_formatted_locking_word = 0
    vhd_file.write("  locking_0 <= "+" OR ".join(["formatted_AW("+str(i)+")" for i in pos_0s_formatted_locking_word])+";\n")
    for AW_index in pos_0s_locking_word:
        # Distribute locking_0 to all AW outputs
        vhd_file.write("  AW("+str(AW_index)+") <= locking_0;\n")

def gen_AW_decoder (AW, width_formatted_AW, locking=True, masking=True, path = "./", formatted_AW=""):
    """Generates a decoder for the Activation Word
    
    AW: activation word, provided MSB first
    width_formatted_AW: width of the formatted AW
    """

    path = "./user_space/"
    # path = "./" 
    locking_word = ""
    masking_word = ""
    if locking:
        if masking:
            # Read locking word and masking word from AW and invert them to get activation words
            locking_word = ''.join('1' if x == '0' else '0' for x in AW.strip().split(" ")[0])
            masking_word = ''.join('1' if x == '0' else '0' for x in AW.strip().split(" ")[1])
        else:
            locking_word = ''.join('1' if x == '0' else '0' for x in AW.strip())
    else:
        if masking:
            masking_word = ''.join('1' if x == '0' else '0' for x in AW.strip())

    AW = locking_word+masking_word
    if locking and masking:
        pos_0s_formatted_locking_word = []
        pos_0s_formatted_masking_word = []
        pos_1s_formatted_locking_word = []
        pos_1s_formatted_masking_word = []
        
        pos_0s_locking_word = [i[0] for i in enumerate(locking_word) if i[1] == "0"]
        pos_1s_locking_word = [i[0] for i in enumerate(locking_word) if i[1] == "1"]
        pos_0s_masking_word = [i[0] for i in enumerate(masking_word, start = len(locking_word)) if i[1] == "0"]
        pos_1s_masking_word = [i[0] for i in enumerate(masking_word, start = len(locking_word)) if i[1] == "1"]
        while not(pos_0s_formatted_locking_word and
                  pos_0s_formatted_masking_word and
                  pos_1s_formatted_locking_word and
                  pos_1s_formatted_masking_word):
            # Generate random formatted AW with 0s and 1s in both halfs
            formatted_AW = ''.join([rd.choice(["0", "1"]) for n in xrange(width_formatted_AW)])
            formatted_locking_word = formatted_AW[:len(formatted_AW)/2]
            formatted_masking_word = formatted_AW[len(formatted_AW)/2:]
            pos_0s_formatted_locking_word = [i[0] for i in enumerate(formatted_locking_word) if i[1] == "0"]
            pos_1s_formatted_locking_word = [i[0] for i in enumerate(formatted_locking_word) if i[1] == "1"]
            pos_0s_formatted_masking_word = [i[0] for i in enumerate(formatted_masking_word, start = len(formatted_locking_word)) if i[1] == "0"]
            pos_1s_formatted_masking_word = [i[0] for i in enumerate(formatted_masking_word, start = len(formatted_locking_word)) if i[1] == "1"]
        print "Formatted AW", formatted_AW
        print AW
        print "pos_0s_locking_word", pos_0s_locking_word
        print "pos_0s_formatted_locking_word", pos_0s_formatted_locking_word
        print "pos_1s_locking_word", pos_1s_locking_word
        print "pos_1s_formatted_locking_word", pos_1s_formatted_locking_word
        print "pos_0s_formatted_masking_word", pos_0s_formatted_masking_word
        print "pos_0s_masking_word", pos_0s_masking_word
        print "pos_1s_formatted_masking_word", pos_1s_formatted_masking_word
        print "pos_1s_masking_word", pos_1s_masking_word
        match_0s_mask, match_1s_mask = match_mask(pos_0s_formatted_masking_word, pos_1s_formatted_masking_word, pos_0s_masking_word, pos_1s_masking_word)
        print match_0s_mask, match_1s_mask
        with open(path+"AW_decoder.vhd", "w") as vhd_file:
            write_header(vhd_file, locking, width_formatted_AW, len(AW))
            vhd_file.write("  -- Formatted AW: "+formatted_AW+"\n")
            vhd_file.write("  --           AW: "+AW+"\n")
            write_masking_part_to_file(vhd_file, match_0s_mask, match_1s_mask)
            write_locking_part_to_file(vhd_file,
                                       pos_0s_formatted_locking_word, pos_0s_locking_word,
                                       pos_1s_formatted_locking_word, pos_1s_locking_word)
            vhd_file.write("\nEND ARCHITECTURE rtl;\n")
    elif locking and not masking:
        pos_0s_formatted_locking_word = []
        pos_1s_formatted_locking_word = []
        
        pos_0s_locking_word = [i[0] for i in enumerate(locking_word) if i[1] == "0"]
        pos_1s_locking_word = [i[0] for i in enumerate(locking_word) if i[1] == "1"]
        while not(pos_0s_formatted_locking_word and
                  pos_1s_formatted_locking_word):
            # Generate random formatted AW with 0s and 1s in both halfs
            formatted_AW = ''.join([rd.choice(["0", "1"]) for n in xrange(width_formatted_AW)])
            formatted_locking_word = formatted_AW
            pos_0s_formatted_locking_word = [i[0] for i in enumerate(formatted_locking_word) if i[1] == "0"]
            pos_1s_formatted_locking_word = [i[0] for i in enumerate(formatted_locking_word) if i[1] == "1"]
        print "Formatted AW", formatted_AW
        print AW
        print "pos_0s_locking_word", pos_0s_locking_word
        print "pos_0s_formatted_locking_word", pos_0s_formatted_locking_word
        print "pos_1s_locking_word", pos_1s_locking_word
        print "pos_1s_formatted_locking_word", pos_1s_formatted_locking_word
        with open(path+"AW_decoder.vhd", "w") as vhd_file:
            write_header(vhd_file, locking, width_formatted_AW, len(AW))
            vhd_file.write("  -- Formatted AW: "+formatted_AW+"\n")
            vhd_file.write("  --           AW: "+AW+"\n")
            write_locking_part_to_file(vhd_file,
                                       pos_0s_formatted_locking_word, pos_0s_locking_word,
                                       pos_1s_formatted_locking_word, pos_1s_locking_word)
            vhd_file.write("\nEND ARCHITECTURE rtl;\n")
    elif not locking and masking:
        pos_0s_formatted_masking_word = []
        pos_1s_formatted_masking_word = []
        
        pos_0s_masking_word = [i[0] for i in enumerate(masking_word) if i[1] == "0"]
        pos_1s_masking_word = [i[0] for i in enumerate(masking_word) if i[1] == "1"]
        while not(pos_0s_formatted_masking_word and
                  pos_1s_formatted_masking_word):
            # Generate random formatted AW with 0s and 1s in both halfs
            formatted_AW = ''.join([rd.choice(["0", "1"]) for n in xrange(width_formatted_AW)])
            formatted_locking_word = formatted_AW
            formatted_masking_word = formatted_AW
            pos_0s_formatted_masking_word = [i[0] for i in enumerate(formatted_masking_word) if i[1] == "0"]
            pos_1s_formatted_masking_word = [i[0] for i in enumerate(formatted_masking_word) if i[1] == "1"]
        print "Formatted AW", formatted_AW
        print AW
        print "pos_0s_formatted_masking_word", pos_0s_formatted_masking_word
        print "pos_0s_masking_word", pos_0s_masking_word
        print "pos_1s_formatted_masking_word", pos_1s_formatted_masking_word
        print "pos_1s_masking_word", pos_1s_masking_word
        match_0s_mask, match_1s_mask = match_mask(pos_0s_formatted_masking_word, pos_1s_formatted_masking_word, pos_0s_masking_word, pos_1s_masking_word)
        print match_0s_mask, match_1s_mask
        with open(path+"AW_decoder.vhd", "w") as vhd_file:
            write_header(vhd_file, locking, width_formatted_AW, len(AW))
            vhd_file.write("  -- Formatted AW: "+formatted_AW+"\n")
            vhd_file.write("  --           AW: "+AW+"\n")
            write_masking_part_to_file(vhd_file, match_0s_mask, match_1s_mask)
            vhd_file.write("\nEND ARCHITECTURE rtl;\n")

    return formatted_AW

if __name__ == "__main__":
    # print gen_AW_decoder("0101 1100", 16, locking=True, masking=True)
    # print gen_AW_decoder("01011100", 16, locking=True, masking=False)
    # print gen_AW_decoder("01011100", 16, locking=False, masking=True)
    # print gen_AW_decoder("0101 1100", 8, locking=True, masking=True)
    print gen_AW_decoder("01011100", 8, locking=True, masking=False)
    # print gen_AW_decoder("01011100", 8, locking=False, masking=True)
    # print gen_AW_decoder("0101 1100", 4, locking=True, masking=True)
    # print gen_AW_decoder("01011100", 4, locking=True, masking=False)
    # print gen_AW_decoder("01011100", 4, locking=False, masking=True)
