LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY AW_decoder IS

  PORT (
    formatted_AW : IN  STD_LOGIC_VECTOR(0 TO 15);
    AW           : OUT STD_LOGIC_VECTOR(0 TO 7));

END ENTITY AW_decoder;

ARCHITECTURE rtl OF AW_decoder IS

BEGIN

  -- Formatted AW: 0111001101010110
  --           AW: 10100011

  -- Mask ORs
  AW(1) <= formatted_AW(15) OR formatted_AW(5);
  AW(3) <= formatted_AW(10) OR formatted_AW(8);
  AW(4) <= formatted_AW(4) OR formatted_AW(12);
  AW(5) <= formatted_AW(0);

  -- Mask ANDs
  AW(0) <= formatted_AW(9) AND formatted_AW(13) AND formatted_AW(14);
  AW(2) <= formatted_AW(3) AND formatted_AW(11);
  AW(6) <= formatted_AW(1) AND formatted_AW(6);
  AW(7) <= formatted_AW(7) AND formatted_AW(2);

END ARCHITECTURE rtl;
