# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: declare_initialize_variables.py
# Date: 2016-10-13

from Tkinter import *
from ttk import *


def declare_initialize_variables(self):
    
    """Declare and initialize the GUI variables"""

    self.modify_design_mode = StringVar() # Masking or locking
    self.daughter_board = StringVar()
    self.daughter_board.set("S6")
    self.PUF_response_displayed = StringVar()
    self.PUF_response_displayed.set(128*" ")
    self.filepath = StringVar()
    self.filename = StringVar()
    self.filename.set("No design loaded")
    self.modified_design_to_wrap = StringVar()
    self.modified_design_to_wrap.set("No design loaded")
    self.com_port = StringVar()
    self.com_port.set("COM5")
    self.masking_overhead = IntVar()
    self.masking_overhead.set(5)
    self.locking_overhead = IntVar()
    self.locking_overhead.set(5)

    self.graph_info = StringVar()
    self.graph_info.set("")

    self.masking_heuristic = StringVar()
    self.masking_heuristic.set("random")
    self.connected = BooleanVar()
    self.connected.set(False)

    self.message_key_saved = StringVar()
    self.message_key_saved.set("")
    self.message_AW_saved = StringVar()
    self.message_AW_saved.set("")
    self.message_modified_design_saved = StringVar()
    self.message_modified_design_saved.set("")

    self.status = StringVar()
    self.status.set("")
    self.board_status = StringVar()
    self.board_status.set("Board not connected")
    self.save_as_reference_response_status = StringVar()
    self.save_as_reference_response_status.set("")

    self.activation_word = StringVar()
    self.activation_word.set("")
    self.associated_activation_word = StringVar()
    self.associated_activation_word.set("")
    self.design_format = StringVar()
    self.design_format.set("BENCH")
    self.generated_design_format = StringVar()
    self.generated_design_format.set("vhd")
    self.programming_command = StringVar()
    self.programming_command.set("")
    self.server_reference_response_displayed = StringVar()
    self.server_reference_response_displayed.set("")
    self.server_reference_response_file = StringVar()
    self.server_reference_response_file.set("")
    self.crypto = StringVar()
    self.crypto.set("OTP")
    self.key_derivation = StringVar()
    self.key_derivation.set("None")
