# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: get_response.py
# Date : 2016-11-03

import Tkinter

import board_commands

def get_response():
    
    """Get the response from the board"""

    tclsh = Tkinter.Tcl()
    board_manager = board_commands.Board_manager()
    tclsh.eval(board_manager.source_tcl_package())
    tclsh.eval(board_manager.connect("COM6"))
    tclsh.eval(board_manager.reset_boards(2))
    for i in range(10):
        response = tclsh.eval(board_manager.generate_response())
        print response.split(" ")
    tclsh.eval(board_manager.disconnect())

if __name__ == "__main__":
    get_response()