from Tkinter import *
import board_commands

tclsh = Tcl()
board_manager = board_commands.Board_manager()

tclsh.eval(board_manager.source_tcl_package())
tclsh.eval(board_manager.connect("COM7"))
tclsh.eval(board_manager.reset_boards(port=2))
print tclsh.eval(board_manager.generate_response())

print "--------------------------------------------------------"
par = tclsh.eval(board_manager.command_get_parities_from_indices([[0, 1, 2, 4],
                                                                  [0, 1, 2, 3],
                                                                  [0, 1, 2, 4],
                                                                  [0, 1, 2, 3]], 128))
print par
